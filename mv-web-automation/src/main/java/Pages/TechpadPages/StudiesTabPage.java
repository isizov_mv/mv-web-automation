package Pages.TechpadPages;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class StudiesTabPage extends BasePage {
	public StudiesTabPage(WebDriver driver) {
        super(driver);
    }
	
	
    @FindBy(css = ("a[class='item study-item']"))
    private List<WebElement> studiesList;
    
    @FindBy(xpath = ("//button[.='Yes']"))
    private WebElement yesButton;
    
    private By yesButtonBy = 
    		By.xpath("//button[.='Yes']");
    
    @FindBy(xpath = ("//button[.='No']"))
    private WebElement noButton;
    
    private By noButtonBy = 
    		By.xpath("//button[.='No']");
    
    @Step(value = "Select a study from studies list by Accession ID")
    public void selectStudyFromStudiesListByIDNumber(String accessionID) {
    	this.sleep(500);	
    	int i = this.getRowIndexFromColumnListByPartialText(studiesList, accessionID);
    	this.scrollElementToCenterOfView(studiesList.get(i));
    	this.sleep(50);
    	this.highlightWebElement(studiesList.get(i));
    	this.click(studiesList.get(i));
    	this.waitAllRequests();
    }

    @Step(value = "Assert that study with accession ID '{accessionID}' is shown")
	public boolean isStudyShown(String accessionID) {
    	int i = this.getRowIndexFromColumnListByPartialText(studiesList, accessionID);
		return (i > -1);
	} 
    
    @Step(value = "Click Yes Button")
    public void clickYesButton()
    {
        this.sleep(150);       
        this.waitAllRequests(3);
        if (driver.findElements(yesButtonBy).size() > 0) {
	    	this.highlightWebElement(yesButton);
	    	this.sleep(300);
	        this.click(yesButton);
        }
        this.sleep(150);
    } 
    @Step(value = "Click No Button")
    public void clickNoButton()
    {
        this.sleep(150);       
        this.waitAllRequests(3);
        if (driver.findElements(noButtonBy).size() > 0) {
	    	this.highlightWebElement(noButton);
	    	this.sleep(300);
	        this.click(noButton);
        }
        this.sleep(150);
    } 
	
	
	
}