package Pages.TechpadPages;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CalendarPage extends BasePage {

	public CalendarPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//select[contains(@ng-model,'data.currentMonth')]")
    private WebElement monthDropdown;
    
    @FindBy(xpath = "//select[contains(@ng-model,'data.currentYear')]")
    private WebElement yearDropdown;
    
    @FindBy(xpath = "//button[.='Close']")
    private WebElement closeButton;
    
    @FindBy(xpath = "//button[.='Set']")
    private WebElement setButton;  
    
    
    
    
	@Step(value = "click Month button")
    public void clickMonthButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(monthDropdown);
    }
    
    
    @Step(value = "Select an option from dropdown menu  Month by option text")
    public void selectOptionFromMonthDropdownByText(String month) {
    	this.sleep(150);
    	this.clickMonthButton() ;
    	this.waitAllRequests(1);
    	this.clickElementFromListByVisibleText(monthDropdown, month);
    	this.sleep(150);
    	this.clickMonthButton() ;
    	this.sleep(200);
    } 
    
    @Step(value = "click Year button")
    public void clickYearButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(yearDropdown);
    }
    
    
    @Step(value = "Select an option from dropdown menu  Year by option text")
    public void selectOptionFromYearDropdownByText(String year) {
    	this.sleep(150);
    	this.clickYearButton() ;
    	this.waitAllRequests(1);
    	this.clickElementFromListByVisibleText(yearDropdown, year);
    	this.sleep(150);
    	this.clickYearButton() ;
    	this.sleep(200);
    }     
    
    @Step(value = "Select and click Day Button from Calendar window by text")
    public void selectDayButtonFromCalendarByText(String day) {	
    	String dayButtonLocator =  "//div[contains(@class,'date_cell ng-binding')][contains(.,'" + day + "')]";
        By dayButtonBy = By.xpath(dayButtonLocator);
        //if the day is already selected then use a locator for selected days
        if (driver.findElements(dayButtonBy).size() == 0) {
        	dayButtonLocator =  "//div[contains(@class,'col no_padding date_col selected_date')][contains(.,'" + day + "')]";
            dayButtonBy = By.xpath(dayButtonLocator);
        }
        WebElement dayButton = driver.findElement(dayButtonBy);        
    	this.sleep(150);
    	this.waitAllRequests(1);
    	this.click(dayButton);
    	this.sleep(200);
    } 
    
    @Step(value = "click Close button")
    public void clickCloseButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.highlightWebElement(closeButton);
        this.click(closeButton);
    }
    
    @Step(value = "click Set button")
    public void clickSetButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.highlightWebElement(setButton);
        this.click(setButton);
    }
    
    
    
    
    
    
    
}