package Pages.TechpadPages;


import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class DemographicTabPage extends BasePage {

	public DemographicTabPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button[class*='calendar']")
    private WebElement calendarButton; 
    
    
	@Step(value = "click Calendar button")
    public void clickCalendarButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(calendarButton);
        this.click(calendarButton);
    }
    
    
    
    
    
    
    
}