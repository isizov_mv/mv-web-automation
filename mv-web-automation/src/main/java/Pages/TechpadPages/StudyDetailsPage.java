package Pages.TechpadPages;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;



public class StudyDetailsPage extends BasePage {
	public StudyDetailsPage(WebDriver driver) {
        super(driver);
    }
	

    //@FindBy(css = "button[ng-click='vm.addToWorklist()']")
    @FindBy(css = "button[ng-click*='addToWorklist']")
    private WebElement addToworklistButton; 
	
    @FindBy(css = "button[ng-click='vm.skipSurveyAndStartExam()']")
    private WebElement skipSurveyButton; 
	
    @FindBy(css = "button[ng-click='vm.startSurvey()']")
    private WebElement startSurveyButton; 
    
    @FindBy(xpath = "//button[.='OK']")
    private WebElement okButton; 
    
    @FindBy(css = "button[ng-click='vm.goBack()']")
    private WebElement goBackButton;
    
  


    	
    
    @Step(value = "click Add to Worklist Button")
    public void clickAddToWorklistButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.click(addToworklistButton);
    	//this.clickOverElement(addToworklistButton);
    	this.sleep(500);
    } 
	
    @Step(value = "click Skip Survey Button")
    public void clickSkipSurveyButton() {
    	this.sleep(500);	
    	this.waitAllRequests(1);
    	this.click(skipSurveyButton);
    	this.sleep(500);
    }
    
    @Step(value = "click Start Survey Button")
    public void clickStartSurveyButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.click(startSurveyButton);
    	this.sleep(500);
    }
    
    @Step(value = "click OK Button")
    public void clickOKButton() {
    	this.sleep(500);	
    	this.waitAllRequests(1);
    	this.click(okButton);
    	this.sleep(500);
    }
    
    @Step(value = "click Go Back Button")
    public void clickGoBackButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	//this.click(goBackButton);
    	this.clickUsingJavascript(goBackButton);
    	this.sleep(2000);
    }
    
    
}