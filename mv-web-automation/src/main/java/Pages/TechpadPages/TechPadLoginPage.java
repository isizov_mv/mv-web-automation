package Pages.TechpadPages;



import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TechPadLoginPage extends BasePage {

    public TechPadLoginPage(WebDriver driver) {
        super(driver);
    }

   String techpadTitle = "Techpad";



    @FindBy(css = "button[ng-click^='vm.changeHospital']")
    private WebElement changeButton;

    @FindBy(css = "input[ng-model*='data.code']")
    private WebElement hospitalCodeTextBox;

    @FindBy(css = "input[ng-model*='username']")
    private WebElement userName;

    @FindBy(css = "input[ng-model*='password']")
    private WebElement userPassword;

    @FindBy(xpath  = "//b[contains(.,'Submit')]")
    private WebElement submitButton;

    @FindBy(css = "button[ng-click^='vm.login']")
    private WebElement signInButton;

    

    
    @Step(value = "Open TechPad Login page")
    public void gotoLoginPage(String URL)
    {
        driver.get(URL);
    }

    @Step(value = "Click Change Button")
    public void clickChangeButton()
    {
        this.click(changeButton);
        this.sleep(2000);
    }

    @Step(value = "Enter Hospital Code")
    public void enterCode(String hospitalCodeText)
    {
        this.clickOverElement(hospitalCodeTextBox);
        this.enterText(hospitalCodeTextBox, hospitalCodeText);
        this.sleep(2000);
    }

    @Step(value = "Enter User Name")
    public void enterUserName(String UseNameText)
    {
        this.clickOverElement(userName);
        this.enterText(userName, UseNameText);
        this.sleep(2000);
    }

    @Step(value = "Enter password")
    public void enterPassword(String passwordText)
    {
        this.clickOverElement(userPassword);
        this.enterText(userPassword, passwordText);
        this.sleep(2000);
    }

    @Step(value = "Press Submit Button")
    public void clickSubmitButton()
    {
        this.clickOverElement(submitButton);
        this.sleep(2000);
    }

    @Step(value = "Press Sign-In button")
    public void clickSignInButton()
    {
        signInButton.click();
        //this.sleep(7000);
    }

    @Step(value = "verify Login is successful")
    public boolean isLoginSuccessful(){
        return this.driver.getTitle().contains(techpadTitle);
    }

    @Step(value = "Login to TechPad")
    public void loginToTechPad(String URL, String hospitalCode, String userName, String password){
        this.gotoLoginPage(URL);
        
        this.enterFullScreenMode();
        
        this.clickChangeButton();
        this.enterCode(hospitalCode);
        this.clickSubmitButton();
        this.enterUserName(userName);
        this.enterPassword(password);
        this.clickSignInButton();
        this.waitUntilTechPadPageIsReady();
    }
       
    @Step(value = "wait until TechPad page is ready")
    public void waitUntilTechPadPageIsReady(){
    	this.waitUntilElementIsPresent(By.cssSelector("button[ng-click^='toggleLeft']"));
    }
    
    
    
    

}
