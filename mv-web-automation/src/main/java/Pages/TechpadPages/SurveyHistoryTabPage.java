package Pages.TechpadPages;



import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class SurveyHistoryTabPage extends BasePage {

	public SurveyHistoryTabPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button[ng-click*='skipSurvey']")
    private WebElement skipSurveyButton; 
    
    @FindBy(css = "button[ng-click*='startSurvey']")
    private WebElement startSurveyButton; 
    
    @FindBy(xpath = " //button[.='Yes']")
    private WebElement yesButton;
 
    @FindBy(xpath = " //button[.='No']")
    private WebElement noButton;  
    
    @FindBy(xpath = " //button[contains(., 'Save')][contains(., 'Continue')]")
    private WebElement saveAndContinueButton;  
    
    @FindBy(xpath = "//button[contains(., 'Discard changes')]")
    private WebElement discardChangesButton;  
    
    @FindBy(xpath = "//button[contains(., 'Cancel')]")
    private WebElement cancelButton;  
    
  
  
    
	@Step(value = "click Skip Survey Button button")
    public void clickSkipSurveyButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(skipSurveyButton);
        this.click(skipSurveyButton);
    }
	
	@Step(value = "click Start Survey Button button")
    public void clickStartSurveyButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(startSurveyButton);
        this.click(startSurveyButton);
    }
    @Step(value = "click Yes Button")
    public void clickYesButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(yesButton);
    	this.click(yesButton);
    	this.sleep(1500);
    } 	
    
    @Step(value = "click No Button")
    public void clickNoButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(noButton);
    	this.click(noButton);
    	this.sleep(1500);
    }
    
    @Step(value = "click Save And Continue Button")
    public void clickSaveAndContinueButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(saveAndContinueButton);
    	this.click(saveAndContinueButton);
    	this.sleep(4500);
    }
    
    @Step(value = "click Discard changes Button")
    public void clickDiscardChangesButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(discardChangesButton);
    	this.click(discardChangesButton);
    	this.sleep(1500);
    }

    @Step(value = "click Cancel Button")
    public void clickCancelButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(cancelButton);
    	this.click(cancelButton);
    	this.sleep(1500);
    }

    
    
}


