package Pages.TechpadPages;



import Pages.BasePage.BasePage;

import io.qameta.allure.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class TechPadHomePage extends BasePage {
	public TechPadHomePage(WebDriver driver) {
        super(driver);
    }
	
	@FindBy(css = "button[ng-click*='toggleLeft']")
    private WebElement hamburgerMenuButton;
	
	private By hamburgerMenuButtonBy = 
			By.cssSelector("button[ng-click*='toggleLeft']");	 
	
     @FindBy(xpath = "//button[contains(.,'PATIENTS')][not(contains(.,'MY'))]")
    private WebElement patientsLeftButton;
     
     @FindBy(xpath = "//button[contains(.,'WORKLIST')]")
    private WebElement worklistLeftButton;
     
    
    @FindBy(css = "button[id='logout-btn'][ng-click='logout()']")
    private WebElement logoutButton;
    
    @FindBy(xpath = "//button[contains(.,'Yes')]")
    private WebElement confirmYesToLogoutButton;
    
    @FindBy(css = "input[ng-model='vm.filter.simpleSearch']")
    private WebElement simpleSearchTextBox;
    
    @FindBy(css = "button[ng-click='vm.search()']")
    private WebElement searchButton;
    
    @FindBy(xpath = ("//ion-item"))
    private List<WebElement> patientsTable;   
    
    @FindBy(css = "button[class*='ion-minus']")
    private WebElement advancedSearchMinusButton;
    
    @FindBy(css = "button[class*='ion-plus']")
    private WebElement advancedSearchPlusButton;
    
    @FindBy(css = "input[ng-model='vm.filter.ssn']")
    private WebElement mrnTextbox;
    
    @FindBy(css = "input[ng-model='vm.filter.firstName']")
    private WebElement firstNameTextbox;
    
    @FindBy(css = "input[ng-model='vm.filter.lastName']")
    private WebElement lastNameTextbox;
    
    @FindBy(css = "div[class='buttons left-buttons']")   
    private WebElement backFromPatientsDetailsButton;
       
    private By lookingForPatientsLabelBy = 
    		By.cssSelector("ion-item[ng-if='vm.isSearching']");
    
    @FindBy(xpath = "//button[.='Yes']")
    private WebElement yesButton; 
  
    @FindBy(xpath = "//button[.='No']")
    private WebElement noButton; 
    
    @FindBy(css = "div [ng-click$='showExamWindow(item)']")
    private List <WebElement> examList; 
    
    
    @FindBy(css = "input[ng-model='vm.filterObj.search']")
    private WebElement examSimpleSearchTextBox; 
    
    @FindBy(css = "button[id='search-btn']")
    private WebElement examSimpleSearchButton; 
    
    
    @FindBy(css = "button[id='searchBox-btn']")
    private WebElement magnifyingGlassExamSimpleSearchButton; 
    
    @FindBy(xpath = "//button[contains(.,'Perform Exam')]")
    private WebElement performExamButton;
    
    @FindBy(xpath = "//button[contains(.,'Yes')]")
    private WebElement confirmYesToOvertakeExamButton;
    
    private By confirmYesToOvertakeExamButtonBy = 
    		By.xpath("//button[contains(.,'Yes')]");
    
    @FindBy(xpath = "//button[contains(.,'No')]")
    private WebElement confirmNoToRejectOvertakeExamButton;
    
    @FindBy(css = "button[id='funnel-btn']")
    private WebElement worklistFilterSettingsButton;
    
    
    private By showOnlyMyExams_CheckedBy = 
    		By.cssSelector("input[ng-model*='MyExams'][class*='ng-not-empty']");
    private By showOnlyMyExams_UncheckedBy = 
    		By.cssSelector("input[ng-model*='MyExams'][class*='ng-empty']");
    
    
    private By showOnlyTodaysExams_CheckedBy = 
    		By.cssSelector("input[ng-model*='TodayExams'][class*='ng-not-empty']");
    private By showOnlyTodaysExams_UncheckedBy = 
    		By.cssSelector("input[ng-model*='TodayExams'][class*='ng-empty']");
   
    @FindBy(css = "button[ng-click*='saveSettings']")
    private WebElement saveSettingsButton;
    
    
    
    
    
    
    @Step(value = "Click Hamburger Menu in the left upper corner")
    public void clickHamburgerMenuButton()
    {
    	this.waitAllRequests();
    	this.sleep(500);
    	this.waitUntilElementIsPresent(hamburgerMenuButtonBy);
        this.highlightWebElement(hamburgerMenuButton);
        this.click(hamburgerMenuButton);
        this.sleep(2000);
    }
	
    @Step(value = "Click Patients Button from the left menu")
    public void clickPatientsLeftButton()
    {
        this.highlightWebElement(patientsLeftButton);
        this.click(patientsLeftButton);
        this.sleep(500);  
    }
    
    @Step(value = "Click Worklist Button from the left menu")
    public void clickWorklistLeftButton()
    {
        this.highlightWebElement(worklistLeftButton);
        this.click(worklistLeftButton);
        this.waitAllRequests();
        this.sleep(1000);
    }
    
    @Step(value = "Click Logout  Button")
    public void clickLogoutButton()
    {
        this.click(logoutButton);
        this.sleep(1000);
    }
    
    @Step(value = "Enter text into Simple Search TextBox")
    public void enterTextIntoSimpleSearchTextbox(String text)
    {
    	this.waitAllRequests();
        this.enterText(simpleSearchTextBox, text);
        this.sleep(1000);
    }
    
    @Step(value = "Click Search Button")
    public void clickSearchButton()
    {
    	this.waitAllRequests();
        this.click(searchButton);
        this.sleep(2500);
    	this.waitAllRequests();       
    }
    
    @Step(value = "Click Yes Button to Confirm Logout")
    public void clickYesToConfirmLogoutButton()
    {
        this.click(confirmYesToLogoutButton);
        this.sleep(1000);
    }
    
    public void printPatientsNamesToConsole()
    {
    	for (WebElement patient : patientsTable){    		
    		String patient_info = patient.getText();
    		System.out.println("patient info = " +  patient_info);
    		this.sleep(1);   		
    	}
    }
    
    @Step(value = "verify patient's first name is shown in search results")
    public boolean verifyPatientFirstNameIsShown(String fname)
    {
    	for (WebElement patient : patientsTable){    		
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(fname.toUpperCase())) {
    			return true;
    		} 		
    	}
    	return false;
    }
    
    @Step(value = "verify patient's last name is shown in search results")
    public boolean verifyPatientLastNameIsShown(String lname)
    {
    	for (WebElement patient : patientsTable){    		
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(lname.toUpperCase())) {
    			return true;
    		} 		
    	}
    	return false;
    }
    
    @Step(value = "verify patient's birthday is shown in search results")
    /**
    This function takes birthday as a String variable
    using format: 12/12/1950
    */
    public boolean verifyPatientBirthdayIsShown(String birthday)
    {
    	for (WebElement patient : patientsTable){    		
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(birthday.toUpperCase())) {
    			return true;
    		} 		
    	}
    	return false;
    }
    
    @Step(value = "verify patient's SSN  is shown in search results")
    public boolean verifyPatientSSNIsShown(String ssn)
    {
    	for (WebElement patient : patientsTable){    		
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(ssn.toUpperCase())) {
    			return true;
    		} 		
    	}
    	return false;
    }
    
    @Step(value = "verify the record  {SSN} - {fname}, {lname} ({birthday})  is shown in search results")
    /**
    This function takes 4 parameters of type String
    1. SSN, example: 1234
    2. First Name, example: Vivica
    3. Last Name, example: Lofthouse
    4. Birthday, example: 12/12/1950
    */
    public boolean verifyPatientSSNFnameLnameBirthdayAreShown(String SSN, String lname, String fname, String birthday)
    {
		String expected_info = SSN + " - " + lname + ", " + fname + " " + "(" + birthday + ")";
		//System.out.println("expected info = " +  expected_info);
    	for (WebElement patient : patientsTable){ 

    		String patient_info = patient.getText();
    		//System.out.println("actual info =       " +  patient_info);
    		if (patient_info.trim().toUpperCase().contains(expected_info.trim().toUpperCase())) {
    			return true;
    		} 		
    	}
    	return false;
    }
    
    
    
    
    

    @Step(value = "Click Advanced Search Minus Button")
    public void clickAdvancedSearchMinusButton()
    {
        this.click(advancedSearchMinusButton);
        this.sleep(500);
    }
    
    @Step(value = "Click Advanced Search Plus Button")
    public void clickAdvancedSearchPlusButton()
    {
        this.click(advancedSearchPlusButton);
        this.sleep(500);
    }    
    
    @Step(value = "Enter text into MRN TextBox")
    public void enterTextIntoMRNTextbox(String text)
    {
        this.enterText(mrnTextbox, text);
        this.sleep(1000);
    }
    
    @Step(value = "Enter text into First Name TextBox")
    public void enterTextIntoFirstNameTextbox(String text)
    {
        this.enterText(firstNameTextbox, text);
        this.sleep(1000);
    }
    
    @Step(value = "Enter text into Last Name TextBox")
    public void enterTextIntoLastNameTextbox(String text)
    {
        this.enterText(lastNameTextbox, text);
        this.sleep(1000);
    }
    
    public boolean selectPatient(String SSN, String lname, String fname, String birthday)
    {
		String expected_info = SSN + " - " + lname + ", " + fname + " " + "(" + birthday + ")";
    	for (WebElement patient : patientsTable){ 
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(expected_info.toUpperCase())) {
    			this.click(patient);
    			return true;
    		} 		
    	}
    	return false;
    }
    
    public boolean selectPatientBySSN(String SSN)
    {
		String expected_info = SSN;
    	for (WebElement patient : patientsTable){ 
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(expected_info.toUpperCase())) {
    			this.click(patient);
    			this.sleep(3000);
    			return true;
    		} 		
    	}
    	return false;
    }
    
    public boolean selectPatient(String lname, String fname)
    {
		String expected_info = lname + ", " + fname;
    	for (WebElement patient : patientsTable){ 
    		String patient_info = patient.getText();
    		if (patient_info.toUpperCase().contains(expected_info.toUpperCase())) {
    			this.click(patient);
    			this.waitAllRequests();
    			this.sleep(500);
    			return true;
    		} 		
    	}
    	return false;
    }
    
    
    @Step(value = "Click Back From Patients Details Button")
    public void clickBackFromPatientsDetailsButton()
    {
    	this.highlightWebElement(backFromPatientsDetailsButton);
        this.clickOverElement(backFromPatientsDetailsButton);
        this.sleep(2500);
    }   
    
    @Step(value = "Wait until Looking For Patients... label is visible")
    public void waitUntilLookingForPatientsLabelIsVisible()
    {
        this.waitUntilElementIsNoLongerDisplayed(lookingForPatientsLabelBy);
        this.sleep(500);
    }
    
    @Step(value = "Click Yes Button for: You have unsaved changes, are you sure you want to revert them?")
    public void clickYesToUnsaveShanges()
    {
    	this.highlightWebElement(yesButton);
        this.clickOverElement(yesButton);
        this.sleep(3500);
    }
    
    
    @Step(value = "Scroll To And Select Exam by patient's SSN")
    public void scrollToAndSelectExamByPatientSSN(String ssn)
    {
    	int row = this.getRowIndexFromColumnListByPartialText(examList, ssn);
    	int j = 1;
    	while ((row == -1) && (j<100)) {
    		try {
				this.scrollDownUsingRobot(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        this.sleep(500);
	        row = this.getRowIndexFromColumnListByPartialText(examList, ssn);
	        j++;
    	}
    	if (j > 1) {
	    	try {
				this.scrollDownUsingRobot(3);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
        this.sleep(300);
    	this.highlightWebElement(examList.get(row));
        this.click(examList.get(row));
        this.waitAllRequests();
        this.sleep(500);
    }

    
    @Step(value = "Search Exam Worklist by Patient's SSN")
    public void searchExamWorklistByPatientSSN(String ssn)
    {
    	
    	//test
    	js.executeScript("window.scrollBy(0,500)");
    	this.sleep(3000);
    	
    	
    	this.click(magnifyingGlassExamSimpleSearchButton);
    	this.sleep(500);
    	this.waitAllRequests();
    	this.enterText(examSimpleSearchTextBox, ssn);
        this.sleep(150);
        this.click(examSimpleSearchButton);
    	this.waitAllRequests();       
        this.sleep(3000);
    }
    
    @Step(value = "Search Exam Worklist by Patient's SSN, Last name, and First name")
    public void searchExamWorklistByPatientSSNLnameFirstName(String ssn, String lname, String fname)
    {
    	this.sleep(500);
    	this.clickUsingJavascript(magnifyingGlassExamSimpleSearchButton);
    	this.sleep(500);
    	this.waitAllRequests();
    	String patientInfo = lname + " " + fname + " " + ssn;
    	this.enterText(examSimpleSearchTextBox, patientInfo);
        this.sleep(150);
        this.click(examSimpleSearchButton);
    	this.waitAllRequests();       
        this.sleep(3000);
    }
    
    
    
    
    @Step(value = "Click Perform Exam button")
    public void clickPerformExamButton()
    {
        this.waitAllRequests();
        this.highlightWebElement(performExamButton);
        this.click(performExamButton);
        this.waitAllRequests(3);
        this.sleep(5000);
    }
    
    
    @Step(value = "Select Exam From Search Results by Patient's SSN")
    public void selectExamFromSearchResultsByPatientSSN(String ssn)
    {
    	this.sleep(500);
    	this.waitAllRequests();
    	int row = this.getRowIndexFromColumnListByPartialText(examList, ssn);
        this.sleep(300);
    	this.highlightWebElement(examList.get(row));
    	
        this.click(examList.get(row));
        this.waitAllRequests();
        this.sleep(500);
    }
    
    
    @Step(value = "Select Exam From Search Results by Patient's SSN, Last name, and First name")
    public void selectExamFromSearchResultsByPatientSSNLnameFname(String ssn, String lname, String fname)
    {
    	this.sleep(500);
    	this.waitAllRequests();
    	String patientInfo = lname + ", " + fname + " (" + ssn + ")";
    	int row = this.getRowIndexFromColumnListByPartialText(examList, patientInfo);
    	
    	//System.out.println("get pat text:");
    	//System.out.println(examList.get(row).getText());    	
        this.sleep(300);
    	this.highlightWebElement(examList.get(row));
        this.click(examList.get(row));
        this.waitAllRequests();
        this.sleep(1500);
    }
    
    
    
    @Step(value = "Click Yes button to overtake Exam")
    public void clickYesToConfirmOvertakeExamButton()
    {
        this.waitAllRequests(3);
        if (driver.findElements(confirmYesToOvertakeExamButtonBy).size() > 0) {
	    	this.highlightWebElement(confirmYesToOvertakeExamButton);
	    	this.sleep(300);
	        this.click(confirmYesToOvertakeExamButton);
	        this.sleep(3000);
        }
    }
    
    @Step(value = "Click No button to reject overtaking Exam")
    public void clickNoToRejectOvertakeExamButton()
    {
        this.waitAllRequests(3);
        this.click(confirmNoToRejectOvertakeExamButton);
        this.sleep(2500);
    }
    
    
    @Step(value = "Click Worklist Settings Button")
    public void clickWorklistFilterSettingsButton()
    {
        this.highlightWebElement(worklistFilterSettingsButton);
        this.click(worklistFilterSettingsButton);
        this.sleep(500);
    }
    
    
    @Step(value = "Check: Show only my exams")
    public void check_ShowOnlyMyExams()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showOnlyMyExams_UncheckedBy);
    }
    @Step(value = "Uncheck: Show only my exams")
    public void uncheck_ShowOnlyMyExams()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showOnlyMyExams_CheckedBy);
    }
    
    @Step(value = "Check: Show only today's exams")
    public void check_ShowOnlyTodaysExams()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showOnlyTodaysExams_UncheckedBy);
    }
    @Step(value = "Uncheck: Show only today's exams")
    public void uncheck_ShowOnlyTodaysExams()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showOnlyTodaysExams_CheckedBy);
    }
    
    
    @Step(value = "Click Save Worklist Filter settings Button")
    public void clickSaveWorklistFilterSettingsButton()
    {
        this.highlightWebElement(saveSettingsButton);
        this.click(saveSettingsButton);
        this.sleep(1000);
    }
    

    
    
    
}