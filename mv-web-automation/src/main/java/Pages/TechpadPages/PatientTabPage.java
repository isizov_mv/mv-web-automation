package Pages.TechpadPages;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;



public class PatientTabPage extends BasePage {
	public PatientTabPage(WebDriver driver) {
        super(driver);
    }
	
	
    @FindBy(css = "input[ng-model='vm.patient.LastName']")
    private WebElement lastNamePatientTextbox;
    
    @FindBy(css = "input[ng-model='vm.patient.FirstName']")
    private WebElement firstNamePatientTextbox;
    
    
    @FindBy(css = "span[ng-if*='vm.patient.DOB']")
    private WebElement DOBButton;
    
    @FindBy(xpath = "//button[.='Close']")
    private WebElement closeDOBCalendarButton; 
    
  
    @FindBy(xpath = "//button[.='Set']")
    private WebElement setDOBCalendarButton; 
    
    private By maleGenderCheckbox_CheckedBy = 
    		By.cssSelector("input[ng-model='vm.patient.Sex'][value='false'][class*='ng-not-empty']");
    private By maleGenderCheckbox_UncheckedBy = 
    		By.cssSelector("input[ng-model='vm.patient.Sex'][value='false'][class*='ng-empty']");    
    
    
    private By femaleGenderCheckbox_CheckedBy = 
    		By.cssSelector("input[ng-model='vm.patient.Sex'][value='true'][class*='ng-not-empty']");
    private By femaleGenderCheckbox_UncheckedBy = 
    		By.cssSelector("input[ng-model='vm.patient.Sex'][value='true'][class*='ng-empty']");   
    

    @FindBy(css = " input[ng-model='vm.patient.Email']")
    private WebElement emailTextbox;
    
    @FindBy(xpath = "//select[contains(.,'Select Race')]")
    private WebElement raceDropdown;
    
    @FindBy(css = "button[ng-click='vm.changeDate()']")
    private WebElement calendarButton;
    
     
/**    
    @FindBy(css = ("option[label='White'], \r\n"
    		+ "option[label='African American'], \r\n"
    		+ "option[label='American Indian, Eskimo, Aleut'], \r\n"
    		+ "option[label='Asian American'], \r\n"
    		+ "option[label='Other Race'], \r\n"
    		+ "option[label='Hispanic'], \r\n"
    		+ "option[label='Chinese'][value='string:7'], \r\n"
    		+ "option[label='Japanese'][value='string:8'], \r\n"
    		+ "option[label='Filipino'], \r\n"
    		+ "option[label='Native Hawaiian'], \r\n"
    		+ "option[label='Asian'], \r\n"
    		+ "option[label='Pacific Islander']"))
    private List<WebElement> raceDropdownList;
/**/    
    @FindBy(xpath = "//button[contains(.,'Save')]")
    private WebElement saveButton; 
    
    @FindBy(xpath = "//li[2]")
    private WebElement studiesTabButton; 
  
    
    
    
    @Step(value = "Enter Patient Last Name inPatient Tab")
    public void enterPatientLastNameInPatientTab(String lname)
    {
        this.enterText(lastNamePatientTextbox, lname);
        this.sleep(500);
    } 
    
    @Step(value = "Enter Patient First Name inPatient Tab")
    public void enterPatientFirstNameInPatientTab(String fname)
    {
        this.enterText(firstNamePatientTextbox, fname);
        this.sleep(500);
    } 
    
    @Step(value = "Click DOB field")
    public void clickDOBField()
    {
        this.click(DOBButton);
        this.sleep(500);
    } 
	
    @Step(value = "Click Close DOB Calendar Button")
    public void clickCloseDOBCalendarButton()
    {
        this.click(closeDOBCalendarButton);
        this.sleep(500);
    } 
    
    @Step(value = "Click Set DOB Calendar Button")
    public void clickSetDOBCalendarButton()
    {
        this.click(setDOBCalendarButton);
        this.sleep(500);
    } 
    
    
    

    @Step(value = "Check: Male Gender Checkbox")
    public void check_maleGenderCheckbox()
    {
        this.checkCheckbox(
        		maleGenderCheckbox_UncheckedBy);
    }
    @Step(value = "Uncheck: Male Gender Checkbox")
    public void uncheck_maleGenderCheckbox()
    {
        this.uncheckCheckbox(
        		maleGenderCheckbox_CheckedBy );
    }
    @Step(value = "Change state and return it back for:  Male Gender Checkbox.")
    public void doOnOff_maleGenderCheckbox()
    {
        this.changeCheckboxStateThenRestoreItBack(
        		maleGenderCheckbox_CheckedBy,
        		maleGenderCheckbox_UncheckedBy);
    }
    
    
    @Step(value = "Check: Female Gender Checkbox")
    public void check_femaleGenderCheckbox()
    {
        this.checkCheckbox(
        		femaleGenderCheckbox_UncheckedBy);
    }
    @Step(value = "Uncheck: Female Gender Checkbox")
    public void uncheck_femaleGenderCheckbox()
    {
        this.uncheckCheckbox(
        		femaleGenderCheckbox_CheckedBy );
    }
    @Step(value = "Change state and return it back for:  Female Gender Checkbox.")
    public void doOnOff_femaleGenderCheckbox()
    {
        this.changeCheckboxStateThenRestoreItBack(
        		femaleGenderCheckbox_CheckedBy,
        		femaleGenderCheckbox_UncheckedBy);
    }
      
    
    
    @Step(value = "Enter Text into Email textbox")
    public void enterTextIntoEmailTextbox (String text) {
    	this.enterText(emailTextbox, text);
    }
    
    
    @Step(value = "Select an option from dropdown menu 'Select Race' by option text")
    public void selectOptionFromSelectRaceDropdownByText(String optionText) {
    	this.sleep(500);
    	this.clickElementFromListByVisibleText(raceDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu 'Select Race' by option index")
    public void selectOptionFromSelectRaceDropdownByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(raceDropdown, i);
    	this.sleep(500);
    }  
    
    @Step(value = "Click Save Button")
    public void clickSaveButton()
    {
        this.sleep(1000);
        this.waitAllRequests();
        this.click(saveButton);
        this.sleep(2500);
    } 
    
    @Step(value = "Click Studies Tab Button")
    public void clickStudiesTabButton()
    {
    	this.waitAllRequests(3);
    	this.sleep(500);
        this.click(studiesTabButton);
        this.sleep(1500);
    } 
    
    @Step(value = "Click Calendar Button")
    public void clickCalendarButton()
    {
    	this.waitAllRequests(3);
    	this.sleep(500);
        this.click(calendarButton);
        this.sleep(1500);
    } 
    
	
}
	