package Pages.TechpadPages;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class ExamTabPage extends BasePage {

	public ExamTabPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//select")
    private List <WebElement> dropdownsSelect; 
    		
    @FindBy(xpath = "//select[contains(.,'requested')][not(contains(.,'lower'))]")
    private WebElement reasonForVisitDropdown;    		
    
    @FindBy(xpath = "//select[not(contains(.,'t'))]")
    private WebElement infectionControlDropdown;
    
    private By infectionControlDropdownBy = 
    		By.xpath("//select[not(contains(.,'t'))]");
    
    @FindBy(xpath = "//select[contains(.,'lower')]")
    private WebElement viewsObtainedDropdown;
    
    private By viewsObtainedDropdownBy = 
    		By.xpath("//select[contains(.,'lower')]");
    
    
    
    private By deleteViewsButtonsBy = 
    		By.cssSelector("button[class*='delete-procedure-btn'][ng-click*='vm.removeView(p)']");
    
    
    @FindBy(xpath = "//select[contains(.,'side')]")
    private List <WebElement> sideViewsObtainedDropdownLists;
    
    private By sideViewsObtainedDropdownsBy = 
    		By.xpath("//select[contains(.,'side')]");
    
    @FindBy(xpath = "//select[not(contains(.,'l'))][contains(.,'y')]")
    private List <WebElement> typeViewsObtainedDropdownLists;
    
    private By typeViewsObtainedDropdownsBy = 
    		By.xpath("//select[not(contains(.,'l'))][contains(.,'y')]");
    
    @FindBy(xpath = "//select[contains(.,'Breast')]")
    private WebElement proceduresDropdown; 
    
    private By proceduresDropdownBy = 
    		By.xpath("//select[contains(.,'Breast')]");
    
    private By deleteProceduresButtonsBy = 
    		By.cssSelector("button[class*='delete-procedure-btn'][ng-click*='vm.removeProcedure(p)']");
    
    @FindBy(xpath = "//select[(contains(.,'Side'))][not(contains(.,'s'))]")
    private List<WebElement> sideProceduresDropdownLists; 
    
    private By sideProceduresDropdownsBy = 
    		By.xpath("//select[(contains(.,'Side'))][not(contains(.,'s'))]");
      
    @FindBy(css = "div[ng-click='vm.selectProblems()']")
    private WebElement problemCircleButton; 
    
    
    @FindBy(css = "div[class^='col-45 name-label problem-name-label']")
    private List<WebElement> indicatedProblemsList; 
    
    //------------------------------------------------V
    @FindBy(css = "input[type='checkbox'][ng-change*='ProblemUnspecified']")
    private List<WebElement> unspecifiedColumnList;
    
    
    @FindBy(css = "input[type='checkbox'][ng-change*='ProblemUnspecified'][class*='ng-empty']")
    private List<WebElement> unspecifiedColumnList_Unchecked;
    
    private By unspecifiedColumnList_UncheckedBy = 
    		By.cssSelector("input[type='checkbox'][ng-change*='ProblemUnspecified'][class*='ng-empty']");
    
    @FindBy(css = "input[type='checkbox'][ng-change*='ProblemUnspecified'][class*='ng-not-empty']")
    private List<WebElement> unspecifiedColumnList_Checked;
    
    private By unspecifiedColumnList_CheckedBy = 
    		By.cssSelector("input[type='checkbox'][ng-change*='ProblemUnspecified'][class*='ng-not-empty']");
    //------------------------------------------------^
    
    
    //------------------------------------------------V
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Left']")
    private List<WebElement> leftColumnList;
    
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Left'][class*='ng-empty']")
    private List<WebElement> leftColumnList_Unchecked;
    
    private By leftColumnList_UncheckedBy = 
    		By.cssSelector("input[ng-change='model.updateProblemSide(v)'][ng-model$='Left'][class*='ng-empty']");
    
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Left'][class*='ng-not-empty']")
    private List<WebElement> leftColumnList_Checked;
    
    private By leftColumnList_CheckedBy = 
    		By.cssSelector("input[ng-change='model.updateProblemSide(v)'][ng-model$='Left'][class*='ng-not-empty']");
    //------------------------------------------------^    
    
    
    //------------------------------------------------V
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Right']")
    private List<WebElement> rightColumnList;
    
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Right'][class*='ng-empty']")
    private List<WebElement> rightColumnList_Unchecked;
    
    private By rightColumnList_UncheckedBy = 
    		By.cssSelector("input[ng-change='model.updateProblemSide(v)'][ng-model$='Right'][class*='ng-empty']");
    
    @FindBy(css = "input[ng-change='model.updateProblemSide(v)'][ng-model$='Right'][class*='ng-not-empty']")
    private List<WebElement> rightColumnList_Checked;
    
    private By rightColumnList_CheckedBy = 
    		By.cssSelector("input[ng-change='model.updateProblemSide(v)'][ng-model$='Right'][class*='ng-not-empty']");
    //------------------------------------------------^
    
    
    //@FindBy(css = "button[ng-class='settings.buttonClasses'])")
    //@FindBy(xpath = "//button")
    
    //@FindBy(xpath = "//button[not(contains(.,'m'))][contains(.,'cat')]")
    @FindBy(css = "button[ng-disabled='disabled'][ng-class='settings.buttonClasses']")
    
    private List<WebElement> selectLocationCheckboxList;
    
    @FindBy(css = "input[class*='checkboxInput']:not([checked='checked'])")
    private List<WebElement> selectLocationCheckboxList_Unchecked;
    
    private By selectLocationCheckboxList_UncheckedBy = 
    		By.cssSelector("input[class*='checkboxInput']:not([checked='checked'])");
    
    
    @FindBy(css = "input[class*='checkboxInput'][checked='checked']")
    private List<WebElement> selectLocationCheckboxList_Checked;
    
    private By selectLocationCheckboxList_CheckedBy = 
    		By.cssSelector("input[class*='checkboxInput'][checked='checked']");   
    
    
    @FindBy(css = "li[role='presentation'][ng-repeat^='option']")
    private List<WebElement> locationOptionsCheckboxList;
    
    @FindBy(css = "ul[class='dropdown-menu dropdown-menu-form']")
    private WebElement locationsDropdownWindow;

    
    
    @FindBy(css = "button[ng-click^='model.ok']")
    private WebElement okButton;
    
    @FindBy(css = "button[ng-click^='model.cancel']")
    private WebElement cancelButton;
    
    @FindBy(css = "label[simple-popover='Indicated Problems']")
    private WebElement indicatedProblemsLabel;
    
    
    @FindBy(css = "div[ng-click='vm.selectUltraSoundTechniqnue()']")
    private WebElement scanTechniqueCircleButton; 
    
    @FindBy(css = "button[ng-click*='cancelEditing']")
    private WebElement cancelUltrasoundScanTechniqueButton; 
    

    @FindBy(css = "button[ng-click*='model.save']")
    private WebElement saveUltrasoundScanTechniqueButton; 
    
    
    @FindBy(css = "button[ng-click*='model.delete']")
    private WebElement deleteUltrasoundScanTechniqueButton;
    
    
    
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'Scan Technique')]//select")
    private WebElement scanTechniqueDropdown;
    
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'Side')]//select")
    private WebElement sideDropdown;
  
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'From Location')]//select")
    private WebElement fromLocationDropdown;
    
    private By fromLocationDropdownBy = 
    		By.xpath("//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'From Location')]//select"); 
    
    @FindBy(xpath = "//div[contains(., 'From Location')][not(contains(.,'To'))]//button[contains(@ng-class, 'buttonClasses')]")
    private WebElement fromLocationDropdownTP800;
    
    private By fromLocationDropdownTP800By = 
    		By.xpath("//div[contains(., 'From Location')][not(contains(.,'To'))]//button[contains(@ng-class, 'buttonClasses')]"); 

    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'From Distance')]//select")
    private WebElement fromDistanceDropdown;
    
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'To Location')]//select")
    private WebElement toLocationDropdown;
    
    private By toLocationDropdownBy = 
    		By.xpath("//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'To Location')]//select");
    
    @FindBy(xpath = "//div[contains(., 'To Location')][not(contains(.,'From'))]//button[contains(@ng-class, 'buttonClasses')]")
    private WebElement toLocationDropdownTP800;
    
    private By toLocationDropdownTP800By = 
    		By.xpath("//div[contains(., 'To Location')][not(contains(.,'From'))]//button[contains(@ng-class, 'buttonClasses')]");
  
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'To Distance')]//select")
    private WebElement toDistanceDropdown;
      
    @FindBy(xpath = "//div[contains(@ng-repeat, 'UTSCAN')][contains(.,'Type')]//select")
    private WebElement typeDropdown;
    
    @FindBy(css = "button[ng-click^='model.cancel'][class='button button-cancel']")
    private WebElement closeUltrasoundScanTechniqueWindowButton; 
    
    @FindBy(xpath = " //button[.='Yes']")
    private WebElement yesButton;
 
    @FindBy(xpath = " //button[.='No']")
    private WebElement noButton;    
    
    @FindBy(css = "ion-item[ng-click^='model.createNew']")
    private WebElement createNewButton; 
    
    
    private By axillaYes_CheckedBy = 
    		By.cssSelector("input[ng-model*='USRecord'][value='True'][class*='ng-not-empty']");
    private By axillaYes_UncheckedBy = 
    		By.cssSelector("input[ng-model*='USRecord'][value='True'][class*='ng-empty']");
    

    private By axillaNo_CheckedBy = 
    		By.cssSelector("input[ng-model*='USRecord'][value='False'][class*='ng-not-empty']");
    private By axillaNo_UncheckedBy = 
    		By.cssSelector("input[ng-model*='USRecord'][value='False'][class*='ng-empty']");

    @FindBy(css = "textarea[class*='editable-textarea']")
    private WebElement noteTextbox; 
    
    
    
    
    
    @Step(value = "click dropdown menu Reason For Visit")
    public void clickReasonForVisitDropdown() {
    	this.sleep(500);  	
    	Point location =dropdownsSelect.get(0).getLocation();
    	Rectangle size = dropdownsSelect.get(0).getRect();
    	String offsetTop = dropdownsSelect.get(0).getAttribute("offsetTop");	
    	int offset = Integer.parseInt(offsetTop);	
    	this.clickLocationXYusingRobot(location.x + size.width/2, location.y + size.height/2 + offset);   	
    	this.sleep(1000);
    }
    
    @Step(value = "Select an option from dropdown menu  Reason For Visit by option text")
    public void selectOptionFromReasonForVisitDropdownByText(String optionText) {
    	this.sleep(3500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(reasonForVisitDropdown);
    	this.highlightWebElement(reasonForVisitDropdown);
    	this.clickElementFromListByVisibleText(reasonForVisitDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu  Reason For Visit by option index")
    public void selectOptionFromReasonForVisitDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(reasonForVisitDropdown);
    	this.highlightWebElement(reasonForVisitDropdown);
    	this.clickElementFromListByIndex(reasonForVisitDropdown, i);
    	this.sleep(500);
    } 
    
    @Step(value = "Select an option from dropdown menu Infection Control by option text")
    public void selectOptionFromInfectionControlDropdownByText(String optionText) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollDownToElementUsingRobot(infectionControlDropdownBy);
    	this.highlightWebElement(infectionControlDropdown);
    	this.clickElementFromListByVisibleText(infectionControlDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Infection Control  by option index")
    public void selectOptionFromInfectionControlDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollDownToElementUsingRobot(infectionControlDropdownBy);
    	this.highlightWebElement(infectionControlDropdown);
    	this.clickElementFromListByIndex(infectionControlDropdown, i);
    	this.sleep(500);
    }
    
    @Step(value = "Select an option from dropdown menu Views Obtained by option text")
    public void selectOptionFromViewsObtainedDropdownByText(String optionText) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	js.executeScript("window.scrollBy(0,300)");
    	this.scrollElementToCenterOfView(viewsObtainedDropdown);
    	this.highlightWebElement(viewsObtainedDropdown);
    	this.clickElementFromListByVisibleText(viewsObtainedDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Views Obtained  by option index")
    public void selectOptionFromViewsObtainedDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(viewsObtainedDropdown);
    	this.highlightWebElement(viewsObtainedDropdown);
    	this.clickElementFromListByIndex(viewsObtainedDropdown, i);
    	this.sleep(500);
    }


    @Step(value = "Select an option from dropdown sub-menu Side of Views Obtained section by option text")
    public void selectOptionFromSide_ViewsObtainedDropdownByText(String optionText, int i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(sideViewsObtainedDropdownLists.get(i-1));
    	this.highlightWebElement(sideViewsObtainedDropdownLists.get(i-1));
    	this.clickElementFromListByVisibleText(sideViewsObtainedDropdownLists.get(i-1), optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown sub-menu Side of Views Obtained section by option index")
    public void selectOptionFromSide_ViewsObtainedDropdownByIndex(int optionIndex, int  i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(sideViewsObtainedDropdownLists.get(i-1));
    	this.highlightWebElement(sideViewsObtainedDropdownLists.get(i-1));
    	this.clickElementFromListByIndex(sideViewsObtainedDropdownLists.get(i-1), optionIndex);
    	this.sleep(500);
    }
    
    
  
    @Step(value = "Select an option from dropdown sub-menu Type of Views Obtained section by option text")
    public void selectOptionFromType_ViewsObtainedDropdownByText(String optionText, int i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(typeViewsObtainedDropdownLists.get(i-1));
    	this.highlightWebElement(typeViewsObtainedDropdownLists.get(i-1));
    	this.clickElementFromListByVisibleText(typeViewsObtainedDropdownLists.get(i-1), optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown sub-menu Type of Views Obtained section by option index")
    public void selectOptionFromType_ViewsObtainedDropdownByIndex(int optionIndex, int  i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(typeViewsObtainedDropdownLists.get(i-1));
    	this.highlightWebElement(typeViewsObtainedDropdownLists.get(i-1));
    	this.clickElementFromListByIndex(typeViewsObtainedDropdownLists.get(i-1), optionIndex);
    	this.sleep(500);
    }
    
    @Step(value = "Select an option from dropdown menu Procedures by option text")
    public void selectOptionFromProceduresDropdownByText(String optionText) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(proceduresDropdown);
    	this.highlightWebElement(proceduresDropdown);
    	this.clickElementFromListByVisibleText(proceduresDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Procedures  by option index")
    public void selectOptionFromProceduresDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(proceduresDropdown);
    	this.highlightWebElement(proceduresDropdown);
    	this.clickElementFromListByIndex(proceduresDropdown, i);
    	this.sleep(500);
    }
  
    @Step(value = "Select an option from dropdown sub-menu Side of Procedures section by option text")
    public void selectOptionFromSide_ProceduresDropdownByText(String optionText, int i) {    	
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(sideProceduresDropdownLists.get(i-1));
    	this.highlightWebElement(sideProceduresDropdownLists.get(i-1));
    	this.clickElementFromListByVisibleText(sideProceduresDropdownLists.get(i-1), optionText);
    	this.sleep(500);
    }    
     
    @Step(value = "Select an option from dropdown sub-menu Side of Procedures section by option index")
    public void selectOptionFromSideProceduresDropdownByIndex(int optionIndex, int  i) {
    	this.sleep(500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(sideProceduresDropdownLists.get(i-1));
    	this.highlightWebElement(sideProceduresDropdownLists.get(i-1));
    	this.clickElementFromListByIndex(sideProceduresDropdownLists.get(i-1), optionIndex);
    	this.sleep(500);
    }
    
    @Step(value = "click Problems  circle button")
    public void clickProblemsCircleButton() {
    	this.sleep(500);
    	this.highlightWebElement(problemCircleButton);
    	this.waitAllRequests(3);
    	this.clickOverElement(problemCircleButton);
    	this.sleep(500);
    }
    
    @Step(value = "delete all Views Obtained")
    public void deleteAllViewsObtained() {
    	this.scrollUpToElementUsingRobot(viewsObtainedDropdownBy);  	
    	this.sleep(500);
    	int numberOfVews = driver.findElements(deleteViewsButtonsBy).size();
    	if (numberOfVews > 0) {
    		for (int i = 0; i < numberOfVews; i++){
	    		WebElement  deleteButton = driver.findElements(deleteViewsButtonsBy).get(i);    	
	        	this.highlightWebElement(deleteButton);
	        	this.sleep(300);
	        	this.waitAllRequests(1);
	        	this.click(deleteButton);
	        	this.sleep(300);
    		}
    	}
    }

    @Step(value = "add a View Obtained, select Side and Type")
	public void addViewsObtainedByOptionText_AddSide_AddType(String view, String side, String type) {
    	this.sleep(500);
    	this.selectOptionFromViewsObtainedDropdownByText(view);
    	int numberOfViews = driver.findElements(deleteViewsButtonsBy).size();
    	if (numberOfViews > 0) {
    		this.selectOptionFromSide_ViewsObtainedDropdownByText(side, numberOfViews);
    		this.sleep(1000);
    		this.selectOptionFromType_ViewsObtainedDropdownByText(type, numberOfViews);
    	}
	}

    @Step(value = "add a Procedure, select Side")
	public void addProcedureByOptionText_AddSide(String procedure, String side) {
    	this.sleep(500);   	
    	this.selectOptionFromProceduresDropdownByText(procedure);
    	int numberOfProcedures = driver.findElements(deleteProceduresButtonsBy).size();
    	if (numberOfProcedures > 0) {
    		this.selectOptionFromSide_ProceduresDropdownByText(side, numberOfProcedures);
    		this.sleep(1000);
    	}	
	}

    @Step(value = "delete all Procedures")
	public void deleteAllProcedures() {
    	this.sleep(300);
		this.scrollDownToElementUsingRobot(proceduresDropdownBy);
    	int numberOfProcedures = driver.findElements(deleteProceduresButtonsBy).size();
    	if (numberOfProcedures > 0) {
    		for (int i = 0; i < numberOfProcedures; i++){
	    		WebElement  deleteButton = driver.findElements(deleteProceduresButtonsBy).get(i);
	        	this.highlightWebElement(deleteButton);
	        	this.sleep(300);
	        	this.waitAllRequests(1);
	        	this.click(deleteButton);
	        	this.sleep(300);
    		}
    	}   	   	
	}
     
    @Step(value = "Specify Problem, select Unspecified, Left, Right sides, and check Problem's Location ")
	public void selectProblem_UnspecifiedLeftRightSide_checkLocation(String name, String unspecified, String left, String right, String location) {
    	this.sleep(300);
    	int index = -1;
    	this.clickUsingJavascript(indicatedProblemsLabel);
    	for (int i = 0; i < indicatedProblemsList.size(); i++){
    		if (indicatedProblemsList.get(i).getText().toUpperCase().equals(name.toUpperCase())) {
    			index = i;
    			break;
    		} 		
    	}
    	if (unspecified.equals("check")){
	    	this.check_Unspecified(name, index);
	    	this.sleep(300);
    	}
    	if (unspecified.equals("uncheck")){
	    	this.uncheck_Unspecified(name, index);
	    	this.sleep(300);
    	}
    	if (left.equals("check")) {
	    	this.check_Left(name, index);
	    	this.sleep(300);
    	}
    	if (left.equals("uncheck")) {
	    	this.uncheck_Left(name, index);
	    	this.sleep(300);
    	}
    	if (right.equals("check")) {
	    	this.check_Right(name, index);
	    	this.sleep(300);
    	}
    	if (right.equals("uncheck")) {
	    	this.uncheck_Right(name, index);
	    	this.sleep(300);
    	}    	
    	if (right.equals("check") || (left.equals("check"))) {
	    	this.clickLocationDropdown(name, ( index));	    	
	    	this.sleep(150);
	    	this.checkLocationOption(location);  
	    	this.sleep(50);
	    	this.clickLocationDropdown(name, ( index));
    	}  	
	}
    @Step(value = "Specify Problem, select Unspecified, Left, Right sides, and uncheck Problem's Location ")
	public void selectProblem_UnspecifiedLeftRightSide_uncheckLocation(String name, String unspecified, String left, String right, String location) {
    	this.sleep(300);
    	int index = -1;
    	this.clickUsingJavascript(indicatedProblemsLabel);
    	for (int i = 0; i < indicatedProblemsList.size(); i++){
    		if (indicatedProblemsList.get(i).getText().toUpperCase().equals(name.toUpperCase())) {
    			index = i;
    			break;
    		} 		
    	}
    	if (unspecified.equals("check")){
	    	this.check_Unspecified(name, index);
	    	this.sleep(300);
    	}
    	if (unspecified.equals("uncheck")){
	    	this.uncheck_Unspecified(name, index);
	    	this.sleep(300);
    	}
    	if (left.equals("check")) {
	    	this.check_Left(name, index);
	    	this.sleep(300);
    	}
    	if (left.equals("uncheck")) {
	    	this.uncheck_Left(name, index);
	    	this.sleep(300);
    	}
    	if (right.equals("check")) {
	    	this.check_Right(name, index);
	    	this.sleep(300);
    	}
    	if (right.equals("uncheck")) {
	    	this.uncheck_Right(name, index);
	    	this.sleep(300);
    	}   	
    	if (right.equals("check") || (left.equals("check"))) {
	    	this.clickLocationDropdown(name, ( index));	    	
	    	this.sleep(150);
	    	this.uncheckLocationOption(location);  
	    	this.sleep(50);
	    	this.clickLocationDropdown(name, ( index));
    	}
	}
    
	@Step(value = "Check: Left for Problem: {name}")
    public void check_Left(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);
        this.clickCheckboxDependingOnAttributeState(
        		leftColumnList.get(i), "class", "ng-empty");
    }
	
	@Step(value = "Uncheck: Left for Problem: {name}")
    public void uncheck_Left(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);
        this.clickCheckboxDependingOnAttributeState(
        		leftColumnList.get(i), "class", "ng-not-empty");
    } 
	
	
	@Step(value = "Check: Right for Problem: {name}")
    public void check_Right(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);
        this.clickCheckboxDependingOnAttributeState(
        		rightColumnList.get(i), "class", "ng-empty");
    }
	
	@Step(value = "Uncheck: Right for Problem: {name}")
    public void uncheck_Right(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);
        this.clickCheckboxDependingOnAttributeState(
        		rightColumnList.get(i), "class", "ng-not-empty");
    } 
		
	@Step(value = "Check: Unspecified for Problem: {name}")
    public void check_Unspecified(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);
        this.clickCheckboxDependingOnAttributeState(
        		unspecifiedColumnList.get(i), "class", "ng-empty");
    }
	
	@Step(value = "Uncheck: Unspecified for Problem: {name}")
    public void uncheck_Unspecified(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);       
		this.clickCheckboxDependingOnAttributeState(  		
        		unspecifiedColumnList.get(i), "class", "ng-not-empty");
    } 	
	
	@Step(value = "click Location dropdown for Problem: {name}")
    public void clickLocationDropdown(String name, int i)  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.clickOverElement(selectLocationCheckboxList.get(i));
    	this.scrollElementToCenterOfView(selectLocationCheckboxList.get(i));
    }
	
	@Step(value = "check Location option: {option}")
    public void checkLocationOption(String option)  {
    	this.sleep(150);
    	this.clickUsingJavascript(locationsDropdownWindow); 		  	
    	String checkBoxLocator =  "//span[text() = \"" + option + "\"]/../../input";
        By locationOptionCheckboxBy = By.xpath(checkBoxLocator);    
        WebElement locationOptionCheckbox = driver.findElement(locationOptionCheckboxBy); 
        this.clickCheckboxIfAttributeIsNotPresent(locationOptionCheckbox, "checked");
        this.sleep(50);
    }
	
	@Step(value = "uncheck Location option: {option}")
    public void uncheckLocationOption(String option)  {
    	this.sleep(150);
    	this.clickUsingJavascript(locationsDropdownWindow); 	   	  	
    	String checkBoxLocator =  "//span[text() = \"" + option + "\"]/../../input";
        By locationOptionCheckboxBy = By.xpath(checkBoxLocator);    
        WebElement locationOptionCheckbox = driver.findElement(locationOptionCheckboxBy);        
        this.clickCheckboxIfAttributeIsPresent(locationOptionCheckbox, "checked");   
        this.sleep(50);
    }
	
	
	@Step(value = "click OK button")
    public void clickOKButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(okButton);
        this.click(okButton);
    }
	@Step(value = "click Cancel button")
    public void clickCancelButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(cancelButton);
        this.click(cancelButton);
    }
	
	@Step(value = "click Scan Technique button")
    public void clickScanTechniqueButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(scanTechniqueCircleButton);
        this.click(scanTechniqueCircleButton);
    }
    
	@Step(value = "click Cancel Ultrasound Scan Technique button")
    public void clickCancelScanTechniqueButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(cancelUltrasoundScanTechniqueButton);
    	this.highlightWebElement(cancelUltrasoundScanTechniqueButton);
        this.click(cancelUltrasoundScanTechniqueButton);
    }
	
	@Step(value = "click Save Ultrasound Scan Technique button")
    public void clickSaveScanTechniqueButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(saveUltrasoundScanTechniqueButton);
    	this.highlightWebElement(saveUltrasoundScanTechniqueButton);
        this.click(saveUltrasoundScanTechniqueButton);
    }
	
	@Step(value = "click Delete Ultrasound Scan Technique button")
    public void clickDeleteScanTechniqueButton()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
    	this.scrollElementToCenterOfView(deleteUltrasoundScanTechniqueButton);
    	this.highlightWebElement(deleteUltrasoundScanTechniqueButton);
        this.click(deleteUltrasoundScanTechniqueButton);
    }


	//Ultrasound Scan technique functions
	
    @Step(value = "Select an option from dropdown menu  Scan Technique by option text")
    public void selectOptionFromScanTechniqueDropdownByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(scanTechniqueDropdown);
    	this.clickUsingJavascript(scanTechniqueDropdown);
    	this.highlightWebElement(scanTechniqueDropdown);
    	this.clickElementFromListByVisibleText(scanTechniqueDropdown, optionText);
    	this.clickUsingJavascript(scanTechniqueDropdown);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu  Scan Technique by option index")
    public void selectOptionFromScanTechniqueDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(scanTechniqueDropdown);
    	this.clickUsingJavascript(scanTechniqueDropdown);
    	this.highlightWebElement(scanTechniqueDropdown);
    	this.clickElementFromListByIndex(scanTechniqueDropdown, i);
    	this.clickUsingJavascript(scanTechniqueDropdown);
    	this.sleep(500);
    } 
    
    @Step(value = "Select an option from dropdown menu  Side by option text")
    public void selectOptionFromSideDropdownByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(sideDropdown);
    	this.clickUsingJavascript(sideDropdown);
    	this.highlightWebElement(sideDropdown);
    	this.clickElementFromListByVisibleText(sideDropdown, optionText);
    	this.clickUsingJavascript(sideDropdown);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu  Side by option index")
    public void selectOptionFromSideDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(sideDropdown);
    	this.clickUsingJavascript(sideDropdown);
    	this.highlightWebElement(sideDropdown);
    	this.clickElementFromListByIndex(sideDropdown, i);
    	this.clickUsingJavascript(sideDropdown);
    	this.sleep(500);
    } 
    
    
    @Step(value = "Select an option from dropdown menu  From Location by option text")
    public void selectOptionFrom_FromLocationDropdown_ByText(String optionText) throws Exception {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	if (driver.findElements(fromLocationDropdownBy).size() > 0) {
        	this.scrollElementToCenterOfView(fromLocationDropdown);
        	this.click(fromLocationDropdown);
        	this.highlightWebElement(fromLocationDropdown);
        	this.clickElementFromListByVisibleText(fromLocationDropdown, optionText);
        	this.click(fromLocationDropdown);
        	this.sleep(500);
    	}
    	else if (driver.findElements(fromLocationDropdownTP800By).size() > 0) {
        	this.scrollElementToCenterOfView(fromLocationDropdownTP800);
        	this.click(fromLocationDropdownTP800);
        	this.highlightWebElement(fromLocationDropdownTP800);
        	this.checkLocationOption(optionText);
        	this.click(fromLocationDropdownTP800);
        	this.sleep(500);
    	}
    	else {
    		throw new Exception ("From Location menu is not found");
    	}

    }    
    
    @Step(value = "Select an option from dropdown menu  From Location by option index")
    public void selectOptionFrom_FromLocationDropdown_ByIndex(int  i) throws Exception {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	if (driver.findElements(fromLocationDropdownBy).size() > 0) {
        	this.scrollElementToCenterOfView(fromLocationDropdown);
        	this.click(fromLocationDropdown);
        	this.highlightWebElement(fromLocationDropdown);
        	this.clickElementFromListByIndex(fromLocationDropdown, i);
        	this.click(fromLocationDropdown);
        	this.sleep(500);
    	}
    	else {
    		throw new Exception ("Please use 'selectOptionFrom_FromLocationDropdown_ByText' to test Ultrasound Locations in techpad 8.0  instead");
    	}
    } 
    
    @Step(value = "Select an option from dropdown menu  From Distance by option text")
    public void selectOptionFrom_FromDistanceDropdown_ByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(fromDistanceDropdown);
    	this.clickUsingJavascript(fromDistanceDropdown);
    	this.highlightWebElement(fromDistanceDropdown);
    	this.clickElementFromListByVisibleText(fromDistanceDropdown, optionText);
    	this.clickUsingJavascript(fromDistanceDropdown);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu  From Distance by option index")
    public void selectOptionFrom_FromDistanceDropdown_ByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(fromDistanceDropdown);
    	this.clickUsingJavascript(fromDistanceDropdown);
    	this.highlightWebElement(fromDistanceDropdown);
    	this.clickElementFromListByIndex(fromDistanceDropdown, i);
    	this.clickUsingJavascript(fromDistanceDropdown);
    	this.sleep(500);
    } 
    
    @Step(value = "Select an option from dropdown menu To Location by option text")
    public void selectOptionFromToLocationDropdownByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	if (driver.findElements(toLocationDropdownBy).size() > 0) {
        	this.scrollElementToCenterOfView(toLocationDropdown);
        	this.click(toLocationDropdown);
        	this.highlightWebElement(toLocationDropdown);
        	this.clickElementFromListByVisibleText(toLocationDropdown, optionText);
        	this.click(toLocationDropdown);
        	this.sleep(500);
    	}
    	else if (driver.findElements(toLocationDropdownTP800By).size() > 0) {
        	this.scrollElementToCenterOfView(toLocationDropdownTP800);
        	this.click(toLocationDropdownTP800);
        	this.highlightWebElement(toLocationDropdownTP800);
        	this.checkLocationOption(optionText);
        	this.click(toLocationDropdownTP800);
        	this.sleep(500);
    	}
    }    
    
    @Step(value = "Select an option from dropdown menu To Location by option index")
    public void selectOptionFromToLocationDropdownByIndex(int  i) throws Exception {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	if (driver.findElements(toLocationDropdownBy).size() > 0) {
        	this.scrollElementToCenterOfView(toLocationDropdown);
        	this.clickUsingJavascript(toLocationDropdown);
        	this.highlightWebElement(toLocationDropdown);
        	this.clickElementFromListByIndex(toLocationDropdown, i);
        	this.clickUsingJavascript(toLocationDropdown);
        	this.sleep(500);
    	}
    	else {
    		throw new Exception ("Please use 'selectOptionFrom_ToLocationDropdown_ByText' to test Ultrasound Locations in techpad 8.0  instead");
    	}
    } 
    
    @Step(value = "Select an option from dropdown menu To Distance by option text")
    public void selectOptionFromToDistanceDropdownByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(toDistanceDropdown);
    	this.clickUsingJavascript(toDistanceDropdown);
    	this.highlightWebElement(toDistanceDropdown);
    	this.clickElementFromListByVisibleText(toDistanceDropdown, optionText);
    	this.clickUsingJavascript(toDistanceDropdown);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu To Distance by option index")
    public void selectOptionFromToDistanceDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(toDistanceDropdown);
    	this.clickUsingJavascript(toDistanceDropdown);
    	this.highlightWebElement(toDistanceDropdown);
    	this.clickElementFromListByIndex(toDistanceDropdown, i);
    	this.clickUsingJavascript(toDistanceDropdown);
    	this.sleep(500);
    }    

    @Step(value = "Select an option from dropdown menu Type by option text")
    public void selectOptionFromTypeDropdownByText(String optionText) {
    	this.sleep(1500);
    	this.waitAllRequests(3);
    	this.scrollElementToCenterOfView(typeDropdown);
    	this.clickUsingJavascript(typeDropdown);
    	this.highlightWebElement(typeDropdown);
    	this.clickElementFromListByVisibleText(typeDropdown, optionText);
    	this.clickUsingJavascript(typeDropdown);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Type by option index")
    public void selectOptionFromTypeDropdownByIndex(int  i) {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.scrollElementToCenterOfView(typeDropdown);
    	this.clickUsingJavascript(typeDropdown);
    	this.highlightWebElement(typeDropdown);
    	this.clickElementFromListByIndex(typeDropdown, i);
    	this.clickUsingJavascript(typeDropdown);
    	this.sleep(500);
    } 	
    
    @Step(value = "click Close Ultrasound Scan Technique Button")
    public void clickCloseUltrasoundScanTechniqueButton (){
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(closeUltrasoundScanTechniqueWindowButton);
    	this.click(closeUltrasoundScanTechniqueWindowButton);
    	this.sleep(1500);
    } 	
    
    @Step(value = "click Yes Button")
    public void clickYesButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(yesButton);
    	this.click(yesButton);
    	this.sleep(1500);
    } 	
    
    @Step(value = "click No Button")
    public void clickNoButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(noButton);
    	this.click(noButton);
    	this.sleep(1500);
    } 	   
       
    @Step(value = "click Create New Button")
    public void clickCreateNewTechniqueButton() {
    	this.sleep(500);
    	this.waitAllRequests(1);
    	this.highlightWebElement(createNewButton);
    	this.click(createNewButton);
    	this.sleep(1500);
    } 	   
  
    @Step(value = "Check: Axilla Yes")
    public void check_axillaYes_buttonClick()
    {
        this.checkCheckbox(
        		axillaYes_UncheckedBy);
    }
    @Step(value = "Uncheck: Axilla Yes")
    public void uncheck_axillaYes_buttonClick()
    {
        this.uncheckCheckbox(
        		axillaYes_CheckedBy);
    }
    
    @Step(value = "Check: Axilla No")
    public void check_axillaNo_buttonClick()
    {
        this.checkCheckbox(
        		axillaNo_UncheckedBy);
    }
    @Step(value = "Uncheck: Axilla No")
    public void uncheck_axillaNo_buttonClick()
    {
        this.uncheckCheckbox(
        		axillaNo_CheckedBy);
    }
    
    @Step(value = "Enter Text into Note Ultrasound Scan Technique textbox")
    public void enterTextIntoNoteTextbox (String text) {
    	this.enterText(noteTextbox, text);
    }
    
    
    
	
}