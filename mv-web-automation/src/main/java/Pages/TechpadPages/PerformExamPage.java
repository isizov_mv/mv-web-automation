package Pages.TechpadPages;


import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class PerformExamPage extends BasePage {

	public PerformExamPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "li[ng-click*='tabs.Exam']")
    private WebElement examTab;
    
    @FindBy(css = "li[ng-click*='tabs.SurveyHistory']")
    private WebElement surveyHistoryTab;
    
    @FindBy(css = "li[ng-click*='tabs.Diagram']")
    private WebElement diagramTab;
    
    @FindBy(css = "li[ng-click*='tabs.Comments']")
    private WebElement commentsTab;
    
    @FindBy(css = "li[ng-click*='tabs.RiskAssessments']")
    private WebElement riskTab;
    
    @FindBy(css = "li[ng-click*='tabs.FollowUps']")
    private WebElement followUpsTab;
    
    @FindBy(css = "li[ng-click*='tabs.Documents']")
    private WebElement documentsTab;
    
    
	@Step(value = "click Exam tab")
    public void clickExamTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(examTab);
    	this.sleep(1000);
    }
    
	@Step(value = "click Survey HistoryTab tab")
    public void clickSurveyHistoryTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(surveyHistoryTab);
    	this.sleep(1000);
    }
	
	@Step(value = "click Diagram Tab tab")
    public void clickDiagramTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(diagramTab);
    	this.sleep(1000);
    }
	
	@Step(value = "click Comments Tab tab")
    public void clickCommentsTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(commentsTab);
    	this.sleep(1000);
    }
	
	@Step(value = "click Risk Tab tab")
    public void clickRiskTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(riskTab);
    	this.sleep(1000);
    }
    
	@Step(value = "click Follow Ups Tab tab")
    public void clickFollowUpsTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(followUpsTab);
    	this.sleep(1000);
    }
	
	@Step(value = "click Documents Tab tab")
    public void clickDocumentsTab()  {
    	this.waitAllRequests(1);
    	this.sleep(150);	
        this.click(documentsTab);
    	this.sleep(1000);
    }
	
	
	
	
	
}

