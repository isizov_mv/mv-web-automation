package Pages.PatientsPortal520Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

public class AdminPortalConfigTechpadSettingsPage52 extends BasePage {
    public AdminPortalConfigTechpadSettingsPage52(WebDriver driver) {
        super(driver);
    }
	
	
    private By enableSurveyHistoryMode_CheckedBy = 
    		By.cssSelector("input[data-ng-model$='ISurveyHistoryEnabled'][class*='ng-not-empty']");
    private By enableSurveyHistoryMode_UncheckedBy = 
    		By.cssSelector("input[data-ng-model$='ISurveyHistoryEnabled'][class*='ng-empty']");
    
  
    
    @FindBy(css = ("button[id='saveBtn']"))
    private WebElement saveButton;  
    
    @FindBy(xpath = ("//label[contains(.,'Enable survey history mode')]"))
    private WebElement enableSurveyHistoryModeButton;  
    
    
    @Step(value = "Check: Enable survey history mode")
    public void check_EnableSurveyHistoryMode()
    {
    	//this.sleep(1500);
    	this.waitAllRequests();
    	this.scrollElementToCenterOfView(enableSurveyHistoryModeButton);
    	this.highlightWebElement(enableSurveyHistoryModeButton);
    	this.sleep(1000);
        this.checkCheckbox(
        		enableSurveyHistoryMode_UncheckedBy);
        this.sleep(1000);
    }
    @Step(value = "Uncheck: Enable survey history mode")
    public void uncheck_EnableSurveyHistoryMode()
    {
        this.uncheckCheckbox(
        		enableSurveyHistoryMode_CheckedBy);
    }
    		
    @Step(value = "Change state and return it back for:  Enable survey history mode")
    public void doOnOff_EnableSurveyHistoryMode()
    {
        this.changeCheckboxStateThenRestoreItBack(
        		enableSurveyHistoryMode_CheckedBy,
        		enableSurveyHistoryMode_UncheckedBy);
    }
    
    @Step(value = "Click Save Button")
    public void clickSaveButton()
    {
    	this.click(saveButton);
        this.sleep(1000);
    }
    

    
    
	
	

}
