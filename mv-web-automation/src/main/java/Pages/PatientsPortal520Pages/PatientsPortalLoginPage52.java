package Pages.PatientsPortal520Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

public class PatientsPortalLoginPage52 extends BasePage {

    public PatientsPortalLoginPage52(WebDriver driver) {
        super(driver);
    }
	
    @FindBy(css = "input[id='UserName']")
    private WebElement userName;

    @FindBy(css = "input[id*='Password']")
    private WebElement password;

    @FindBy(css = "input[id*='loginBtn']")
    private WebElement loginButton;
    
    
    @Step(value = "Open Patient Portal 5.20 Login page")
    public void gotoLoginPage(String portalURL)
    {
        driver.get(portalURL);
    }
    @Step(value = "Enter User Name")
    public void enterUserName(String UseNameText)
    {
        this.clickOverElement(userName);
        this.enterText(userName, UseNameText);
        this.sleep(2000);
    }
    @Step(value = "Enter Password")
    public void enterPassword(String passwordText)
    {
        this.clickOverElement(password);
        this.enterText(password, passwordText);
        this.sleep(2000);
    }
    @Step(value = "Press Login Button")
    public void clickLoginButton()
    {
    	this.click(loginButton);
        this.sleep(7000);
    }
    @Step(value = "Login to Patient Portal")
    public void loginToPatientPortal(String portalURL, String userName, String password){
        this.gotoLoginPage(portalURL);
        this.enterUserName(userName);
        this.enterPassword(password);
        this.clickLoginButton();
    }
	
	
	

}
