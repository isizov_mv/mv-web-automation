package Pages.PatientsPortal520Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

public class PatientsPortalAdminPage52 extends BasePage{
    public PatientsPortalAdminPage52(WebDriver driver) {
        super(driver);
    }
	
	
    @FindBy(xpath = "//a[contains(.,'Portal Configuration')]")
    private WebElement portalConfigurationButton;
    
    @FindBy(xpath = "//a[contains(.,'TechPad')][contains(.,'Settings')]")
    private WebElement techpadSettingsButton;
  
    

    
    @Step(value = "Press Portal Configuration Button")
    public void clickPortalConfigurationButton()
    {
        this.click(portalConfigurationButton);
        this.sleep(1000);
    }

    @Step(value = "Press Techpad Settings Button")
    public void clickTechpadSettingsButton()
    {
        this.click(techpadSettingsButton);
        this.sleep(1000);
    }
	
	

}
