package Pages.PatientsPortal520Pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

public class PatientsPortalHomePage52 extends BasePage{
    public PatientsPortalHomePage52(WebDriver driver) {
        super(driver);
    }
	
	
	String homePagePortalTitle = "Patients Portal";

    @FindBy(css = "i[class='fa fa-user']")
    private WebElement greyDropdownButton;

    @FindBy(css = "a[class='dropdown-link'][href='/Account/LogOff']")
    private List<WebElement> logoutLocators;   
    //private WebElement logoutButton = logoutLocators.get(0);

    @FindBy(css = "a[href*='admin']")
    private WebElement adminButton;



    @Step(value = "Press Admin Button")
    public void clickAdminButton()
    {
    	this.waitAllRequests();
        this.click(adminButton);
        this.sleep(4000);
    }



    @Step(value = "Press User Dropdown Button")
    public void clickGreyDropdownButton()
    {
        this.click(greyDropdownButton);
        this.sleep(1000);
    }

    @Step(value = "Press Logout Button")
    public void clickLogoutButton()
    {
    	this.click(greyDropdownButton);
        this.sleep(1000);
        //this.selectOptionFromWelcomeAsiDropdownByText("Logout");
        this.click(logoutLocators.get(0));
        this.sleep(3000);
    }

    @Step(value = "Verify Login is successful")
    public boolean isLoginSuccessful(){
        return this.driver.getTitle().contains(homePagePortalTitle);
    }
    
    
    @Step(value = "Select an option from dropdown menu Welcome Asi by option text")
    public void selectOptionFromWelcomeAsiDropdownByText(String optionText) {
    	this.sleep(500);
    	this.clickElementFromListByVisibleText(greyDropdownButton, optionText);
    	this.sleep(500);
    }    

}
