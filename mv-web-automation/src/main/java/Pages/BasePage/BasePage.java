package Pages.BasePage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.util.List;


public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected  JavascriptExecutor js;
    private int waitingTime = 20; //sec
    
    public BasePage(WebDriver driver){
        this.driver = driver;       
        wait = new WebDriverWait(driver, waitingTime);
        js = (JavascriptExecutor) driver;
    }
    

    //*** JSWAITER functions ********************************//
    private void ajaxComplete() {
        js.executeScript("var callback = arguments[arguments.length - 1];"
                + "var xhr = new XMLHttpRequest();" + "xhr.open('GET', '/Ajax_call', true);"
                + "xhr.onreadystatechange = function() {" + "  if (xhr.readyState == 4) {"
                + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
    }
    private void waitForJQueryLoad() {
        try {
            ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) this.wait)
                    .executeScript("return jQuery.active") == 0);

            boolean jqueryReady = (Boolean) js.executeScript("return jQuery.active==0");

            if (!jqueryReady) {
                wait.until(jQueryLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }
    private void waitForAngularLoad() {
        String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
        angularLoads(angularReadyScript);
    }
    private void waitUntilJSReady() {
        try {
            ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) this.wait)
                    .executeScript("return document.readyState").toString().equals("complete");

            boolean jsReady = js.executeScript("return document.readyState").toString().equals("complete");

            if (!jsReady) {
                wait.until(jsLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }
    private void waitUntilJQueryReady() {
        Boolean jQueryDefined = (Boolean) js.executeScript("return typeof jQuery != 'undefined'");
        if (jQueryDefined) {
            sleep(waitingTime);
            waitForJQueryLoad();
            sleep(waitingTime);
        }
    }
    private void waitForAngular5Load() {
        String angularReadyScript = "return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1";
        angularLoads(angularReadyScript);
    }
    private void angularLoads(String angularReadyScript) {
        try {
        	ExpectedCondition<Boolean> angularLoad = driver -> Boolean.valueOf(
        			js.executeScript(angularReadyScript).toString());
        	
            boolean angularReady = Boolean.valueOf(js.executeScript(angularReadyScript).toString());

            if (!angularReady) {
                wait.until(angularLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }

    protected void waitUntilAngularReady() {
        try {
            Boolean angularUnDefined = (Boolean) js.executeScript("return window.angular === undefined");
            if (!angularUnDefined) {
                Boolean angularInjectorUnDefined = (Boolean) js.executeScript("return angular.element(document).injector() === undefined");
                if (!angularInjectorUnDefined) {
                    sleep(waitingTime);
                    waitForAngularLoad();
                    sleep(waitingTime);
                }
            }
        } catch (WebDriverException ignored) {
        }
    }
    protected void waitUntilAngular5Ready() {
        try {
            Object angular5Check = js.executeScript("return getAllAngularRootElements()[0].attributes['ng-version']");
            if (angular5Check != null) {
                Boolean angularPageLoaded = (Boolean) js.executeScript("return window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1");
                if (!angularPageLoaded) {
                    sleep(waitingTime);
                    waitForAngular5Load();
                    sleep(waitingTime);
                }
            }
        } catch (WebDriverException ignored) {
        }
    }
    protected void waitAllRequests() {
        waitUntilJSReady();
        ajaxComplete();
        waitUntilJQueryReady();
        waitUntilAngularReady();
        waitUntilAngular5Ready();
    }
    
    protected void waitAllRequests(int customWaitTime) {
    	wait = new WebDriverWait(driver, customWaitTime);
        waitUntilJSReady();
        ajaxComplete();
        waitUntilJQueryReady();
        waitUntilAngularReady();
        waitUntilAngular5Ready();
        wait = new WebDriverWait(driver, waitingTime);
    }

    /**
     * Method to make sure a specific element has loaded on the page
     * @param by
     * @param expected
     */
    protected void waitForElementsAreComplete(By by, int expected) {
        ExpectedCondition<Boolean> angularLoad = driver -> {
            int loadingElements = this.driver.findElements(by).size();
            return loadingElements >= expected;
        };
        wait.until(angularLoad);
    }

    /**
     * Waits for the elements animation to be completed
     * @param css
     */
    protected void waitForAnimationToComplete(String css) {
        ExpectedCondition<Boolean> angularLoad = driver -> {
            int loadingElements = this.driver.findElements(By.cssSelector(css)).size();
            return loadingElements == 0;
        };
        wait.until(angularLoad);
    }

    protected void sleep(long milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //******************************** JSWAITER functions ***//


    protected void hoverByAction (WebElement element) {
        this.waitAllRequests();
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

    protected void click (By by) {
        this.waitAllRequests(2);
        this.scrollElementToCenterOfView(driver.findElement(by)); 
        //Explicit wait
        wait.until(ExpectedConditions.elementToBeClickable(by)).click();
    }

    protected void click (WebElement element) {
        this.waitAllRequests(5);
        this.scrollElementToCenterOfView(element);        
        //Explicit wait
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }
    protected void clickUsingJavascript (WebElement element) {
        this.scrollElementToCenterOfView(element);        
       js.executeScript("arguments[0].click();", element);
    }

    //Close popup if exists
    protected void handlePopup (By by) throws InterruptedException {
        this.waitAllRequests();

        List<WebElement> popup = driver.findElements(by);
        if(!popup.isEmpty()){
            popup.get(0).click();
            this.sleep(300);
        }
    }

    protected void waitUntilPageLoaded (WebDriver webdriver)
    {
        try {
            WebDriverWait wait = new WebDriverWait(webdriver, 10);
            wait.until(ExpectedConditions.jsReturnsValue(
                    "return window.jQuery != undefined && jQuery.active == 0 && document.readyState == \"complete\""));
        } catch (Exception e)
        {
            //don't do anything
        }
    }

    protected void hover_to(WebElement element)
    {
        this.sleep(1000);
        this.waitAllRequests();
        Actions actionBuilder = new Actions(this.driver);
        actionBuilder.moveToElement(element).perform();

    }

    protected void hover_to_by_offset_and_click(int X, int Y) {
        this.sleep(1000);
        this.waitAllRequests();
        Actions actionBuilder = new Actions(this.driver);
        actionBuilder.moveByOffset(X, Y).click().perform();
    }

    protected void highlightWebElement (WebElement element)
    {
    	String attr = element.getAttribute("style");
        js.executeScript("arguments[0].setAttribute('style', 'border: 3px solid red;');", element);
        this.sleep(700);
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, attr);
    }

    //if there is another element overlaying  the element
    //and receiving click
    //then use this function
    protected void clickOverElement (WebElement element)
    {
        Actions actionBuilder = new Actions(this.driver);
        actionBuilder.moveToElement(element).click().perform();
    }



    
       
    protected void scrollAboveElement(WebDriver webDriver, WebElement element){
		try {
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			js.executeScript("window.scrollBy(0, -100)");
		}
		catch (org.openqa.selenium.NoSuchElementException e) {
		}
	}
  
    protected void scrollToAndHighlightElement(WebElement element){
    	try {
    		boolean found = (Boolean)js.executeScript(
					"var elem = arguments[0],                 " +
							"  box = elem.getBoundingClientRect(),    " +
							"  cx = box.left + box.width / 2,         " +
							"  cy = box.top + box.height / 2,         " +
							"  e = document.elementFromPoint(cx, cy); " +
							"  for (; e; e = e.parentElement) {         " +
							"  if (e === elem)                        " +
							"    return true;                         " +
							"}                                        " +
							"return false;                            "
					, element);

			if(!found)
				scrollAboveElement(this.driver, element);
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
        }
    }
    
    protected void scrollElementToCenterOfView( WebElement element){
    	String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
    			+ "var elementTop = arguments[0].getBoundingClientRect().top;"
    			+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";
    	js.executeScript(scrollElementIntoMiddle, element);    
    }
    
    protected void waitForVisibilityOfElement (WebElement element) {
    	this.waitAllRequests();
    	this.wait.until(ExpectedConditions.visibilityOf(element));    	
    }

    
    protected void waitUntilElementIsNoLongerDisplayed (By elementBy) {
    	this.waitAllRequests();
    	wait.until(ExpectedConditions.invisibilityOfElementLocated(elementBy));
    }

   
    protected boolean isElementPresentBy(By by) {
    	this.sleep(150);
  		this.waitAllRequests(1);
        if (this.driver.findElements(by).size() > 0) {
        	return true;
        }
        return false;
    }   
    
    protected void changeCheckboxStateThenRestoreItBack (By boxCheckedBy, By boxUncheckedBy) {
    	this.sleep(500);
    	if (this.isElementPresentBy(boxCheckedBy) == true) {
    		
        	this.scrollElementToCenterOfView(this.driver.findElement(boxCheckedBy));
        	this.clickOverElement(this.driver.findElement(boxCheckedBy));
        	this.sleep(500);
        	this.clickOverElement(this.driver.findElement(boxUncheckedBy));
    	}
    	else if (this.isElementPresentBy(boxUncheckedBy) == true) {
        	this.scrollElementToCenterOfView(this.driver.findElement(boxUncheckedBy));
        	this.clickOverElement(this.driver.findElement(boxUncheckedBy));
        	this.sleep(500);
        	this.clickOverElement(this.driver.findElement(boxCheckedBy));    		   		
    	}
        this.sleep(500);  	
    }

    protected void checkCheckbox(By boxUncheckedBy)
    {
    	this.sleep(500);
    	if (this.isElementPresentBy(boxUncheckedBy) == true) {
        	this.scrollElementToCenterOfView(this.driver.findElement(boxUncheckedBy));
        	this.clickOverElement(this.driver.findElement(boxUncheckedBy));
    	}
        this.sleep(150);
    }
    
    protected void uncheckCheckbox(By boxCheckedBy)
    {
    	this.sleep(500);
    	if (this.isElementPresentBy(boxCheckedBy) == true) {
        	this.scrollElementToCenterOfView(this.driver.findElement(boxCheckedBy));
        	this.clickOverElement(this.driver.findElement(boxCheckedBy));
    	}
        this.sleep(150);
    }     
    
    protected void clickCheckboxDependingOnAttributeState( WebElement checkbox, String attributeName, String attributeValue)
    {
    	this.sleep(50);
    	if (checkbox.getAttribute(attributeName).toUpperCase().contains(attributeValue.toUpperCase())) {
        	this.scrollElementToCenterOfView(checkbox);
        	this.click(checkbox);
    	}
        this.sleep(50);
    } 
     
    protected void clickCheckboxIfAttributeIsNotPresent( WebElement checkbox, String attributeName)
    {
    	this.sleep(50);
    	if (checkbox.getAttribute(attributeName) == null) {
        	this.scrollElementToCenterOfView(checkbox);
        	this.clickOverElement(checkbox);
    	}
        this.sleep(50);
    } 
    protected void clickCheckboxIfAttributeIsPresent( WebElement checkbox, String attributeName)
    {
    	this.sleep(50);
    	if (checkbox.getAttribute(attributeName) != null) {
        	this.scrollElementToCenterOfView(checkbox);
        	
        	this.clickOverElement(checkbox);
    	}
        this.sleep(50);
    } 
    
    
    
    
    protected void uncheckCheckbox( WebElement boxChecked)
    {
    	this.sleep(150);
    	if (boxChecked.isDisplayed() == true) {
        	this.scrollElementToCenterOfView(boxChecked);
        	this.click(boxChecked);
    	}
        this.sleep(150);
    }
    
    
    
    
    
    protected List<WebElement> getDropdownOptions (WebElement dropdownElement){
    	Select dropdown = new Select (dropdownElement);
    	return dropdown.getOptions();
    }
      
    private String generateErrorMessage (String text) {
    	String method = Thread.currentThread().getStackTrace()[3].toString().split("select")[1].split("[(]")[0];
    	method = method.replaceAll("(?!^)([A-Z])", " $1");
    	return "The element text \"" + text + "\" was not found with a " + method.toLowerCase() + ".";    	
    }

    protected void clickElementFromListByExactOptionText(List<WebElement> elementsList, String elementText) {
    	boolean optionNotFound = true;
    	for (WebElement optionElement : elementsList) {
    		if (this.isElementDisplayed(optionElement) &&
    				optionElement.getText().toUpperCase().equals(elementText.toUpperCase())) {
    			Actions action = new Actions(this.driver);
    			action.click(optionElement).perform();
    			optionNotFound = false;
    		}
    	}
    	if (optionNotFound) throw new org.openqa.selenium.NoSuchElementException(generateErrorMessage(elementText));
    }
    
    protected void clickElementFromListByPartialOptionText(List<WebElement> elementsList, String elementText) {
    	boolean optionNotFound = true;
    	for (WebElement optionElement : elementsList) {
    		if (this.isElementDisplayed(optionElement) &&
    				optionElement.getText().toUpperCase().contains(elementText.toUpperCase())) {
    			Actions action = new Actions(this.driver);
    			action.click(optionElement).perform();
    			optionNotFound = false;
    		}
    	}
    	if (optionNotFound) throw new org.openqa.selenium.NoSuchElementException(generateErrorMessage(elementText));
    }
    
    protected int getRowIndexFromColumnListByPartialText(List<WebElement> elementsList, String text) {
    	int i = 0;
    	for (WebElement row : elementsList) {
    		if (row.getText().trim().toUpperCase().contains(text.trim().toUpperCase())){    					
        		return i;
    		}
    		i++;    		
    	}
    	//System.out.println("row not found... " + String.valueOf(i));		
    	return -1;
    }
    
    protected int getRowIndexFromColumnListByExactText(List<WebElement> elementsList, String text) {
    	int i = 0;
    	for (WebElement row : elementsList) {
    		if (row.getText().equals(text)){    			
    	    	//System.out.println("row found...");			
        		return i;
    		}
    		i++;    		
    	}
    	return -1;
    }
    

    
    
    
    
    protected void clickElementFromListByIndex(List<WebElement> elementsList, int elementNumber) {
    	this.sleep(500);
    	this.click(elementsList.get(elementNumber));
    	this.sleep(500);
    }
    
    
    protected void clickElementFromListByIndex(WebElement selectElement, int optionNumber) {
    	this.sleep(500);
    	Select dropdownOptions = new Select(selectElement);
    	dropdownOptions.selectByIndex(optionNumber);
    	this.sleep(500);
    }
      
    protected void doubleclickElementFromListByOptionText(List<WebElement> elementsList, String elementText) {
    	boolean optionNotFound = true;
    	for (WebElement optionElement : elementsList) {
    		if (this.isElementDisplayed(optionElement) &&
    				optionElement.getText().toUpperCase().equals(elementText.toUpperCase())) {
    			Actions action = new Actions(this.driver);
    			action.doubleClick(optionElement).perform();
    			optionNotFound = false;
    		}
    	}
    	if (optionNotFound) throw new org.openqa.selenium.NoSuchElementException(generateErrorMessage(elementText));
    }     
    
    protected void clickElementFromListByVisibleText (WebElement elementsList, String elementText) {
      	this.sleep(500); 
    	Select option = new Select (elementsList);
    	option.selectByVisibleText(elementText);
    	this.sleep(500);
    }
    
    protected boolean isElementDisplayed (WebElement element) {
    	try {
    		this.waitAllRequests();
    		this.waitForVisibilityOfElement(element);
    		this.scrollToAndHighlightElement(element);
    		this.sleep(500);
    		return true;    		
    	}
    	catch (TimeoutException e) {
    		return false;
    	}
    }
    
    

    
    
    
    
    
    
    protected boolean isElementDisplayed (WebElement element, int waitTime) {
    	try {
    		WebDriverWait waitT = new WebDriverWait(driver, waitTime);
    		this.waitAllRequests();
    		waitT.until(ExpectedConditions.visibilityOf(element));    	
    		this.sleep(150);
    		return true;    		
    	}
    	catch (TimeoutException e) {
    		return false;
    	}
    }
    
    
    protected boolean isElementClickable (WebElement element, int waitTime) {
    	try {
    		WebDriverWait waitT = new WebDriverWait(driver, waitTime);
    		this.waitAllRequests();
    		WebElement testEl = null;
    		testEl = waitT.until(ExpectedConditions.elementToBeClickable(element));
    		if ( testEl != null ) {
    			return true;    			
    		}    		
    		else {
    			return false;    			
    		}
    	}
    	catch (TimeoutException e) {
    		return false;
    	}
    }
   
    
    protected void waitForElementBeClickable (WebElement element) {
    	this.waitAllRequests();
    	wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
    	wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
    	
    }
    
    protected void enterText (WebElement element, String text) {
    	this.waitForElementBeClickable(element);
    	this.scrollElementToCenterOfView(element);
    	this.waitAllRequests(3);
    	element.click();
    	element.clear();
    	element.sendKeys(text);
    	this.sleep(500);
    }
    
   protected void pressEnter(WebElement element) {
	   element.sendKeys(Keys.RETURN);
   }
   
   protected void scrollDownUsingRobot(int wheelSteps) {
	   this.sleep(150);
       Robot r;
       try {
	   	   r = new Robot();
	   	   
	    	java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	    	int width = (int) screenSize.getWidth();
	    	int height = (int) screenSize.getHeight();
	    	if (width <= 1366) {
	    		//if testing using laptop monitor
	    		//r.mouseMove(10, 400);
	    		r.mouseMove(width/2, height);
	    	}
	    	else {
	    		//if testing using 2nd monitor
	    		r.mouseMove(1366 + (width-1366)/2, height);
	    	}
	   	   
	   	   
	   	   for(int i = 0; i < wheelSteps; i++){
	   		   r.mouseWheel(3);
	   	       this.sleep(150);
	   	   }
       } 
       catch (AWTException e) {
      		e.printStackTrace();
       }       
	   this.sleep(200);
   }
   protected void scrollUpUsingRobot(int wheelSteps) {
	   this.sleep(150);
       Robot r;
       try {
	   	   r = new Robot();
	   	   r.mouseMove(10, 400);
	   	   for(int i = 0; i < wheelSteps; i++){
	   		   r.mouseWheel(-3);
	   	       this.sleep(150);
	   	   }
       } 
       catch (AWTException e) {
      		e.printStackTrace();
       }       
	   this.sleep(200);
   }
   
   
   protected void clickLocationXYusingRobot(int X, int Y) {
	   this.sleep(150);  
   	   try {
   	       Robot r;
   		   r = new Robot();
   		   //add browser's menu vertical offset and click in element position
		   //r.mouseMove(X, Y + 120);
   		   //if browser's window is in full screen state, don't use offset
   		   r.mouseMove(X, Y);
		   this.sleep(200);
		   r.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		   r.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	   } catch (AWTException e) {
		   e.printStackTrace();
	   }
   	   this.sleep(200);
   } 
   
   
   public void scrollDownToElementUsingRobot(By locator) {
	   int row = -1;
	   if (this.isElementPresentBy(locator)) {
	   		row = 1;
	   }  	
	   int j = 1;
	   while ((row == -1) && (j<100)) {
			try {
				this.scrollDownUsingRobot(1);
			} catch (Exception e) {
				e.printStackTrace();
			}					
	        this.sleep(150);
	       	if (this.isElementPresentBy(locator)) {
	       		row = 1;
	       	}
	        j++;
	   }	   
	   if (j > 1) {
	    	try {
				this.scrollDownUsingRobot(3);
			} catch (Exception e) {
				e.printStackTrace();
			}
	   }
	   this.sleep(150);
   }
	   
  
   public void scrollUpToElementUsingRobot(By locator) {
	   int row = -1;
	   if (this.isElementPresentBy(locator)) {
	   		row = 1;
	   }  	
	   int j = 1;
	   while ((row == -1) && (j<100)) {
			try {
				this.scrollUpUsingRobot(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
	        this.sleep(150);
	       	if (this.isElementPresentBy(locator)) {
	       		row = 1;
	       	}
	        j++;
	   }	   
	   if (j > 1) {
	    	try {
				this.scrollUpUsingRobot(3);
			} catch (Exception e) {
				e.printStackTrace();
			}
	   }
	   this.sleep(150);
   }
   
   public void enterFullScreenMode () {
	   driver.manage().window().fullscreen();
   }
   
   public void exitFullScreenMode () {
	   driver.manage().window().maximize();
   }
	
   public void waitUntilElementIsPresent(By by){
       int attempts = 0;
       while (  (driver.findElements(by).size() == 0) && (attempts < 1500)  ) {
       	this.waitAllRequests(1);
       	this.sleep(200);
       	attempts++;
       }
   }
    
    
    
    
}