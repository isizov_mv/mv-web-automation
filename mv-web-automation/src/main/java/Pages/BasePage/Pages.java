package Pages.BasePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


import Pages.PatientsPortal630Pages.AdminTechPadGeneralSubMenuPage63;
import Pages.PatientsPortal630Pages.PatientsPortalAdminPage63;
import Pages.PatientsPortal630Pages.PatientsPortalHomePage63;
import Pages.PatientsPortal630Pages.PatientsPortalLoginPage63;
import Pages.PatientsPortal630Pages.UserListSubMenuPage63;

import Pages.PatientsPortal520Pages.PatientsPortalLoginPage52;
import Pages.PatientsPortal520Pages.AdminPortalConfigTechpadSettingsPage52;
import Pages.PatientsPortal520Pages.PatientsPortalAdminPage52;
import Pages.PatientsPortal520Pages.PatientsPortalHomePage52;
import Pages.TechpadPages.CalendarPage;
import Pages.TechpadPages.DemographicTabPage;
import Pages.TechpadPages.ExamTabPage;
import Pages.TechpadPages.PatientTabPage;
import Pages.TechpadPages.PerformExamPage;
import Pages.TechpadPages.StudiesTabPage;
import Pages.TechpadPages.StudyDetailsPage;
import Pages.TechpadPages.SurveyHistoryTabPage;
import Pages.TechpadPages.TechPadHomePage;
import Pages.TechpadPages.TechPadLoginPage;



/*
import Pages.UnifiPages.UnifiHomePage;
import Pages.UnifiPages.UnifiLoginPage;
import Pages.TechPadPages.TechPadLoginPage;
import Pages.TechPadPages.TechPadHomePage;


*/

public class Pages extends BasePage {


    public Pages(WebDriver driver) {
        super(driver);
    }
/*
    public UnifiLoginPage unifiLoginPage () {
        UnifiLoginPage unifiLoginPage = PageFactory.initElements(driver, UnifiLoginPage.class);
        return unifiLoginPage;
    }

    public UnifiHomePage unifiHomePage () {
        UnifiHomePage unifiHomePage = PageFactory.initElements(driver, UnifiHomePage.class);
        return unifiHomePage;
    }
*/
    public PatientsPortalLoginPage63 patientsPortalLoginPage63() {
        PatientsPortalLoginPage63 patientsPortalLoginPage63 = PageFactory.initElements(driver, PatientsPortalLoginPage63.class);
        return patientsPortalLoginPage63;
    }

    public PatientsPortalHomePage63 patientsPortalHomePage63() {
        PatientsPortalHomePage63 patientsPortalHomePage63 = PageFactory.initElements(driver, PatientsPortalHomePage63.class);
        return patientsPortalHomePage63;
    }

    public PatientsPortalAdminPage63 patientsPortalAdminPage63() {
    	PatientsPortalAdminPage63 patientsPortalAdminPage63 =  PageFactory.initElements(driver, PatientsPortalAdminPage63.class);
        return patientsPortalAdminPage63;   
    }

    public UserListSubMenuPage63 userListSubTab63() {
    	UserListSubMenuPage63 userListSubTab63 = PageFactory.initElements(driver, UserListSubMenuPage63.class);
        return userListSubTab63;
    }
    
    public TechPadLoginPage techPadLoginPage () {
    	TechPadLoginPage techpadLoginPage = PageFactory.initElements(driver, TechPadLoginPage.class);
        return techpadLoginPage;
    }
        
    public TechPadHomePage techPadHomePage () {
    	TechPadHomePage techPadHomePage = PageFactory.initElements(driver, TechPadHomePage.class);
        return techPadHomePage;
    }
    
    public AdminTechPadGeneralSubMenuPage63 adminTechPadGeneralSubMenuPage63 () {
    	AdminTechPadGeneralSubMenuPage63 adminTechPadGeneralSubMenu = PageFactory.initElements(driver, AdminTechPadGeneralSubMenuPage63.class);
        return adminTechPadGeneralSubMenu;
    }
      
    public PatientTabPage patientTabPage () {
    	PatientTabPage patientTabPage = PageFactory.initElements(driver, PatientTabPage.class);
        return patientTabPage;
    } 
    
    public StudiesTabPage studiesTabPage () {
    	StudiesTabPage studiesTabPage = PageFactory.initElements(driver, StudiesTabPage.class);
        return studiesTabPage;
    } 
    
    public StudyDetailsPage studyDetailsPage () {
    	StudyDetailsPage studyDetailsPage = PageFactory.initElements(driver, StudyDetailsPage.class);
        return studyDetailsPage;
    } 
    
    public PatientsPortalLoginPage52 patientsPortalLoginPage52 () {
    	PatientsPortalLoginPage52 patientsPortalLoginPage52 = PageFactory.initElements(driver, PatientsPortalLoginPage52.class);
        return patientsPortalLoginPage52;
    } 
     
    public PatientsPortalHomePage52 patientsPortalHomePage52 () {
    	PatientsPortalHomePage52 patientsPortalHomePage52 = PageFactory.initElements(driver, PatientsPortalHomePage52.class);
        return patientsPortalHomePage52;
    } 
    
    public PatientsPortalAdminPage52 patientsPortalAdminPage52 () {
    	PatientsPortalAdminPage52 patientsPortalAdminPage52 = PageFactory.initElements(driver, PatientsPortalAdminPage52.class);
        return patientsPortalAdminPage52;
    } 
    
    public AdminPortalConfigTechpadSettingsPage52 adminPortalConfigTechpadSettingsPage52 () {
    	AdminPortalConfigTechpadSettingsPage52 adminPortalConfigTechpadSettingsPage52 = PageFactory.initElements(driver, AdminPortalConfigTechpadSettingsPage52.class);
        return adminPortalConfigTechpadSettingsPage52;
    } 
    
    public ExamTabPage examTabPage () {
    	ExamTabPage examTabPage = PageFactory.initElements(driver, ExamTabPage.class);
        return examTabPage;
    }    

    public PerformExamPage performExamPage () {
    	PerformExamPage performExamPage = PageFactory.initElements(driver, PerformExamPage.class);
        return performExamPage;
    }    
    
    public DemographicTabPage demographicTabPage () {
    	DemographicTabPage demographicTabPage = PageFactory.initElements(driver, DemographicTabPage.class);
        return demographicTabPage;
    }   
    
    public CalendarPage calendarPage () {
    	CalendarPage calendarPage = PageFactory.initElements(driver, CalendarPage.class);
        return calendarPage;
    }
    
    public SurveyHistoryTabPage surveyHistoryTabPage () {
    	SurveyHistoryTabPage surveyHistoryTabPage = PageFactory.initElements(driver, SurveyHistoryTabPage.class);
        return surveyHistoryTabPage;
    }    
    
    
}


