package Pages.PatientsPortal630Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;


public class UserListSubMenuPage63 extends BasePage {
	
	public UserListSubMenuPage63(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "button[ng-click*='addUser']")
    private WebElement addUserButton;
	

    @FindBy(css = "button[ng-click*='$close()']")
    private WebElement addUserCancelButton;

    
        
    
    
    @Step(value = "Press Add User Button")
    public void clickAddUserButton()
    {
    	this.scrollElementToCenterOfView(addUserButton);
    	this.sleep(3000);
    	this.clickOverElement(addUserButton);
    	//testsButton.click();
        this.sleep(4000);
    }   

    @Step(value = "Press Cancel to Add User Button")
    public void clickCancelToAddUserButton()
    {
    	this.scrollElementToCenterOfView(addUserCancelButton);
    	this.sleep(3000);
    	this.clickOverElement(addUserCancelButton);
    	//testsButton.click();
        this.sleep(4000);
    }   

    
    
    
}



	
	
