package Pages.PatientsPortal630Pages;



import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class PatientsPortalHomePage63 extends BasePage {

    public PatientsPortalHomePage63(WebDriver driver) {
        super(driver);
    }


    String homePagePortalTitle = "Patients Portal";

    @FindBy(css = "b[class*='caret grey-colored']")
    private WebElement dropdownButton;

    @FindBy(css = "a[href*='LogOff']")
    private WebElement logoutButton;

    @FindBy(css = "a[href*='admin']")
    private WebElement adminButton;



    @Step(value = "Press Admin Button")
    public void clickAdminButton()
    {
        adminButton.click();
        this.sleep(4000);
    }



    @Step(value = "Press User Dropdown Button")
    public void clickGreyDropdownButton()
    {
        dropdownButton.click();
        this.sleep(1000);
    }

    @Step(value = "Press Logout Button")
    public void clickLogoutButton()
    {
        dropdownButton.click();
        this.sleep(1000);
        logoutButton.click();
        this.sleep(3000);
    }

    @Step(value = "Verify Login is successful")
    public boolean isLoginSuccessful(){
        return this.driver.getTitle().contains(homePagePortalTitle);
    }

}

