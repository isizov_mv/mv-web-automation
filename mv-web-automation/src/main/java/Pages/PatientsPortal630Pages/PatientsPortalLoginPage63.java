package Pages.PatientsPortal630Pages;


import Pages.BasePage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PatientsPortalLoginPage63 extends BasePage {

    public PatientsPortalLoginPage63(WebDriver driver) {
        super(driver);
    }

    //String URL = "https://portal-alpha.magview.com/account/login";
    String portalTitle = "Patients Portal - Login";

    @FindBy(css = "input[id*='UserName']")
    private WebElement userName;

    @FindBy(css = "input[id*='Password']")
    private WebElement password;

    @FindBy(css = "input[id*='loginBtn']")
    private WebElement loginButton;

    
    
    @Step(value = "Open Patient Portal Login page")
    public void gotoLoginPage(String portalURL)
    {
        driver.get(portalURL);
    }
    @Step(value = "Enter User Name")
    public void enterUserName(String UseNameText)
    {
        this.clickOverElement(userName);
        this.enterText(userName, UseNameText);
        this.sleep(2000);
    }
    @Step(value = "Enter Password")
    public void enterPassword(String passwordText)
    {
        this.clickOverElement(password);
        this.enterText(password, passwordText);
        this.sleep(2000);
    }
    @Step(value = "Press Login Button")
    public void clickLoginButton()
    {
    	this.click(loginButton);
        this.sleep(7000);
    }
    @Step(value = "Login to Patient Portal")
    public void loginToPatientPortal(String portalURL, String userName, String password){
        this.gotoLoginPage(portalURL);
        this.enterUserName(userName);
        this.enterPassword(password);
        this.clickLoginButton();
    }


}
