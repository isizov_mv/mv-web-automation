package Pages.PatientsPortal630Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;

public class PatientsPortalAdminPage63 extends BasePage {
	
	
	public PatientsPortalAdminPage63(WebDriver driver) {
        super(driver);
    }

	//---Main ADMIN Tab menu-----------------------------
    @FindBy(css = "a[href*='admin']")
    private WebElement adminTab;

    @FindBy(css = "a[href*='admin/audit']")
    private WebElement auditLogs;    
    
    @FindBy(css = "i[ng-show*='user-management']")
    private WebElement usersButton;     

    @FindBy(css = "i[ng-show*='portal-configuration']")
    private WebElement portalCoreButton;      
    
    @FindBy(css = "i[ng-show*='patient-result']")
    private WebElement patientResultPortalButton;    
        
    @FindBy(css = "i[ng-show*='history-config']")
    //@FindBy(css = "i[ng-click*='history-config']")
    //@FindBy(css = "a[href*='path#collapseSix']")
    private WebElement historyPortalButton;     
    
    @FindBy(css = "i[ng-show*='survey-config']")
    private WebElement surveyButton;    

    @FindBy(css = "i[ng-show*='techpad-settings']")
    private WebElement techPadButton;     
    
    @FindBy(css = "a[href*='/test/full']")
    private WebElement testsButton;
	//-----------------------------Main ADMIN Tab menu---    
    
    
    //USER sub-menu
    //-------------------------------------------------
    @FindBy(css = "a[href*='admin/manage-users']")
    private WebElement userListSubMenu;
    
    @FindBy(css = "a[href*='admin/manage-refphy']")
    private WebElement referringPhysiciansSubMenu;
    
    @FindBy(css = "a[href*='admin/manage-roles']")
    private WebElement rolesSubMenu;
    
    
    //PORTAL CORE sub-menu
    //----------------------------------------------------
    @FindBy(css = "a[href*='admin/configuration-core']")
    private WebElement coreSettingsSubMenu;
    
    @FindBy(css = "a[href*='admin/configuration-user-account-settings']")
    private WebElement userAccountSettingsSubMenu;
    
    @FindBy(css = "a[href*='admin/configuration-dynamic-labels']")
    private WebElement labelCustomizationSubMenu;
    
    @FindBy(css = "a[href*='admin/configuration-login']")
    private WebElement loginPageConfigurationSubMenu;

    @FindBy(css = "a[href*='admin/configuration-form-xml-list']")
    private WebElement formXMLListSubMenu;
    
    @FindBy(css = "a[href*='admin/configuration-license-management']")
    private WebElement licenseManagementSubMenu;
    
    @FindBy(css = "a[href*='admin/configuration-other']")
    private WebElement otherSettingsSubMenu;
    
    
    //PATIENT RESULT PORTAL sub-menu
    //----------------------------------------------------
    @FindBy(css = "a[href*='/admin/patient-result-email-service']")
    private WebElement emailServiceSubMenu;
    
    @FindBy(css = "a[href*='/admin/patient-result-documents']")
    private WebElement documentsSubMenu;
    
    
    //HISTORY PORTAL sub-menu
    //----------------------------------------------------
    @FindBy(css = "a[href*='/admin/history-config-general']")
    private WebElement generalHistoryPortalSubMenu;
    
    @FindBy(css = "a[href*='/admin/history-config-status']")
    private WebElement historySurveyStatusMenu;
    

    //SURVEY sub-menu
	//----------------------------------------------------
    @FindBy(css = "a[href*='/admin/survey-config-configuration-survey']")
    private WebElement generalSurveySubMenu;
    
    @FindBy(css = "a[href*='/admin/survey-config-survey-management']")
    private WebElement surveyPackageSubMenu;

    @FindBy(css = "a[href*='/admin/survey-config-maglink']")
    private WebElement maglinkEventsSubMenu;
    
    @FindBy(css = "a[href*='/admin/survey-config-actions']")
    private WebElement surveyActionsSubMenu;
    
    
    //TECHPAD sub-menu
	//----------------------------------------------------
    @FindBy(css = "a[href*='admin/techpad-settings?tab=general']")
    private WebElement generalTechpadSubMenu;

    @FindBy(css = "a[href*='admin/techpad-settings?tab=maglink-events']")
    private WebElement maglinkEventsTechpadSubMenu;

    @FindBy(css = "a[href*='admin/techpad-settings?tab=exam-actions']")
    private WebElement examActionsTechpadSubMenu;
    
    @FindBy(css = "a[href*='admin/techpad-settings?tab=comments-settings']")
    private WebElement commentsSettingsTechpadSubMenu;
    
    @FindBy(css = "a[href*='admin/techpad-settings?tab=diagram-settings']")
    private WebElement diagramSettingsTechpadSubMenu;
    
    @FindBy(css = "a[href*='admin/techpad-settings?tab=worklist-sorting']")
    private WebElement worklistSortingTechpadSubMenu;
    
    @FindBy(css = "a[href*='admin/techpad-settings?tab=dynamic-labels']")
    private WebElement dynamicLabelsTechpadSubMenu;

    @FindBy(css = "a[href*='admin/techpad-settings?tab=status-and-tags']")
    private WebElement statusAndTagsTechpadSubMenu;

    @FindBy(css = "a[href*='admin/techpad-settings?tab=resources']")
    private WebElement resourcesTechpadSubMenu;
    
    @FindBy(css = "a[href*='admin/techpad-settings-pdf-config']")
    private WebElement PDFFormConfigTechpadSubMenu;

    
    
    
    
    //---MAIN TABS-----------------------------------------
    @Step(value = "Press Admin Button")
    public void clickAdminTab()
    {
        adminTab.click();
        this.sleep(6000);
    }
    //-----------------------------------------MAIN TABS---

    
    //---ADMIN TAB MENU AND SUBMENUS-----------------------------------------------
        
    //---AUDIT LOGS----------------------------------------
    @Step(value = "Press Audit Log Button")
    public void clickAuditLogs()
    {
    	this.scrollElementToCenterOfView(auditLogs);
    	this.sleep(500);
    	this.clickOverElement(auditLogs);
    	
    	//auditLogs.clickOverElement();    	
        this.sleep(4000);
    }
    //----------------------------------------AUDIT LOGS---
    
    
    //---USER-----------------------------------------------------
    @Step(value = "Press Users Button")
    public void clickUserButton()
    {
    	this.scrollElementToCenterOfView(usersButton);
    	this.sleep(3000);
    	this.clickOverElement(usersButton);
    	//usersButton.click();
        this.sleep(4000);
    }    
    //User sub-menu
    @Step(value = "Select User List")
    public void clickUserListsSubMenu()
    {
    	this.scrollElementToCenterOfView(userListSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(userListSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }
    @Step(value = "Select Referring Physicians")
    public void clickReferringPhysiciansSubMenu()
    {
    	this.scrollElementToCenterOfView(referringPhysiciansSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(referringPhysiciansSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }
    @Step(value = "Select User Account Settings")
    public void clickRolesSubMenu()
    {
    	this.scrollElementToCenterOfView(rolesSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(rolesSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //-----------------------------------------------------USER---    

    
    //---PORTAL CORE-----------------------------------------------    
    @Step(value = "Press Portal Core Button")
    public void clickPortalCoreButton()
    {
    	this.scrollElementToCenterOfView(portalCoreButton);
    	this.sleep(3000);
    	this.clickOverElement(portalCoreButton);
    	//portalCoreButton.click();
        this.sleep(4000);
    }    
    //Portal Core sub-menu
    @Step(value = "Select Core Settings")
    public void clickSelectCoreSettingsSubMenu()
    {
    	this.scrollElementToCenterOfView(coreSettingsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(coreSettingsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select User Account Settings")
    public void clickUserAccountSettingsSubMenu()
    {
    	this.scrollElementToCenterOfView(userAccountSettingsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(userAccountSettingsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Label Customization")
    public void clickLabelCustomizationSubMenu()
    {
    	this.scrollElementToCenterOfView(labelCustomizationSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(labelCustomizationSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Login Page Configuration")
    public void clickLoginPageConfigSubMenu()
    {
    	this.scrollElementToCenterOfView(loginPageConfigurationSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(loginPageConfigurationSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Form XML List")
    public void clickFormXMLListSubMenu()
    {
    	this.scrollElementToCenterOfView(formXMLListSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(formXMLListSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select License Management")
    public void clickLicenseMngmntSubMenu()
    {
    	this.scrollElementToCenterOfView(licenseManagementSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(licenseManagementSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Other Settings")
    public void clickOtherSettingsSubMenu()
    {
    	this.scrollElementToCenterOfView(otherSettingsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(otherSettingsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //------------------------------------------------PORTAL CORE---   

    
    //---PATIENT RESULT PORTAL--------------------------------------    
    @Step(value = "Press Patient Result Portal Button")
    public void clickPatientResultPortalButton()
    {
    	this.scrollElementToCenterOfView(patientResultPortalButton);
    	this.sleep(3000);
    	this.clickOverElement(patientResultPortalButton);
    	//portalCoreButton.click();
        this.sleep(4000);
    }     
    //Patient Result Portal sub-menu
    @Step(value = "Select Email Serice from Patient Result Portal")
    public void clickEmailServiceSubMenu()
    {
    	this.scrollElementToCenterOfView(emailServiceSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(emailServiceSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Documents from Patient Result Portal")
    public void clickDocumentsPatientResultSubMenu()
    {
    	this.scrollElementToCenterOfView(documentsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(documentsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //--------------------------------------PATIENT RESULT PORTAL---
    
    
    //---HISTORY PORTAL---------------------------------------------
    @Step(value = "Press History Portal Button")
    public void clickHistoryPortalButton()
    {
    	this.scrollElementToCenterOfView(historyPortalButton);
    	this.sleep(3000);
    	this.clickOverElement(historyPortalButton);
    	//historyPortalButton.click();
        this.sleep(4000);
    }    
    //History Portal sub-menu
    @Step(value = "Select General from History Portal")
    public void clickGeneralHistoryPortalSubMenu()
    {
    	this.scrollElementToCenterOfView(generalHistoryPortalSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(generalHistoryPortalSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select History Survey Status from History Portal")
    public void clickHistorySurveyStatusSubMenu()
    {
    	this.scrollElementToCenterOfView(historySurveyStatusMenu);
    	this.sleep(3000);
    	this.clickOverElement(historySurveyStatusMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //---------------------------------------------HISTORY PORTAL---    


    //---SURVEY------------------------------------------------
    @Step(value = "Press Survey Button")
    public void clickSurveyButton()
    {
    	this.scrollElementToCenterOfView(surveyButton);
    	this.sleep(3000);
    	this.clickOverElement(surveyButton);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //Survey sub-menu click functions
    @Step(value = "Select General from Survey")
    public void clickGeneralSurveySubMenu()
    {
    	this.scrollElementToCenterOfView(generalSurveySubMenu);
    	this.sleep(3000);
    	this.clickOverElement(generalSurveySubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Survey Package from Survey")
    public void clickSurveyPackageSubMenu()
    {
    	this.scrollElementToCenterOfView(surveyPackageSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(surveyPackageSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Maglink Events from Survey")
    public void clickMaglinkEventsSubMenu()
    {
    	this.scrollElementToCenterOfView(maglinkEventsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(maglinkEventsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    @Step(value = "Select Survey Actions from Survey")
    public void clickSurveyActionsSubMenu()
    {
    	this.scrollElementToCenterOfView(surveyActionsSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(surveyActionsSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }    
    //-------------------------------------------------SURVEY---        
    
    
    //---TECHPAD------------------------------------------------    
    @Step(value = "Press Techpad Button")
    public void clickTechpadButton()
    {
    	this.scrollElementToCenterOfView(techPadButton);
    	this.sleep(3000);
    	this.clickOverElement(techPadButton);
    	//techPadButton.click();
        this.sleep(4000);
    }    
    //TechPad sub-menu click functions
    @Step(value = "Select General from Techpad")
    public void clickGeneralTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(generalTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(generalTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Maglink Events from Techpad")
    public void clickMaglinkEventsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(maglinkEventsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(maglinkEventsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Exam Actions from Techpad")
    public void clickExamActionsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(examActionsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(examActionsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Comments Settings from Techpad")
    public void clickCommentsSettingsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(commentsSettingsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(commentsSettingsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Diagram Settings from Techpad")
    public void clickDiagramSettingsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(diagramSettingsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(diagramSettingsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Worklist Sorting from Techpad")
    public void clickWorklistSortingTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(worklistSortingTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(worklistSortingTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Dynamic Labels from Techpad")
    public void clickDynamicLabelsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(dynamicLabelsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(dynamicLabelsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Status & Tags from Techpad")
    public void clickStatusAndTagsTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(statusAndTagsTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(statusAndTagsTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select Resources from Techpad")
    public void clickResourcesTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(resourcesTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(resourcesTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    @Step(value = "Select PDF Forms Configuration from Techpad")
    public void clickPDFFormConfigTechpadSubMenu()
    {
    	this.scrollElementToCenterOfView(PDFFormConfigTechpadSubMenu);
    	this.sleep(3000);
    	this.clickOverElement(PDFFormConfigTechpadSubMenu);
    	//surveyButton.click();
        this.sleep(4000);
    }       
    //------------------------------------------------TECHPAD---
    
    
    
    
    
    
    
    @Step(value = "Press Tests Button")
    public void clickTestsButton()
    {
    	this.scrollElementToCenterOfView(testsButton);
    	this.sleep(3000);
    	this.clickOverElement(testsButton);
    	//testsButton.click();
        this.sleep(4000);
    }        
    //-----------------------------------------------ADMIN TAB MENU AND SUBMENUS---    
    
    
    
    
}
