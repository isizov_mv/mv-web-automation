package Pages.PatientsPortal630Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import Pages.BasePage.BasePage;
import io.qameta.allure.Step;


public class AdminTechPadGeneralSubMenuPage63 extends BasePage {

    public AdminTechPadGeneralSubMenuPage63(WebDriver driver) {
        super(driver);
    }
    
    
    @FindBy(css = ( "button[ng-click^='model.save']"))
    private WebElement saveButton;
  
    
    //===Various Options Section=======================================================================
    
    private By updateStudiesRISHold_F_onEndExam_buttonClick_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsUpdateStudyRISHoldOnEndExam'][class*='ng-not-empty']");
    private By updateStudiesRISHold_F_onEndExam_buttonClick_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsUpdateStudyRISHoldOnEndExam'][class*='ng-empty']");
    
    private By updateStudies_T_End_timestamp_on_EndExam_buttonClick_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsUpdateStudyT_EndOnEndExam'][class*='ng-not-empty']");
    private By updateStudies_T_End_timestamp_on_EndExam_buttonClick_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsUpdateStudyT_EndOnEndExam'][class*='ng-empty']");

    private By showDemoMenuItem_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowDemoMenuItem'][class*='ng-not-empty']");
    private By showDemoMenuItem_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowDemoMenuItem'][class*='ng-empty']");
    
    private By hideStart_EndExamButtons_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='HideStartAndEndButtons'][class*='ng-not-empty']");
    private By hideStart_EndExamButtons_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='HideStartAndEndButtons'][class*='ng-empty']");

    private By showPatients_MenuItem_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowPatientsMenuItem'][class*='ng-not-empty']");
    private By showPatients_MenuItem_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowPatientsMenuItem'][class*='ng-empty']");

    private By disableRequiringStudiesFields_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='DisableRequiringStudyFields'][class*='ng-not-empty']");
    private By disableRequiringStudiesFields_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='DisableRequiringStudyFields'][class*='ng-empty']");
    
    private By enableWorklistPatientsDataSynchronization_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='EnableWorklistPatientsDataSync'][class*='ng-not-empty']");
    private By enableWorklistPatientsDataSynchronization_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='EnableWorklistPatientsDataSync'][class*='ng-empty']");

    private By showSelectSecondTechnologist_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowSelectSecondTechnologist'][class*='ng-not-empty']");
    private By showSelectSecondTechnologist_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowSelectSecondTechnologist'][class*='ng-empty']");
    
    private By showSpeedTestMenuItem_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowSpeedTestTab'][class*='ng-not-empty']");
    private By showSpeedTestMenuItem_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowSpeedTestTab'][class*='ng-empty']");
    
    private By enableSurveyHistoryMode_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='ISurveyHistoryEnabled'][class*='ng-not-empty']");
    private By enableSurveyHistoryMode_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='ISurveyHistoryEnabled'][class*='ng-empty']");
    
    private By includedPersonalBRCAHistory_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsIncludePersonalBRCAHistory'][class*='ng-not-empty']");
    private By includedPersonalBRCAHistory_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsIncludePersonalBRCAHistory'][class*='ng-empty']");
    
    private By includedFamilyBRCAHistory_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsIncludeFamilyBRCAHistory'][class*='ng-not-empty']");
    private By includedFamilyBRCAHistory_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsIncludeFamilyBRCAHistory'][class*='ng-empty']");
    
    private By enableAbilityToCreatePDFDocuments_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsDocumentsCreationAbilityEnabled'][class*='ng-not-empty']");
    private By enableAbilityToCreatePDFDocuments_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsDocumentsCreationAbilityEnabled'][class*='ng-empty']");
        
    private By hideSkipStartSurveyButtonsOnStudyDetailsPage_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='HideSkipAndStartButtons'][class*='ng-not-empty']");
    private By hideSkipStartSurveyButtonsOnStudyDetailsPage_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='HideSkipAndStartButtons'][class*='ng-empty']");
      
    private By disableStoreRiskTriggerForWorklistSave_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsStoreRiskTriggerDisabledForWorklistSave'][class*='ng-not-empty']");
    private By disableStoreRiskTriggerForWorklistSave_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsStoreRiskTriggerDisabledForWorklistSave'][class*='ng-empty']");
        
    private By enableReloadDocumentsOnEachClickOnDocumentsTab_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsLoadDocumentsOnEachTabClick'][class*='ng-not-empty']");
    private By enableReloadDocumentsOnEachClickOnDocumentsTab_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsLoadDocumentsOnEachTabClick'][class*='ng-empty']");
        
    private By validateRequiredFieldsOnEveryExamSave_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsValidateReqiuredFieldsOnEverySave'][class*='ng-not-empty']");
    private By validateRequiredFieldsOnEveryExamSave_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsValidateReqiuredFieldsOnEverySave'][class*='ng-empty']");
    
    private By enableDashboardMode_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsDashboardMode'][class*='ng-not-empty']");
    private By enableDashboardMode_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsDashboardMode'][class*='ng-empty']");
        
    private By showBarcodeForAccessionNumber_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowBarcodeForAccessionNumber'][class*='ng-not-empty']");
    private By showBarcodeForAccessionNumber_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowBarcodeForAccessionNumber'][class*='ng-empty']");
    
    //Show barcode for accession number dropdown list--------------------------------------------
    //Visible when Show barcode for accession number = ON
    @FindBy(css = ("select[ng-model*='settings.BarcodeType']"))
    private WebElement barcodeTypeDropdown; 
    
    @FindBy(css = ("select[ng-model*='settings.BarcodeType'] option[label*='CODE']"))
    private List<WebElement> barcodeTypeDropdownList;
    //--------------------------------------------Show barcode for accession number dropdown list
    
    
    private By showChatMenuItem_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowChatMenuItem'][class*='ng-not-empty']");
    private By showChatMenuItem_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowChatMenuItem'][class*='ng-empty']");
    
    //Chat service url  textbox----------------------------------------------------------------------
    //Visible when Show Chat Menu Item = ON
    @FindBy(css = ("input[type='text'][data-ng-model*='model.settings.ChatServiceUrl']"))
    private WebElement chatServiceUrlTextbox; 
    

    
    
    
    
   //----------------------------------------------------------------------Chat service url  textbox
    
    private By allowToChangeWorklistStatusFromCompleted_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsAllowedStatusToBeChangedFromCompleted'][class*='ng-not-empty']");
    private By allowToChangeWorklistStatusFromCompleted_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsAllowedStatusToBeChangedFromCompleted'][class*='ng-empty']");
    
    //Allow to change worklist status from completed dropdown-------------------------------------------------------------
    //Visible when Allow to change worklist status from completed = ON
    @FindBy(css = ("select[ng-model*='settings.AllowedStatusToBeChangedFromCompleted']"))
    private WebElement allowedStatusToBeChangedFromCompletedDropdown; 
    
    @FindBy(css = ("select[ng-model*='settings.AllowedStatusToBeChangedFromCompleted'] option[value*='Exam']"))
    private List<WebElement> allowToChangeWorklistStatusDropdownList;   
    //-------------------------------------------------------------Allow to change worklist status from completed dropdown
    
    
    
    private By enableTechpadDiagramRedirection_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsEnabledTechpadDiagramRedirection'][class*='ng-not-empty']");
    private By enableTechpadDiagramRedirection_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsEnabledTechpadDiagramRedirection'][class*='ng-empty']");
    
    //Techpad Diagram Redirect URL textbox---------------------------------------------------------------------------------
    //Visible when Enable Techpad Diagram redirection = ON
    @FindBy(css = ("input[type='text'][data-ng-model*='model.settings.TechpadDiagramRedirectURL']"))
    private WebElement techpadDiagramRedirectURLTextbox; 
    //---------------------------------------------------------------------------------Techpad Diagram Redirect URL textbox
   
    private By groupExamTypeWorklistFilter_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='GroupExamTypeWorklistFilter'][class*='ng-not-empty']");
    private By groupExamTypeWorklistFilter_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='GroupExamTypeWorklistFilter'][class*='ng-empty']");

    private By showTyrerCuzikTab_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowTyrerCuzikTab'][class*='ng-not-empty'][class*='ng-dirty']");   
    private By showTyrerCuzikTab_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowTyrerCuzikTab'][class*='ng-empty']");
    
    //Family Tree Survey dropdown list-----------------------------------------------------------
    //Visible when Show Tyrer Cuzik Tab = ON
    // has... functions not tested yet
    @FindBy(css = ("select[data-ng-model*='FamilyTreeSurveyName']"))
    private WebElement familyTreeSurveyNameDropdown; 
    
    @FindBy(css = ("select[data-ng-model*='FamilyTreeSurveyName'] option"))
    private List<WebElement> familyTreeSurveyNameDropdownList;   
    //-----------------------------------------------------------Family Tree Survey dropdown list
    
    
    
    
    
    
    //Diagram Carry Forward ON radio button
    @FindBy(css = ( "input[name='diagramCarryForward'][id='enabled']"))
    private WebElement diagramCarryForward_OnRadioButton;    
    
    //Diagram Carry Forward OFF radio button
    @FindBy(css = ( "input[name='diagramCarryForward'][id='disabled']"))
    private WebElement diagramCarryForward_OffRadioButton; 
    
    //COM Object ID ON radio button
    @FindBy(css = ( "input[name='ComID'][id='prodID']"))
    private WebElement comObjectID_OnRadioButton;    
    
    //COM Object ID OFF radio button
    @FindBy(css = ( "input[name='ComID'][id='testID']"))
    private WebElement comObjectID_OffRadioButton;  
    
    
    //=== NOTE: Default tab is "Demographic" ======================================
    
    //---Default Worklist Tab dropdown--------------------------------------------------------
    @FindBy(css = ("select[data-ng-model*='settings.DefaultWorklistTab']"))
    private WebElement defaultWorklistTabDropdown;      
    
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@.. no function for this locator only 1-5-2021
    @FindBy(css = ("select[data-ng-model*='DefaultWorklistTab'] option"))
    private List<WebElement> defaultWorklistTabDropdownList;  
  

    
    
    
    
    
    // 1st element is  options[0]
    @FindBy(css = ("option[selected*='selected'][value=\"\"]"))
    private List<WebElement> options;    
    //2nd element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='1']"))
    private WebElement defaultWorklistTabDropdownOption2; 
    //3rd element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='3']"))
    private WebElement defaultWorklistTabDropdownOption3; 
    //4th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='4']"))
    private WebElement defaultWorklistTabDropdownOption4; 
    //5th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='6']"))
    private WebElement defaultWorklistTabDropdownOption5;   
    //6th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='7']"))
    private WebElement defaultWorklistTabDropdownOption6; 
    //7th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='8']"))
    private WebElement defaultWorklistTabDropdownOption7; 
    //8th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='9']"))
    private WebElement defaultWorklistTabDropdownOption8;   
    //9th element
    @FindBy(css = ("option[data-ng-repeat='tab in model.tabs'][value='11']"))
    private WebElement defaultWorklistTabDropdownOption9;     
    //--------------------------------------------------------Default Worklist Tab dropdown---
    
    //Enabled Codes on FamilyTree enabled textbox
    @FindBy(css = ("input[data-ng-model*='model.settings.FamilyTreeEnabledCodeList']"))
    private WebElement enabledCodesOnFamilyTreeTextbox;  
    
    //Worklist Prioritizing Accuracy textbox
    @FindBy(css = ("input[data-ng-model*='model.settings.WorklistPrioritizingAccuracy']"))
    private WebElement worklistPrioritizingAccuracyTextbox;  
    
   
    //Reset Various Options button
    @FindBy(css = ("button[ng-click*='resetVariousOptions']"))
    private WebElement resetVariousOptionsButton;      
    
    
    

   //=======================================================================Various Options Section===
    
   
    

    //Session Expiration Timeout textbox
    @FindBy(css = ("input[data-ng-model*='model.settings.SessionExpirationTimeout']"))
    private WebElement sessionExpirationTimeoutTextbox;  
    
    //Session Warn Timeout textbox
    @FindBy(css = ("input[data-ng-model*='SessionWarnTimeout']"))
    private WebElement sessionWarnTimeoutTextbox;  
    
    //History Load Retries textbox
    @FindBy(css = ("input[data-ng-model*='HistoryLoadRetries']"))
    private WebElement historyLoadRetriesTextbox;  
     
    
    
    
    //=== Worklist Time Tracking Section ==============================================================
    
    //Worklist Time Tracking / Time Format  "Days" radio button
    @FindBy(css = ( "input[name^='showDetailTime'][id$='TimingDisabled']"))
    private WebElement timeFormat_DaysRadioButton;  
    
    //Worklist Time Tracking / Time Format  "Always" radio button
    @FindBy(css = ( "input[name^='showDetailTime'][id$='TimingEnabled']"))
    private WebElement timeFormat_AlwaysRadioButton;   
    
    
    
    
    //--Time Tracking Options--------------------------
    
    //Show Total Time
    private By showTotalTimeOptions_CheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowBothTotalTimeAnd'][class*='ng-not-empty']");   
    private By showTotalTimeOptions_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowBothTotalTimeAnd'][class*='ng-empty']");
    
     
    //Show Detail Time
    private By showDetailTime_CheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowDetailTimeInWorklist'][class*='ng-not-empty']");   
    private By showDetailTime_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowDetailTimeInWorklist'][class*='ng-empty']");
    
    
/**    
    private By showBothTotalTimeAndLastStatusChangeTimeInWorklist_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowBothTotalTimeAndLastStatusTimeInWorklist'][class*='ng-not-empty']");
    private By showBothTotalTimeAndLastStatusChangeTimeInWorklist_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowBothTotalTimeAndLastStatusTimeInWorklist'][class*='ng-empty']");
    
    private By showDetailTimeWithin24HoursDuration_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowDetailTimeWithin24HoursDuration'][class*='ng-not-empty']");
    private By showDetailTimeWithin24HoursDuration_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='ShowDetailTimeWithin24HoursDuration'][class*='ng-empty']");
**/    
    
    
    
    
    
    

    //Show Show Comment Time
    //Visible when Show Detail Time = ON
    private By showCommentTime_CheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowDetailCommentTimeInWorklist'][class*='ng-not-empty']");   
    private By showCommentTime_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='ShowDetailCommentTimeInWorklist'][class*='ng-empty']");
    
    //-------------------------Time Tracking Options---
    
    //============================================================== Worklist Time Tracking Section ===
    
    
    
    
    //===Alternative Patient Identifier Settings Section===================================================
    
    //Use alternative patient identifier checkbox
    private By useAlternativePatientIdentifier_CheckedBy = 
	By.cssSelector("input[data-ng-model*='UseAlternateUserIdentifier'][class*='ng-not-empty']");   
    private By useAlternativePatientIdentifie_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='UseAlternateUserIdentifier'][class*='ng-empty']");
    
    
    //Patient Identifier Fields dropdown list
    //Visible when Use alternative patient identifier = ON
    @FindBy(css = ("select[data-ng-model*='AlternateUserIdentifierField']"))
    private WebElement patientIdentifierFieldsDropdown; 
    
    @FindBy(css = ("select[data-ng-model*='AlternateUserIdentifierField'] option"))
    private List<WebElement> patientIdentifierFieldsDropdownList;  
    
    
    //===================================================Alternative Patient Identifier Settings Section===
    
    
    
    //===Action for "Mark as Complete" Section========================================================
    
    //Update (Studies.RISHold = '.F.' Studies.Depart = timestamp) checkbox
    private By updateStudiesRISHoldFStudiesDepartTimestamp_CheckedBy = 
	By.cssSelector("input[data-ng-model*='IsUpdateStudyOnMarkAsCompleted'][class*='ng-not-empty']");   
    private By updateStudiesRISHoldFStudiesDepartTimestamp_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='IsUpdateStudyOnMarkAsCompleted'][class*='ng-empty']");
    
    //========================================================Action for "Mark as Complete" Section===
    
    
    
    //===LDAP Settings Section=======================================================================
    
    
    //Enable LDAP Authentication checkbox

    private By enableLDAPAuthentication_CheckedBy = 
	By.cssSelector("input[data-ng-model*='IsLdapAuthenticationEnabled'][class*='ng-not-empty']");   
    private By enableLDAPAuthentication_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='IsLdapAuthenticationEnabled'][class*='ng-empty']");
    

    //LDAP Domain textbox
    //Enabled when Enable LDAP Authentication = ON
    @FindBy(css = ("input[data-ng-model*='model.settings.LdapDomain']"))
    private WebElement LDAPDomainTextbox;    
    
    //=======================================================================LDAP Settings Section===
    
    
    
    //===DEMO Section=============================================================================
    
    //Demo Mode checkbox
    private By demoMode_CheckedBy = 
	By.cssSelector("input[data-ng-model*='IsTechpadDemoMode'][class*='ng-not-empty']");   
    private By demoMode_UncheckedBy = 
	By.cssSelector("input[data-ng-model*='IsTechpadDemoMode'][class*='ng-empty']");
    
    
    
    
    //=============================================================================DEMO Section===
    
    
    //===Diagram Mode Section======================================================================
    
    //Diagram Mode  "Small' radio button
    @FindBy(css = ( "input[name='diagramMode'][id='smallMode']"))
    private WebElement demoMode_SmallRadioButton;
    
    //Diagram Mode  "Large' radio button
    @FindBy(css = ( "input[name='diagramMode'][id='largeMode']"))
    private WebElement demoMode_LargeRadioButton;
    
    //Show Diagram Old Mode Warning checkbox
    private By showDiagramOldModeWarning_CheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowDiagramOldModeWarning'][class*='ng-not-empty']");
    private By showDiagramOldModeWarning_UncheckedBy = 
    		By.cssSelector("input[data-ng-model*='IsShowDiagramOldModeWarning'][class*='ng-empty']");
    
    
    //Diagram Old Mode Warning textbox. Shown when Show Diagram Old Mode Warning = ON
    @FindBy(css = ("textarea[rows='3'][data-ng-model*='model.settings.DiagramOldModeWarning']"))
    private WebElement diagramOldModeWarningTextbox;    
    
    //======================================================================Diagram Mode Section===
    

    
    
    //FUNCTIONS
    
    
    
    //---Update (Studies.RISHold = '.F.')----------------------------------------------------------------------------
    @Step(value = "Check: Update (Studies.RISHold = '.F.') on 'End Exam' button click?")
    public void check_UpdateStudiesRISHold_F_onEndExam_buttonClick()
    {
        this.checkCheckbox(
        		updateStudiesRISHold_F_onEndExam_buttonClick_UncheckedBy);
    }
    @Step(value = "Uncheck: Update (Studies.RISHold = '.F.') on 'End Exam' button click?")
    public void uncheck_UpdateStudiesRISHold_F_onEndExam_buttonClick()
    {
        this.uncheckCheckbox(
                updateStudiesRISHold_F_onEndExam_buttonClick_CheckedBy );
    }
    @Step(value = "Change state and return it back for:  Update (Studies.RISHold = '.F.') on 'End Exam' button click? Change the state and return it back.")
    public void doOnOff_UpdateStudiesRISHold_F_onEndExam_buttonClick()
    {
        this.changeCheckboxStateThenRestoreItBack(
        		updateStudiesRISHold_F_onEndExam_buttonClick_CheckedBy,
                updateStudiesRISHold_F_onEndExam_buttonClick_UncheckedBy);
    }
    //----------------------------------------------------------------------------Update (Studies.RISHold = '.F.')---


    //---Update (Studies.T_End = timestamp)-----------------------------------------------------------------------
    @Step(value = "Check: Update (Studies.T_End = timestamp) on 'End Exam' button click?")
    public void check_UpdateStudies_T_End_timestamp_on_EndExam_buttonClick()
    {
        this.checkCheckbox(
                updateStudies_T_End_timestamp_on_EndExam_buttonClick_UncheckedBy);
    }
    @Step(value = "Uncheck: Update (Studies.T_End = timestamp) on 'End Exam' button click?")
    public void uncheck_UpdateStudies_T_End_timestamp_on_EndExam_buttonClick()
    {
        this.uncheckCheckbox(
                updateStudies_T_End_timestamp_on_EndExam_buttonClick_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Update (Studies.T_End = timestamp) on 'End Exam' button click?")
    public void doOnOff_UpdateStudies_T_End_timestamp_on_EndExam_buttonClick()
    {
        this.changeCheckboxStateThenRestoreItBack(
                updateStudies_T_End_timestamp_on_EndExam_buttonClick_CheckedBy,
                updateStudies_T_End_timestamp_on_EndExam_buttonClick_UncheckedBy);
    }
    //-----------------------------------------------------------------------Update (Studies.T_End = timestamp)---


    //---Show Demo Menu Item----------------------------------------------------------------------------------------
    @Step(value = "Check: Show Demo Menu Item")
    public void check_showDemoMenuItem()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                showDemoMenuItem_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Demo Menu Item")
    public void uncheck_showDemoMenuItem()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                showDemoMenuItem_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Demo Menu Item")
    public void doOnOff_showDemoMenuItem()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                showDemoMenuItem_CheckedBy,
                showDemoMenuItem_UncheckedBy);
    }
    //----------------------------------------------------------------------------------------Show Demo Menu Item--- 


    //---Hide Start/End Exam Buttons-----------------------------------------------------------------------------------
    @Step(value = "Check: Hide Start/End Exam Buttons")
    public void check_hideStart_EndExamButtons()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                hideStart_EndExamButtons_UncheckedBy);
    }
    @Step(value = "Uncheck: Hide Start/End Exam Buttons")
    public void uncheck_hideStart_EndExamButtons()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                hideStart_EndExamButtons_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Hide Start/End Exam Buttons")
    public void doOnOff_hideStart_EndExamButtons()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                hideStart_EndExamButtons_CheckedBy,
                hideStart_EndExamButtons_UncheckedBy);
    }
    //-----------------------------------------------------------------------------------Hide Start/End Exam Buttons---


    //---Show Patients Menu Item--------------------------------------------------------------------------------------
    @Step(value = "Check: Show Patients Menu Item")
    public void check_showPatients_MenuItem()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showPatients_MenuItem_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Patients Menu Item")
    public void uncheck_showPatients_MenuItem()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                showPatients_MenuItem_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Patients Menu Item")
    public void doOnOff_showPatients_MenuItem()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                showPatients_MenuItem_CheckedBy,
                showPatients_MenuItem_UncheckedBy);
    }
    //--------------------------------------------------------------------------------------Show Patients Menu Item---


    //---Disable Requiring Studies Fields-------------------------------------------------------------------------------
    @Step(value = "Check: Disable Requiring Studies Fields")
    public void check_disableRequiringStudiesFields()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                disableRequiringStudiesFields_UncheckedBy);
    }
    @Step(value = "Uncheck: Disable Requiring Studies Fields")
    public void uncheck_disableRequiringStudiesFields()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                disableRequiringStudiesFields_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Disable Requiring Studies Fields")
    public void doOnOff_disableRequiringStudiesFields()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                disableRequiringStudiesFields_CheckedBy,
                disableRequiringStudiesFields_UncheckedBy);
    }
    //-------------------------------------------------------------------------------Disable Requiring Studies Fields---


    //---Enable Worklist Patients Data Synchronization------------------------------------------------------------
    @Step(value = "Check: Enable Worklist Patients Data Synchronization")
    public void check_enableWorklistPatientsDataSynchronization()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                enableWorklistPatientsDataSynchronization_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable Worklist Patients Data Synchronization")
    public void uncheck_enableWorklistPatientsDataSynchronization()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                enableWorklistPatientsDataSynchronization_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable Worklist Patients Data Synchronization")
    public void doOnOff_enableWorklistPatientsDataSynchronization()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                enableWorklistPatientsDataSynchronization_CheckedBy,
                enableWorklistPatientsDataSynchronization_UncheckedBy);
    }
    //------------------------------------------------------------Enable Worklist Patients Data Synchronization---      


    //---Show Select Second Technologist-----------------------------------------------------------------------------
    @Step(value = "Check: Show Select Second Technologist")
    public void check_showSelectSecondTechnologist()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                showSelectSecondTechnologist_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Select Second Technologist")
    public void uncheck_showSelectSecondTechnologist()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                showSelectSecondTechnologist_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Select Second Technologist")
    public void doOnOff_showSelectSecondTechnologist()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                showSelectSecondTechnologist_CheckedBy,
                showSelectSecondTechnologist_UncheckedBy);
    }
    //-----------------------------------------------------------------------------Show Select Second Technologist---      


    //---Show Speed Test Menu Item----------------------------------------------------------------------------------
    @Step(value = "Check: Show Speed Test Menu Item")
    public void check_showSpeedTestMenuItem()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                showSpeedTestMenuItem_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Speed Test Menu Item")
    public void uncheck_showSpeedTestMenuItem()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                showSpeedTestMenuItem_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Speed Test Menu Item")
    public void doOnOff_showSpeedTestMenuItem()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                showSpeedTestMenuItem_CheckedBy,
                showSpeedTestMenuItem_UncheckedBy);
    }
    //-----------------------------------------------------------------------------Show Select Second Technologist---               


    //---Enable survey history mode-----------------------------------------------------------------------------------
    @Step(value = "Check: Enable survey history mode")
    public void check_enableSurveyHistoryMode()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                enableSurveyHistoryMode_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable survey history mode")
    public void uncheck_enableSurveyHistoryMode()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                enableSurveyHistoryMode_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable survey history mode")
    public void doOnOff_enableSurveyHistoryMode()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                enableSurveyHistoryMode_CheckedBy,
                enableSurveyHistoryMode_UncheckedBy);
    }
    //-----------------------------------------------------------------------------------Enable survey history mode---                


    //---Included Personal BRCA History-------------------------------------------------------------------------------
    @Step(value = "Check: Included Personal BRCA History")
    public void check_includedPersonalBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                includedPersonalBRCAHistory_UncheckedBy);
    }
    @Step(value = "Uncheck: Included Personal BRCA History")
    public void uncheck_includedPersonalBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                includedPersonalBRCAHistory_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Included Personal BRCA History")
    public void doOnOff_includedPersonalBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                includedPersonalBRCAHistory_CheckedBy,
                includedPersonalBRCAHistory_UncheckedBy);
    }
    //-------------------------------------------------------------------------------Included Personal BRCA History---              


    //---Included Family BRCA History----------------------------------------------------------------------------------
    @Step(value = "Check: Included Family BRCA History")
    public void check_includedFamilyBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                includedFamilyBRCAHistory_UncheckedBy);
    }
    @Step(value = "Uncheck: Included Family BRCA History")
    public void uncheck_includedFamilyBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                includedFamilyBRCAHistory_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Included Family BRCA History")
    public void doOnOff_includedFamilyBRCAHistory()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                includedFamilyBRCAHistory_CheckedBy,
                includedFamilyBRCAHistory_UncheckedBy);
    }
    //-------------------------------------------------------------------------------Included Personal BRCA History---              


    //---Enable ability to create PDF documents----------------------------------------------------------------------
    @Step(value = "Check: Enable ability to create PDF documents")
    public void check_enableAbilityToCreatePDFDocuments()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
                enableAbilityToCreatePDFDocuments_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable ability to create PDF documents")
    public void uncheck_enableAbilityToCreatePDFDocuments()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
                enableAbilityToCreatePDFDocuments_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable ability to create PDF documents")
    public void doOnOff_enableAbilityToCreatePDFDocuments()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
                enableAbilityToCreatePDFDocuments_CheckedBy,
                enableAbilityToCreatePDFDocuments_UncheckedBy);
    }
    //----------------------------------------------------------------------Enable ability to create PDF documents---              


    //---Hide Skip/Start Survey Buttons on Study Details page----------------------------------------------------
    @Step(value = "Check: Hide Skip/Start Survey Buttons on Study Details page")
    public void check_hideSkipStartSurveyButtonsOnStudyDetailsPage()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		hideSkipStartSurveyButtonsOnStudyDetailsPage_UncheckedBy);
    }
    @Step(value = "Uncheck: Hide Skip/Start Survey Buttons on Study Details page")
    public void uncheck_hideSkipStartSurveyButtonsOnStudyDetailsPage()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		hideSkipStartSurveyButtonsOnStudyDetailsPage_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Hide Skip/Start Survey Buttons on Study Details page")
    public void doOnOff_hideSkipStartSurveyButtonsOnStudyDetailsPage()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		hideSkipStartSurveyButtonsOnStudyDetailsPage_CheckedBy,
        		hideSkipStartSurveyButtonsOnStudyDetailsPage_UncheckedBy);
    }
    //----------------------------------------------------Hide Skip/Start Survey Buttons on Study Details page---     


    //---Disable Store Risk Trigger for worklist save-----------------------------------------------------------------
    @Step(value = "Check: Disable Store Risk Trigger for worklist save")
    public void check_disableStoreRiskTriggerForWorklistSave()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		disableStoreRiskTriggerForWorklistSave_UncheckedBy);
    }
    @Step(value = "Uncheck: Disable Store Risk Trigger for worklist save")
    public void uncheck_disableStoreRiskTriggerForWorklistSave()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		disableStoreRiskTriggerForWorklistSave_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Disable Store Risk Trigger for worklist save")
    public void doOnOff_disableStoreRiskTriggerForWorklistSave()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		disableStoreRiskTriggerForWorklistSave_CheckedBy,
        		disableStoreRiskTriggerForWorklistSave_UncheckedBy);
    }
    //-----------------------------------------------------------------Disable Store Risk Trigger for worklist save---


    //---Enable reload documents on each click on Documents tab----------------------------------------------
    @Step(value = "Check: Enable reload documents on each click on Documents tab")
    public void check_enableReloadDocumentsOnEachClickOnDocumentsTab()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		enableReloadDocumentsOnEachClickOnDocumentsTab_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable reload documents on each click on Documents tab")
    public void uncheck_enableReloadDocumentsOnEachClickOnDocumentsTab()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		enableReloadDocumentsOnEachClickOnDocumentsTab_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable reload documents on each click on Documents tab")
    public void doOnOff_enableReloadDocumentsOnEachClickOnDocumentsTab()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		enableReloadDocumentsOnEachClickOnDocumentsTab_CheckedBy,
        		enableReloadDocumentsOnEachClickOnDocumentsTab_UncheckedBy);
    }
    //----------------------------------------------Enable reload documents on each click on Documents tab---

    
    //---Validate required fields on every exam save----------------------------------------------------------------
    @Step(value = "Check: Validate required fields on every exam save")
    public void check_validateRequiredFieldsOnEveryExamSave()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		validateRequiredFieldsOnEveryExamSave_UncheckedBy);
    }
    @Step(value = "Uncheck: Validate required fields on every exam save")
    public void uncheck_validateRequiredFieldsOnEveryExamSave()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		validateRequiredFieldsOnEveryExamSave_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Validate required fields on every exam save")
    public void doOnOff_validateRequiredFieldsOnEveryExamSave()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		validateRequiredFieldsOnEveryExamSave_CheckedBy,
        		validateRequiredFieldsOnEveryExamSave_UncheckedBy);
    }
    //----------------------------------------------------------------Validate required fields on every exam save---  
    
    
    //---Enable Dashboard Mode----------------------------------------------------------------------------------------
    @Step(value = "Check: Enable Dashboard Mode")
    public void check_enableDashboardMode()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		enableDashboardMode_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable Dashboard Mode")
    public void uncheck_enableDashboardMode()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		enableDashboardMode_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable Dashboard Mode")
    public void doOnOff_enableDashboardMode()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		enableDashboardMode_CheckedBy,
        		enableDashboardMode_UncheckedBy);
    }
    //----------------------------------------------------------------------------------------Enable Dashboard Mode---


    //---Show barcode for accession number-------------------------------------------------------------------------
    @Step(value = "Check: Show barcode for accession number")
    public void check_showBarcodeForAccessionNumber()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showBarcodeForAccessionNumber_UncheckedBy);
    }
    @Step(value = "Uncheck: Show barcode for accession number")
    public void uncheck_showBarcodeForAccessionNumber()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showBarcodeForAccessionNumber_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show barcode for accession number")
    public void doOnOff_showBarcodeForAccessionNumber()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showBarcodeForAccessionNumber_CheckedBy,
        		showBarcodeForAccessionNumber_UncheckedBy);
    }
    
    @Step(value = "click dropdown menu Barcode Types")
    public void clickBarcodeDropdown() {
    	this.sleep(500);
    	this.click(barcodeTypeDropdown);
    	this.sleep(500);
    }
    
    @Step(value = "Select an option from dropdown menu Barcode Types by option text")
    public void selectOptionFromBarcodeDropdownByText(String optionText) {
    	this.sleep(500);
    	//this.selectListOptionBYText(barcodeDropdownCSS, optionText);
    	this.clickElementFromListByVisibleText(barcodeTypeDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Barcode Types by option index")
    public void selectOptionFromBarcodeDropdownByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(barcodeTypeDropdownList, i-1);
    	this.sleep(500);
    }    
    
    //-------------------------------------------------------------------------Show barcode for accession number--- 
    

    //---Show Chat Menu Item------------------------------------------------------------------------------------------
    @Step(value = "Check: Show Chat Menu Item")
    public void check_showChatMenuItem()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showChatMenuItem_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Chat Menu Item")
    public void uncheck_showChatMenuItem()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showChatMenuItem_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Chat Menu Item")
    public void doOnOff_showChatMenuItem()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showChatMenuItem_CheckedBy,
        		showChatMenuItem_UncheckedBy);
    }

    @Step(value = "Enter Text into Techpad Diagram Redirect URL textbox")
    public void enterTextIntoTechpadDiagramRedirectURLTextbox (String text) {
    	this.enterText(techpadDiagramRedirectURLTextbox, text);
    }
    
    
    //------------------------------------------------------------------------Show barcode for accession number---

     
    //---Allow to change worklist status from completed----------------------------------------------------------
    @Step(value = "Check: Show Chat Menu Item")
    public void check_allowToChangeWorklistStatusFromCompleted()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		allowToChangeWorklistStatusFromCompleted_UncheckedBy);
    }
    @Step(value = "Uncheck: Allow to change worklist status from completed")
    public void uncheck_allowToChangeWorklistStatusFromCompleted()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		allowToChangeWorklistStatusFromCompleted_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Allow to change worklist status from completed")
    public void doOnOff_allowToChangeWorklistStatusFromCompleted()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		allowToChangeWorklistStatusFromCompleted_CheckedBy,
        		allowToChangeWorklistStatusFromCompleted_UncheckedBy);
    }
    
    
    @Step(value = "Click  dropdown menu 'Allow to change worklist status from completed' ")
    public void clickAllowToChangeWorklistStatusDropdown() {
    	this.sleep(500);
    	this.click(allowedStatusToBeChangedFromCompletedDropdown);
    	this.sleep(500);
    }  
    
    @Step(value = "Select an option from dropdown menu 'Allow to change worklist status from completed' by option text")
    public void selectOptionFromAllowToChangeWorklistStatusDropdownByText(String optionText) {
    	this.sleep(500);
    	this.clickElementFromListByVisibleText(allowedStatusToBeChangedFromCompletedDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu 'Allow to change worklist status from completed' by option index")
    public void selectOptionFromAllowToChangeWorklistStatusDropdownByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(allowToChangeWorklistStatusDropdownList, i-1);
    	this.sleep(500);
    }  
    //----------------------------------------------------------Allow to change worklist status from completed---
  

    //---Enable Techpad Diagram redirection-------------------------------------------------------------------------
    @Step(value = "Check: Enable Techpad Diagram redirection")
    public void check_enableTechpadDiagramRedirection()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		enableTechpadDiagramRedirection_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable Techpad Diagram redirection")
    public void uncheck_enableTechpadDiagramRedirection()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		enableTechpadDiagramRedirection_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Enable Techpad Diagram redirection")
    public void doOnOff_enableTechpadDiagramRedirection()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		enableTechpadDiagramRedirection_CheckedBy,
        		enableTechpadDiagramRedirection_UncheckedBy);
    }
       
    //-------------------------------------------------------------------------Enable Techpad Diagram redirection---


        
    //---Show Diagram Old Mode Warning-----------------------------------------------------------------------------
    @Step(value = "Check: Show Diagram Old Mode Warning")
    public void check_showDiagramOldModeWarning()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showDiagramOldModeWarning_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Diagram Old Mode Warning")
    public void uncheck_showDiagramOldModeWarning()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showDiagramOldModeWarning_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Diagram Old Mode Warning")
    public void doOnOff_showDiagramOldModeWarning()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showDiagramOldModeWarning_CheckedBy,
        		showDiagramOldModeWarning_UncheckedBy);
    }
    
    
    
    @Step(value = "Enter Text into Diagram Old Mode Warning textbox")
    public void enterTextIntoDiagramOldModeWarningTextbox (String text) {
    	this.enterText(diagramOldModeWarningTextbox, text);
    }
    
    //-----------------------------------------------------------------------------Show Diagram Old Mode Warning---


    //---Show Group "Exam Type" Worklist Filter----------------------------------------------------------------------
    @Step(value = "Check: Group \"Exam Type\" Worklist Filter")
    public void check_groupExamTypeWorklistFilter()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		groupExamTypeWorklistFilter_UncheckedBy);
    }
    @Step(value = "Uncheck: Group \"Exam Type\" Worklist Filter")
    public void uncheck_groupExamTypeWorklistFilter()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		groupExamTypeWorklistFilter_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Group \"Exam Type\" Worklist Filter")
    public void doOnOff_groupExamTypeWorklistFilter()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		groupExamTypeWorklistFilter_CheckedBy,
        		groupExamTypeWorklistFilter_UncheckedBy);
    }
    //----------------------------------------------------------------------Show Group "Exam Type" Worklist Filter---


    //---Show Tyrer Cuzik Tab-----------------------------------------------------------------------------------------------
    @Step(value = "Check: Show Tyrer Cuzik Tab")
    public void check_showTyrerCuzikTab()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showTyrerCuzikTab_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Tyrer Cuzik Tab")
    public void uncheck_showTyrerCuzikTab()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showTyrerCuzikTab_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Tyrer Cuzik Tab")
    public void doOnOff_showTyrerCuzikTab()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showTyrerCuzikTab_CheckedBy,
        		showTyrerCuzikTab_UncheckedBy);
    }
    //-----------------------------------------------------------------------------------------------Show Tyrer Cuzik Tab---

  
    
        
    //---Default Worklist Tab------------------------------------------------------------------------------------------------- 
    @Step(value = "Select an option from dropdown menu 'Default Worklist Tab' by option text")
    public void clickDefaultWorklistTabsDropdown() {
    	this.sleep(500);
    	this.click(defaultWorklistTabDropdown);
    	//this.defaultWorklistTabDropdown.click();
    	this.sleep(500);
    }    
      
    @Step(value = "Select an option from dropdown menu 'Default Worklist Tabd' by option index")
    public void selectOptionFromDefaultWorklistTabDropdownByIndex(int i) {
    	
    	if (i == 1) {
    		this.click(options.get(0));
    	}
    	else if (i == 2) {
    		this.click(defaultWorklistTabDropdownOption2);
    	}
    	else if (i == 3) {
    		this.click(defaultWorklistTabDropdownOption3);
    	}
    	else if (i == 4) {
    		this.click(defaultWorklistTabDropdownOption4);
    	}
    	else if (i == 5) {
    		this.click(defaultWorklistTabDropdownOption5);
    	}
    	else if (i == 6) {
    		this.click(defaultWorklistTabDropdownOption6);
    	}
    	else if (i == 7) {
    		this.click(defaultWorklistTabDropdownOption7);
    	}
    	else if (i == 8) {
    		this.click(defaultWorklistTabDropdownOption8);
    	}
    	else if (i == 9) {
    		this.click(defaultWorklistTabDropdownOption9);
    	}
    	this.sleep(1000);
    } 
    
    //-------------------------------------------------------------------------------------------------Default Worklist Tab---   

    
    
    //---Radio Buttons---------------------------------------------------------------------------------------------------------
    @Step(value = "Click Diagram Carry Forward ON radio button")
    public void clickDiagramCarryForward_OnRadioButton() {
    	this.sleep(500);
    	this.click(diagramCarryForward_OnRadioButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Click Diagram Carry Forward OFF radio button")
    public void clickDiagramCarryForward_OFFRadioButton() {
    	this.sleep(500);
    	this.click(diagramCarryForward_OffRadioButton);
    	this.sleep(500);
    } 
    

    
    @Step(value = "Click Com Object ID ON radio button")
    public void clickComObjectID_ProductionRadioButton() {
    	this.sleep(500);
    	this.click(comObjectID_OnRadioButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Click Com Object ID OFF radio button")
    public void clickComObjectID_TestRadioButton() {
    	this.sleep(500);
    	this.click(comObjectID_OffRadioButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Click Time Format  Days radio button")
    public void clickTimeFormat_DaysRadioButton() {
    	this.sleep(500);
    	this.click(timeFormat_DaysRadioButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Time Format  Always radio button")
    public void clickTimeFormat_AlwaysRadioButton() {
    	this.sleep(500);
    	this.click(timeFormat_AlwaysRadioButton);
    	this.sleep(500);
    } 
    
    
    @Step(value = "Click Diagram Mode  Small radio button")
    public void clickDiagramMode_SmallRadioButton() {
    	this.sleep(500);
    	this.click(demoMode_SmallRadioButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Click Diagram Mode  Large radio button")
    public void clickDiagramMode_LargeRadioButton() {
    	this.sleep(500);
    	this.click(demoMode_LargeRadioButton);
    	this.sleep(500);
    }


    
    @Step(value = "Click Family Tree Survey dropdown list")
    public void clickFamilyTreeSurveyNameDropdown() {
    	this.sleep(500);
    	this.click(familyTreeSurveyNameDropdown);
    	this.sleep(500);
    } 
    
    @Step(value = "Select an option from dropdown menu 'Family Tree Survey' by option text")
    public void selectOptionFromFamilyTreeSurveyNameDropdownByText(String optionText) {
    	this.sleep(500);
    	//this. clickElementFromListByOptionText(familyTreeSurveyNameDropdownList, optionText);
    	this.clickElementFromListByVisibleText(familyTreeSurveyNameDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu 'Family Tree Survey' by option index")
    public void  selectOptionFromFamilyTreeSurveyNameDropdownByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(familyTreeSurveyNameDropdownList, i-1);
    	this.sleep(500);
    }     
    
    
    
    @Step(value = "Enter Text into Enabled Codes on FamilyTree textbox")
    public void enterTextIntoEnabledCodesOnFamilyTreeTextbox (String text) {
    	this.enterText(enabledCodesOnFamilyTreeTextbox, text);
    }
    
    @Step(value = "Enter Text into Worklist Prioritizing Accuracy textbox")
    public void enterTextIntoWorklistPrioritizingAccuracyTextbox (String text) {
    	this.enterText(worklistPrioritizingAccuracyTextbox, text);
    }
    
    @Step(value = "Click Reset Various Options button")
    public void clickResetVariousOptionsButton() {
    	this.sleep(500);
    	this.click(resetVariousOptionsButton);
    	this.sleep(500);
    } 
    
    @Step(value = "Enter Text into  Session Warn Timeout textbox")
    public void enterTextIntoSessionWarnTimeoutTextbox (String text) {
    	this.enterText(sessionWarnTimeoutTextbox, text);
    }
    
    @Step(value = "Enter Text into History Load Retries textbox")
    public void enterTextIntoHistoryLoadRetriesTextbox (String text) {
    	this.enterText(historyLoadRetriesTextbox, text);
    }
    
    @Step(value = "Enter Text into Session Expiration Timeout textbox")
    public void enterTextIntoSessionExpirationTimeoutTextbox (String text) {
    	this.enterText(sessionExpirationTimeoutTextbox, text);
    }
    

    
    @Step(value = "Check: Show Total Time")
    public void check_ShowTotalTimeOptions()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showTotalTimeOptions_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Total Time")
    public void uncheck_ShowTotalTimeOptions()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showTotalTimeOptions_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Total Time")
    public void doOnOff_ShowTotalTimeOptions()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showTotalTimeOptions_CheckedBy,
        		showTotalTimeOptions_UncheckedBy);
    }
    

    
    @Step(value = "Check: Show Detail Time")
    public void check_showDetailTime()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showDetailTime_UncheckedBy);
    }
    @Step(value = "Uncheck: Show Detail Time")
    public void uncheck_showDetailTime()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showDetailTime_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show Detail Time")
    public void doOnOff_showDetailTime()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showDetailTime_CheckedBy,
        		showDetailTime_UncheckedBy);
    }
    
    
    @Step(value = "Check: Show Comment Time")
    public void check_showCommentTime()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		showCommentTime_UncheckedBy);
    }
    @Step(value = "Uncheck:  Show Comment Time")
    public void uncheck_showCommentTime()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		showCommentTime_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Show  Comment Time")
    public void doOnOff_showCommentTime()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		showCommentTime_CheckedBy,
        		showCommentTime_UncheckedBy);
    }
    
    
    @Step(value = "Check: Use alternative patient identifier checkbox")
    public void check_useAlternativePatientIdentifier()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		useAlternativePatientIdentifie_UncheckedBy);
    }
    @Step(value = "Uncheck: Use alternative patient identifier checkbox")
    public void uncheck_useAlternativePatientIdentifier()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		useAlternativePatientIdentifier_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Use alternative patient identifier checkbox")
    public void doOnOff_useAlternativePatientIdentifier()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		useAlternativePatientIdentifier_CheckedBy,
        		useAlternativePatientIdentifie_UncheckedBy);
    }
    

    
    @Step(value = "Click Patient Identifier Fields dropdown list")
    public void clickPatientIdentifierFieldsDropdown() {
    	this.sleep(500);
    	this.click(patientIdentifierFieldsDropdown);
    	this.sleep(500);
    } 
    
    @Step(value = "Select an option from dropdown menu 'Patient Identifier Fields' by option text")
    public void selectOptionFromPatientIdentifierFieldsDropdownByText(String optionText) {
    	this.sleep(500);
    	//this. clickElementFromListByOptionText(patientIdentifierFieldsDropdownList, optionText);
    	this.clickElementFromListByVisibleText(patientIdentifierFieldsDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu 'Patient Identifier Fields' by option index")
    public void  selectOptionFromPatientIdentifierFieldsByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(patientIdentifierFieldsDropdownList, i-1);
    	this.sleep(500);
    }
    
    

    @Step(value = "Check: Update (Studies.RISHold = '.F.' Studies.Depart = timestamp) checkbox")
    public void check_updateStudiesRISHoldFStudiesDepartTimestamp()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		updateStudiesRISHoldFStudiesDepartTimestamp_UncheckedBy);
    }
    @Step(value = "Uncheck: Update (Studies.RISHold = '.F.' Studies.Depart = timestamp) checkbox")
    public void uncheck_updateStudiesRISHoldFStudiesDepartTimestamp()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		updateStudiesRISHoldFStudiesDepartTimestamp_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Update (Studies.RISHold = '.F.' Studies.Depart = timestamp) checkbox")
    public void doOnOff_updateStudiesRISHoldFStudiesDepartTimestamp()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		updateStudiesRISHoldFStudiesDepartTimestamp_CheckedBy,
        		updateStudiesRISHoldFStudiesDepartTimestamp_UncheckedBy);
    }
    

    
    
    @Step(value = "Check: Enable LDAP Authentication checkbox")
    public void check_enableLDAPAuthentication()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		enableLDAPAuthentication_UncheckedBy);
    }
    @Step(value = "Uncheck: Enable LDAP Authentication checkbox")
    public void uncheck_enableLDAPAuthentication()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		enableLDAPAuthentication_CheckedBy );
    }
    @Step(value = "Change state and return it back for:  Enable LDAP Authentication checkbox")
    public void doOnOff_enableLDAPAuthentication()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		enableLDAPAuthentication_CheckedBy,
        		enableLDAPAuthentication_UncheckedBy);
    }
    
    
    @Step(value = "Enter Text into LDAP Domain textbox")
    public void enterTextIntoLDAPDomainTextbox (String text) {
    	this.enterText(LDAPDomainTextbox, text);
    }
    
    @Step(value = "Enter Text into Chat service url textbox")
    public void enterTextIntoChatServiceUrlTextboxTextbox (String text) {
    	this.enterText(chatServiceUrlTextbox, text);
    }  

    
    

    @Step(value = "Check: Demo Mode checkbox")
    public void check_demoMode()
    {
    	this.waitAllRequests(3);
        this.checkCheckbox(
        		demoMode_UncheckedBy);
    }
    @Step(value = "Uncheck: Demo Mode checkbox")
    public void uncheck_demoMode()
    {
    	this.waitAllRequests(3);
        this.uncheckCheckbox(
        		demoMode_CheckedBy);
    }
    @Step(value = "Change state and return it back for:  Demo Mode checkbox")
    public void doOnOff_demoMode()
    {
    	this.waitAllRequests(3);
        this.changeCheckboxStateThenRestoreItBack(
        		demoMode_CheckedBy,
        		demoMode_UncheckedBy);
    }
    
    
    @Step(value = "Select an option from dropdown menu Default Worklist Tab by option text")
    public void selectDefaultWorklistTabDropdownByText(String optionText) {
    	this.sleep(500);
    	//this.selectListOptionBYText(barcodeDropdownCSS, optionText);
    	this.clickElementFromListByVisibleText(defaultWorklistTabDropdown, optionText);
    	this.sleep(500);
    }    
    
    @Step(value = "Select an option from dropdown menu Default Worklist Tab by option index")
    public void selectDefaultWorklistTabDropdownByIndex(int i) {
    	this.sleep(500);	
    	this.clickElementFromListByIndex(defaultWorklistTabDropdownList, i-1);
    	this.sleep(500);
    }    
    
    @Step(value = "Click Save Button")
    public void clickSaveButton() {
    	this.sleep(500);	
    	this.click(saveButton);
    	this.waitAllRequests();
    	this.sleep(500);
    } 
    
    
}

