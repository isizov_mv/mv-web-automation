package Utilities;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Hashtable;
import java.util.Map.Entry;

public class ServerConnection {
    
    private java.sql.Connection con = null;
    
    private final String url = "jdbc:sqlserver://";
    private final String queryTimeout="100";
    private final String userName = "magviewdb";
    private final String password = "MagView#1";
    
    public ServerConnection() {
    }

    private String getConnectionUrl(String serverName, String databaseName) {   	
    	return url + serverName +  ";databaseName=" + databaseName +";queryTimeout=" + queryTimeout;
    }
    
    private java.sql.Connection getConnection(String serverName, String databaseName) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = java.sql.DriverManager.getConnection(getConnectionUrl(serverName, databaseName), userName, password);
            if (con != null) {
                System.out.println("Connection Successful!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error Trace in getConnection() : " + e.getMessage());
        }
        return con;
    }
    
    
    
    public void displayDbProperties(String serverName, String databaseName) {
        java.sql.DatabaseMetaData dm = null;
        java.sql.ResultSet rs = null;
        try {
            con = this.getConnection(serverName, databaseName);
            Statement statement = con.createStatement();
            if (con != null) {
                dm = con.getMetaData();
                System.out.println("Driver Information");
                System.out.println("\tDriver Name: " + dm.getDriverName());
                System.out.println("\tDriver Version: " + dm.getDriverVersion());
                System.out.println("\nDatabase Information ");
                System.out.println("\tDatabase Name: " + dm.getDatabaseProductName());
                System.out.println("\tDatabase Version: " + dm.getDatabaseProductVersion());

                System.out.println("Avalilable Catalogs ");
                rs = dm.getCatalogs();
                while (rs.next()) {
                    System.out.println("\tcatalog: " + rs.getString(1));
                }
       
                rs.close();
                rs = null;
                closeConnection();
            } else {
                System.out.println("Error: No active Connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dm = null;
    }
    

	
    public void addNewPatient(String ssn, String lname, String fname, String birthday, String server, String database) {
        java.sql.DatabaseMetaData dm = null;
        java.sql.ResultSet rs = null;
        try {
            con = this.getConnection(server, database);
            Statement statement = con.createStatement();
            if (con != null) {
                dm = con.getMetaData();

                ResultSet resultSet = null;
                
                //---Check if the ssn is already used in PATIENTS table --------------------------------------
                String selectSql = "SELECT SSN FROM PATIENTS";
                resultSet = statement.executeQuery(selectSql);
                ResultSetMetaData rsmd = resultSet.getMetaData();  
                String ssnValues = "";
        		while (resultSet.next()) {                 		
        			ssnValues = ssnValues + ( resultSet.getString(1)).trim() + ", ";
            	}     
                                  
        		if (ssnValues.contains(ssn) == false) {
               
                    //get first row as a template from patients table
                    selectSql = "SELECT TOP 1 * FROM PATIENTS";
                    resultSet = statement.executeQuery(selectSql);
                    rsmd = resultSet.getMetaData();                 
                    int columnsNumber = rsmd.getColumnCount();
                    
                    // creating a patient Dictionary
                    //key: column name
                    //value: column value
            		Hashtable<String, String> patientDict = new Hashtable<String, String>();
            		while (resultSet.next()) {
                    	for (int i = 0; i < columnsNumber; i++) {                   		
                    		patientDict.put(rsmd.getColumnName(i+1), resultSet.getString(i+1));
                  		} 
                	}
            		System.out.println();
            		//change the new patient data
            		patientDict.put("SSN", ssn);
                	patientDict.put("LNAME", lname);
                	patientDict.put("FNAME",  fname);
                	patientDict.put("DOB",  birthday);
                	patientDict.put("STREET",  "123 Somewhere St");
                	patientDict.put("CITY",  "Burton");
                	patientDict.put("PCELL",  "2223334455");
                	
                	//----prepare SQL INSERT query and run it--------------------------------------------------
                	String  values = "";
                	String columns="";        	
                	for (Entry<String, String> entry : patientDict.entrySet()) {
                	    String key = entry.getKey();
                	    String val = entry.getValue();
                	    columns = columns + key.trim() + ", ";
                	    if (val.trim().isEmpty()){
                	    	values = values + "'', ";
                	    }
                	    else {
                	    	values = values + "'" + val.trim() + "', ";
                	    }          	    
                	}
                	values = values.substring(0, values.length() - 2);
                	columns = columns.substring(0, columns.length() - 2);            	
                	String insertQuery = "INSERT INTO PATIENTS (" + columns + ") VALUES (" + values + ")";
                	System.out.println("---  Insert new patient query ---");
                	System.out.println(insertQuery);
                	System.out.println();
                	statement.executeQuery(insertQuery);
                	//--------------------------------------------------prepare SQL INSERT query and run it----           	
                	
            		}
        		else {
        			System.out.println();
        			System.out.println("SSN : '" + ssn + "' exists in PATIENTS table");
        			System.out.println("The patient will not be added");
        			System.out.println();
        		}
         
                rs.close();
                rs = null;
                closeConnection();
            } else {
                System.out.println("Error: No active Connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dm = null;
    }
    

    public void addNewStudy(String ssn, String server, String database) {
    	
    	
    	String[] columnsArray = {"SSN","SDATE","STUDY","ACCESSION","LINKED_TO","DSTATUS","RISHOLD","INIT","FULLREP","VTYPE","LOC_NUM","REF_NUM","SPROCS","PROBLEM","REASON","VIEWS","QUEUE","FTISSUE","FRISK","INTERVAL","INT_DATE","RECALL_FOR","LNUM","LNUM_DATES","LETTER","FU","FUDATE","FUSAVED","COMPARE","HEIGHT","WEIGHT","PARITY","GRAVIDA","PREGNANT","LMP","M_AGE","P_AGE","A_AGE","FIRST_MAM","IMPLANT","HISTORY","TECH_INIT","TRAN_INIT","TINIT","RESIDENT","LUPDATE","LUPTIME","FLUPDATE","SECTIONS","CASE","CNUM","STYPE","DFILE","MFILE","SITE","T_ARRIVE","T_BEGIN","T_END","T_OUT","T_DEPART","DT_MVQ","DT_DICT","DT_TRAN","DT_FINAL","DT_REL","IFILE","DICTSTAT","DICTADDEND","CADSTATE","UTECHNIQUE","INIT2","INIT2AGREE","INIT3","INIT3AGREE","INIT4","INIT4AGREE","TECH_INIT2","TECH_INIT3","PROC_OC","ICP_DONE","OMITNUMBER","COMMENT","CBE","RSURG_NONE","BSURG_NONE","PBCX_NONE","POCX_NONE","FBCX_NONE","FOCX_NONE","CONT_NONE","HORM_NONE","THER_NONE","CSURG_NONE","BPROC_NONE","MMED_NONE","FOCX_UNK","FBCX_UNK","EDITED","SENDREPORT","BRASIZE","ALCOHOL","SMOKE","BOTODAY","BOPT","BO16","BO25","BO40","ASHKENAZI","LFILE","QDONE","FILMSFROM","ESTATUS","CSTATUS","PREGTRY","LACTFREQ","LACTATING","REF_NUM2","REF_NUM3","REF_NUM4","REF_NUM5","REF_NUM6","REF_NUM7","REF_NUM8","REF_NUM9","REF_NUM10","RESIDENT2","FUTOADD","ACCOUNT","ORDERID","RIS_PROCS","SCHCOM","OMITIMPNEG","FINDNOTE","CONDOC_ID","DRA1","DRA2","DRA3","DRR1","DRR2","DRR3","READ_LOC","FORCE_TN","BRCA1","BRCA2","FUTOADD2","FUTOADD3","ID","RIS_ACC","RV_ACC","HISTCARRY","ORIG_INIT","EHR_LOAD","CPOE","IBISHIS","BRCATEST","OFFICE","LUNGRADS_S","LUNGRADS_C","SMOKE_P","SMOKE_Y","SMOKE_PY","SMOKE_QY","CMHEIGHT","KGWEIGHT","U1","U2","U3","U4","U5","U6","U7","U8","U9","U10","U11","U12","U13","U14","U15","U16","U17","U18","U19","U20","U21","U22","U23","U24","U25","U26","U27","U28","U29","U30","U31","U33","U32","U34","U35","U36","U37","U38","U39","U40","U41","U42","U43","U44","U45","U46","U47","U48","U49","PSEDIT","MBPE_R","MBPE_SYM","MBPE_LEVEL","RESIDENT3","RESIDENT4","UTISSUE","ATTEST","INCIDENTAL","IBISUDAU","IBISUSIS","IBISUPHSIS","IBISUMHSIS","IBISUPAUNT","IBISUMAUNT","LRVERSION","COMORTAL","ASHKSIDE","POLYSNP","FBRCA_NONE","PROCCHANGE","SMOKE_QD"};
    	String[] valuesArray = {"388114419","2021-01-28","2021-01-28","100033","1900-01-01","","False","","","U","1","1234","DB-B","UB","","B,CCB,MLO","","","C,Q","","1900-01-01","","0","","","","1900-01-01","","","0","0","3","4","N","1900-01-01","52","20","12","N","","","ASI","","","","2021-01-08","17:54:42","1900-01-01","400L01Mammogram-1MAMMOGRAM FINDINGS:500L01Ultrasound-2ULTRASOUND FINDINGS:700L01Ductogram-3Ductogram Findings0","","0","","","","","17:54","","","","","1900-01-01","1900-01-01","1900-01-01","1900-01-01","1900-01-01","","False","False","False","","","","","","","","","","","False","False","","","False","False","False","False","False","False","False","False","False","False","False","False","False","False","False","True","","","9","","","","","","N","","False","","","","","","","","","","","","","","","","","","","","","","False","","","","","","","","","","False","","","","","S00112E2X1","","","True","","False","False","False","","False","False","False","0","0.0","0.0","0.0","0","0","1900-01-01","1900-01-01","","","","","","","","Y-","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","False","","","","","","","","False","","","","","","","","","","0.0","False","True","1900-01-01"};
    	//System.out.println("columns " + String.valueOf(columnsArray.length));
    	//System.out.println("values " + String.valueOf(valuesArray.length));
    	
    	//SSN
    	valuesArray[0] = ssn;
    	//ACCESSION #
    	valuesArray[3] = "AccNum" + ssn;
    	
    	LocalDate ldate = LocalDate.now().minusDays(2);  	
    	String date = ldate.toString();
  	
    	//set study date 2 days back from today's date
    	//SDATE
    	valuesArray[1] = date;	
    	//STUDY
    	valuesArray[2] = date;   	
    	
        java.sql.DatabaseMetaData dm = null;
        java.sql.ResultSet rs = null;
        try {
            con = this.getConnection(server, database);
            Statement statement = con.createStatement();
            if (con != null) {
                dm = con.getMetaData();
                ResultSet resultSet = null;
                
                //---Check if the accession # is already used in STUDIES table----------------------------
                String selectSql = "SELECT Accession FROM STUDIES";
                resultSet = statement.executeQuery(selectSql);
                ResultSetMetaData rsmd = resultSet.getMetaData();  
                String accessionValues = "";
        		while (resultSet.next()) {                 		
        			accessionValues = accessionValues + (resultSet.getString(1)).trim() + ", ";
            	}     

        		if (accessionValues.contains(valuesArray[3]) == false) {
        			//----prepare SQL INSERT query and run it--------------------------------------------------
            		String columns ="";
            		String values = "";
                    int i = 0;
                    for(String s : columnsArray) {
                    	if (i == 0) {
                    		columns = columns + "[" + s + "]";
                    	}
                    	else {
                    		columns = columns + ", " + "[" + s + "]";
                    	}
                    	i++;
                    }
                    
                    i = 0;
                    for(String s : valuesArray) {
                    	if (i == 0) {
                    		values = values + "'" + s + "'";
                    	}
                    	else {
                    		values = values + ", '" + s + "'";
                    	}
                    	i++;
                    } 
                          	         	
                	String insertQuery = "INSERT INTO STUDIES (" + columns + ") VALUES (" + values + ")";
                    //String insertQuery = "INSERT INTO STUDIES VALUES (" + values + ")";
                	System.out.println();
                	System.out.println("---  Insert new study query ---");
                	System.out.println(insertQuery);
                	System.out.println();
                	statement.executeQuery(insertQuery);
                	//--------------------------------------------------prepare SQL INSERT query and run it----       
            	}
        		else {
        			System.out.println();
        			System.out.println("Accession #: '" + valuesArray[3] + "' exists in STUDIES table");
        			System.out.println("The study will not be added");
        			System.out.println();
        		}
         
                if (rs != null) {rs.close(); }
                rs = null;
                closeConnection();
            } else {
                System.out.println("Error: No active Connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dm = null;
    }
    
    


    private void closeConnection() {
        try {
            if (con != null) {
                con.close();
            }
            con = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
}
