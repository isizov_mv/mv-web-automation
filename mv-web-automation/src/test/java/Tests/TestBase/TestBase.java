package Tests.TestBase;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import Pages.BasePage.Pages;

//import java.awt.Point;
import java.awt.Toolkit;

//******************
//import Utilities.DesiredCapsManager;
//import Utilities.DriverManager;
//******************

//import Utilities.JSWaiter;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;


public class TestBase {
    protected WebDriver driver;

    //******************
    //private DesiredCapsManager desiredCapsManager = new DesiredCapsManager();
    //******************


    public Pages pages;
    public SoftAssert softAssert;
    

    //Do the test setup
    @BeforeClass
    @Parameters({"browser","platform", "monitor"})
    public void setupTest (ITestContext context, @Optional String browser, @Optional String platform, @Optional int monitor) throws MalformedURLException {    
    	
    	//set global variables available to each test
        context.setAttribute("LastNameParam", "Lname");
        context.setAttribute("FirstNameParam", "Fname");
        context.setAttribute("SSNParam", "auto5");
        context.setAttribute("BirthdayParam", "12/12/1950");
        context.setAttribute("SurveyParam", "Test Survey 1");
    

        browser = "Chrome";
        platform = "windows";



        //******************
        /*
        //Get DesiredCapabilities
        DesiredCapabilities capabilities = desiredCapsManager.getDesiredCapabilities(browser,platform);
        //Create Driver with capabilities
        driver = new DriverManager(capabilities, browser).createDriver();

         */


        //System.out.println("Working Directory = " + System.getProperty("user.dir"));
        String framework_location = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", framework_location + "\\src\\main\\chromedriver88.exe");
        
        ChromeOptions options = new ChromeOptions(); 
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"}); 
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        options.setExperimentalOption("prefs", prefs);
        
        
        driver = new ChromeDriver(options);
        if (monitor == 2) {
        	this.useSecondMonitor();
        }
       
        driver.manage().window().maximize();
        //driver.manage().window().fullscreen();
        //
        //driver = new FirefoxDriver();


        //Create Pages object
        pages = new Pages(driver);
        softAssert= new SoftAssert(); 
    }
    
    private void useSecondMonitor() {
    	java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	int width = (int) screenSize.getWidth();
    	if (width <= 1366) {
    	    org.openqa.selenium.Point point = new Point(width, 0);
    	    driver.manage().window().setPosition(point);
    	}
    	/**
    	else {
    	    Point point = new Point(0, 0);
    	    driver.manage().window().setPosition(point);
    	    Dimension targetWindowSize = new Dimension(1920, 1080);
    	    driver.manage().window().setSize(targetWindowSize);
    	}
    	/**/
    	
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
