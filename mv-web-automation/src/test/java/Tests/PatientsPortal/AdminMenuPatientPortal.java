package Tests.PatientsPortal;

import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patient Portal")
public class AdminMenuPatientPortal extends TestBase{

    @Description("Test that Login to Patients Portal is successful, test Admin Page Menu functionalty")
    @Test
    @Parameters({"PortalURL", "PortalUser", "PortalPwd"})
    public void openPatientsPortalLoginPage(String PortalURL, String PortalUser, String PortalPwd) 
    		throws InterruptedException {

    	
        pages.patientsPortalLoginPage63().gotoLoginPage(PortalURL);
        pages.patientsPortalLoginPage63().enterUserName(PortalUser);
        pages.patientsPortalLoginPage63().enterPassword(PortalPwd);
        pages.patientsPortalLoginPage63().clickLoginButton();
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patient Portal was unsuccessfull");
        
        pages.patientsPortalAdminPage63().clickAdminTab();
        pages.patientsPortalAdminPage63().clickAuditLogs();
        
        
        //---USER-----------------------------------------------------
        pages.patientsPortalAdminPage63().clickUserButton();
        //User sub-menu buttons
        pages.patientsPortalAdminPage63().clickUserListsSubMenu();
        pages.patientsPortalAdminPage63().clickReferringPhysiciansSubMenu();
        pages.patientsPortalAdminPage63().clickRolesSubMenu();
        //------------------------------------------------------USER---

        //---PORTAL CORE-----------------------------------------------
        pages.patientsPortalAdminPage63().clickPortalCoreButton();
        //Portal Core sub-menu buttons
        pages.patientsPortalAdminPage63().clickSelectCoreSettingsSubMenu();
        pages.patientsPortalAdminPage63().clickUserAccountSettingsSubMenu();
        pages.patientsPortalAdminPage63().clickLabelCustomizationSubMenu();
        pages.patientsPortalAdminPage63().clickLoginPageConfigSubMenu();
        pages.patientsPortalAdminPage63().clickFormXMLListSubMenu();
        pages.patientsPortalAdminPage63().clickLicenseMngmntSubMenu();
        pages.patientsPortalAdminPage63().clickOtherSettingsSubMenu();               
        //------------------------------------------------PORTAL CORE---        
    
        
        //---PATIENT RESULT PORTAL--------------------------------------
        pages.patientsPortalAdminPage63().clickPatientResultPortalButton();
        //Patient Result Portal sub-menu buttons
        pages.patientsPortalAdminPage63().clickEmailServiceSubMenu();
        pages.patientsPortalAdminPage63().clickDocumentsPatientResultSubMenu();
        //--------------------------------------PATIENT RESULT PORTAL---        
        

        //HISTORY PORTAL IS NOT AVAILABLE NOW, SHOULD WORK WITH PREF PPORTAL=ON
        //--------------------------------------------------------------------
        //pages.patientPortalAdminPage().clickHistoryPortalButton();
        //History Portal sub-menu
        //pages.patientPortalAdminPage().clickGeneralHistoryPortalSubMenu();
        //pages.patientPortalAdminPage().clickHistorySurveyStatusSubMenu();
        
        
        //---SURVEY------------------------------------------------
        pages.patientsPortalAdminPage63().clickSurveyButton();
        //Survey SubMenus
        pages.patientsPortalAdminPage63().clickGeneralSurveySubMenu();
        pages.patientsPortalAdminPage63().clickSurveyPackageSubMenu();
        pages.patientsPortalAdminPage63().clickMaglinkEventsSubMenu();
        pages.patientsPortalAdminPage63().clickSurveyActionsSubMenu();
        //-------------------------------------------------SURVEY---        
        
        
        //---TECHPAD------------------------------------------
        pages.patientsPortalAdminPage63().clickTechpadButton();
        //Techpad SubMenus
        pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickMaglinkEventsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickExamActionsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickCommentsSettingsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickDiagramSettingsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickWorklistSortingTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickDynamicLabelsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickStatusAndTagsTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickResourcesTechpadSubMenu();
        pages.patientsPortalAdminPage63().clickPDFFormConfigTechpadSubMenu();
        //------------------------------------------TECHPAD---        
        
        
        
        
        //pages.patientPortalAdminPage().clickTestsButton();

        
        pages.patientsPortalHomePage63().clickGreyDropdownButton();
        pages.patientsPortalHomePage63().clickLogoutButton();
        
     


        Thread.sleep(1000);

    }
}
