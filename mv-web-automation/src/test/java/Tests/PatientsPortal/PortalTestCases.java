package Tests.PatientsPortal;

import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patient Portal")
public class PortalTestCases extends TestBase{
	
	
	

    
    @Description(" test dropdowns and text boxes in General submenu -> Techpad menu -> Admin tab")
    @Test
    @Parameters({"PortalURL", "PortalUser", "PortalPwd"})
    public void  testDropdownsAndTextboxesFromGeneralSubMenuTechpadMenuAdminTab(String PortalURL, String PortalUser, String PortalPwd) 
    		throws InterruptedException {

    	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
        
        
        pages.patientsPortalAdminPage63().clickAdminTab();
        pages.patientsPortalAdminPage63().clickTechpadButton();
        pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
        
        //pages.patientPortalAdminPage().clickDiagramSettingsTechpadSubMenu();

        
       

        
 /*       
        pages.adminTechPadGeneralSubMenu().check_showBarcodeForAccessionNumber();         
        pages.adminTechPadGeneralSubMenu().clickBarcodeDropdown();
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByText("CODE128"); 

        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByText("CODE128A");  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByText("CODE128B");  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByText("CODE39");  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByText("CODE39Extended");  
       
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByIndex(1); 
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByIndex(2);  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByIndex(3);  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByIndex(4);  
        pages.adminTechPadGeneralSubMenu().selectOptionFromBarcodeDropdownByIndex(5);      
   
        
        pages.adminTechPadGeneralSubMenu().check_allowToChangeWorklistStatusFromCompleted();   
        pages.adminTechPadGeneralSubMenu().clickAllowToChangeWorklistStatusDropdown();
        pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByText("ExamWaiting");
        Thread.sleep(500);
*/
/*        
        pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByText("ExamIncomplete");
        Thread.sleep(500);
        pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByText("ExamWaiting");
        Thread.sleep(500);
        pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByText("ExamIncomplete");
       
        pages.adminTechPadGeneralSubMenu().check_showChatMenuItem(); 
        pages.adminTechPadGeneralSubMenu().enterTextIntoChatServiceUrlTextbox("this is a test for Chat service url");
        
        
        pages.adminTechPadGeneralSubMenu().check_enableTechpadDiagramRedirection();
        pages.adminTechPadGeneralSubMenu().enterTextIntoTechpadDiagramRedirectURLTextbox("this is a test for Redirect URL");
        
        
        pages.adminTechPadGeneralSubMenu().check_showDiagramOldModeWarning();
        String initialText = "You are trying to edit the diagram which is in old format and is not supported anymore. "
        		+ "<br> To prevent unexpected result, you may need to re-draw entire diagram. "
        		+ "<br> To do that, click 'Clear Diagram' button and start drawing in new format.";
        pages.adminTechPadGeneralSubMenu().enterTextIntodiagramOldModeWarningTextbox("this is a test for Diagram Old Mode Warning");
        pages.adminTechPadGeneralSubMenu().enterTextIntodiagramOldModeWarningTextbox(initialText);
        
        
        pages.adminTechPadGeneralSubMenu().uncheck_carryForwardTechpadDiagram();
        pages.adminTechPadGeneralSubMenu().check_carryForwardTechpadDiagram();      
        pages.adminTechPadGeneralSubMenu().doOnOff_carryForwardTechpadDiagram();   
        
*/ 
/**        
        pages.adminTechPadGeneralSubMenu().clickDefaultWorklistTabsDropdown();
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("History");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Documents");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Risk");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("FollowUps");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Select Tab");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Exam");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Demographic");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Diagram");
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabsDropdownByText("Comments");

        pages.adminTechPadGeneralSubMenu().doOnOff_showTyrerCuzikTab();          
        
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(4);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(9);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(8);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(7);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(5);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(6);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(2);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(1);
        pages.adminTechPadGeneralSubMenu().selectOptionFromDefaultWorklistTabDropdownByIndex(3);
        
        pages.adminTechPadGeneralSubMenu().doOnOff_showTyrerCuzikTab();
        
**/        

        pages.adminTechPadGeneralSubMenuPage63().clickDiagramCarryForward_OFFRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramCarryForward_OnRadioButton();
  
        pages.adminTechPadGeneralSubMenuPage63().clickComObjectID_TestRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickComObjectID_ProductionRadioButton();

        pages.adminTechPadGeneralSubMenuPage63().clickTimeFormat_AlwaysRadioButton(); 
        pages.adminTechPadGeneralSubMenuPage63().clickTimeFormat_DaysRadioButton();

        pages.adminTechPadGeneralSubMenuPage63().clickDiagramMode_LargeRadioButton();              
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramMode_SmallRadioButton();             
       
        
        
        
        
        //pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByIndex(1);
        //pages.adminTechPadGeneralSubMenu().selectOptionFromAllowToChangeWorklistStatusDropdownByIndex(2);
        
        
        
        

       

        Thread.sleep(2000);

    }
    
    
	
	
	

    @Description(" test  checkboxes")
    @Test
    @Parameters({"PortalURL", "PortalUser", "PortalPwd"})
    public void testRegressionForGeneralSubMenuTechpadMenuAdminTab(String PortalURL, String PortalUser, String PortalPwd) 
    		throws InterruptedException {

    	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patient Portal was unsuccessfull");
        
        //pages.patientsPortalAdminPage().clickAdminTab();
        pages.patientsPortalHomePage63().clickAdminButton();
        pages.patientsPortalAdminPage63().clickTechpadButton();
        pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
        
        
        
        //===Various Options section=================================================================
        
        // test checkboxes:
/**/       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_UpdateStudiesRISHold_F_onEndExam_buttonClick();           
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_UpdateStudies_T_End_timestamp_on_EndExam_buttonClick();    
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showDemoMenuItem();                  
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_hideStart_EndExamButtons();        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showPatients_MenuItem();       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_disableRequiringStudiesFields();       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableWorklistPatientsDataSynchronization();     
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showSelectSecondTechnologist();       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showSpeedTestMenuItem();       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableSurveyHistoryMode();     
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_includedPersonalBRCAHistory();          
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_includedFamilyBRCAHistory();     
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableAbilityToCreatePDFDocuments();  
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_hideSkipStartSurveyButtonsOnStudyDetailsPage();       
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_disableStoreRiskTriggerForWorklistSave();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableReloadDocumentsOnEachClickOnDocumentsTab();  
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_validateRequiredFieldsOnEveryExamSave();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableDashboardMode();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showBarcodeForAccessionNumber();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showChatMenuItem();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_allowToChangeWorklistStatusFromCompleted();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableTechpadDiagramRedirection();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_groupExamTypeWorklistFilter();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showTyrerCuzikTab();
         
        //test textboxes and dropdown lists:
        pages.adminTechPadGeneralSubMenuPage63().check_showBarcodeForAccessionNumber();
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromBarcodeDropdownByText("CODE128A");  
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromBarcodeDropdownByText("CODE128B");  
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromBarcodeDropdownByText("CODE39");  
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromBarcodeDropdownByText("CODE39Extended"); 
        
        pages.adminTechPadGeneralSubMenuPage63().check_showChatMenuItem();
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoChatServiceUrlTextboxTextbox("test");
       
        pages.adminTechPadGeneralSubMenuPage63().check_showTyrerCuzikTab();
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromFamilyTreeSurveyNameDropdownByText("Select Family Tree Survey");
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromFamilyTreeSurveyNameDropdownByText("Genesis_Medical");        
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromFamilyTreeSurveyNameDropdownByText("All_Questions_v6");
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromFamilyTreeSurveyNameDropdownByText("All");
         
/**/       
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramCarryForward_OFFRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramCarryForward_OnRadioButton();
       
        pages.adminTechPadGeneralSubMenuPage63().clickComObjectID_TestRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickComObjectID_ProductionRadioButton();
        
        
        pages.adminTechPadGeneralSubMenuPage63().clickDefaultWorklistTabsDropdown();
        
/**/        

        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("History");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Documents");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Risk");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("FollowUps");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Select Tab");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Exam");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Demographic");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Diagram");
        pages.adminTechPadGeneralSubMenuPage63().selectDefaultWorklistTabDropdownByText("Comments");        

        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoEnabledCodesOnFamilyTreeTextbox("TEST1, TEST2");
        
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoWorklistPrioritizingAccuracyTextbox("TEST3, TEST4");
        
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoSessionExpirationTimeoutTextbox("999");
        
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoSessionWarnTimeoutTextbox("888");
        
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoHistoryLoadRetriesTextbox("777");
        
        pages.adminTechPadGeneralSubMenuPage63().clickTimeFormat_AlwaysRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickTimeFormat_DaysRadioButton();
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_ShowTotalTimeOptions();
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showDetailTime();
        pages.adminTechPadGeneralSubMenuPage63().check_showDetailTime();
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showCommentTime();
        
      
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_useAlternativePatientIdentifier();
        pages.adminTechPadGeneralSubMenuPage63().check_useAlternativePatientIdentifier();
        
        pages.adminTechPadGeneralSubMenuPage63().selectOptionFromPatientIdentifierFieldsDropdownByText("CMRN");
/**/     
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_updateStudiesRISHoldFStudiesDepartTimestamp();
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_enableLDAPAuthentication();
        pages.adminTechPadGeneralSubMenuPage63().check_enableLDAPAuthentication();
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoLDAPDomainTextbox("SOME DOMAIN TEST");
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_demoMode();
        
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramMode_LargeRadioButton();
        pages.adminTechPadGeneralSubMenuPage63().clickDiagramMode_SmallRadioButton();
        
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showDiagramOldModeWarning();
        pages.adminTechPadGeneralSubMenuPage63().check_showDiagramOldModeWarning();
        pages.adminTechPadGeneralSubMenuPage63().enterTextIntoDiagramOldModeWarningTextbox("TEST OLD MODE WARNING TESTBOX");
        
        
        //pages.adminTechPadGeneralSubMenu().clickResetVariousOptionsButton();
        
/**/
        
        
        Thread.sleep(1000);

    }
    
    @Description("a short test")
    @Test
    @Parameters({"PortalURL", "PortalUser", "PortalPwd"})
    public void shortTest(String PortalURL, String PortalUser, String PortalPwd) 
    		throws InterruptedException {

    	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patient Portal was unsuccessfull");
        

        pages.patientsPortalHomePage63().clickAdminButton();
        pages.patientsPortalAdminPage63().clickTechpadButton();
        pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
        
        

        pages.adminTechPadGeneralSubMenuPage63().doOnOff_UpdateStudiesRISHold_F_onEndExam_buttonClick();           
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_UpdateStudies_T_End_timestamp_on_EndExam_buttonClick();    
        pages.adminTechPadGeneralSubMenuPage63().doOnOff_showDemoMenuItem();                  
   
        
        
        Thread.sleep(1000);

    }
   
    
}
