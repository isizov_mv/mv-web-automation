package Tests.PatientsPortal;

import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patient Portal")
public class UserListPatientPortal extends TestBase{

    @Description("Test User List functionalty")
    @Test
    @Parameters({"PortalURL", "PortalUser", "PortalPwd"})
    public void testUserList(String PortalURL, String PortalUser, String PortalPwd) 
    		throws InterruptedException {

    	
        pages.patientsPortalLoginPage63().gotoLoginPage(PortalURL);
        pages.patientsPortalLoginPage63().enterUserName(PortalUser);
        pages.patientsPortalLoginPage63().enterPassword(PortalPwd);
        pages.patientsPortalLoginPage63().clickLoginButton();
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patient Portal was unsuccessfull");
        
        pages.patientsPortalAdminPage63().clickAdminTab();
        
        
        
        //---USER-----------------------------------------------------
        pages.patientsPortalAdminPage63().clickUserButton();
        //User sub-menu buttons
        pages.patientsPortalAdminPage63().clickUserListsSubMenu();
        
        //press add user and then press cancel
        pages.userListSubTab63().clickAddUserButton();
        pages.userListSubTab63().clickCancelToAddUserButton();
        //------------------------------------------------------USER---



        
        pages.patientsPortalHomePage63().clickGreyDropdownButton();
        pages.patientsPortalHomePage63().clickLogoutButton();
        
     


        Thread.sleep(1000);

    }
}
