package Tests.SQL;

import Utilities.ServerConnection;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.util.Random;


@Feature("Techpad")
public class PerformSQLQuery {

    private ServerConnection serverConnection;
    @Description("Add a new patient")
    @Test
    @Parameters({"SSN", "LastName", "FirstName", "birthday",  "serverName", "databaseName"})
    public void addPatient(String SSN, String lastName, String firstName, String birthday, String serverName, String databaseName) 
    		throws InterruptedException {
    	 	
    	serverConnection= new ServerConnection();
    	serverConnection.addNewPatient(SSN, lastName, firstName, birthday, serverName, databaseName);

        Thread.sleep(1000);
    }
    
    @Description("Add a new study")
    @Test
    @Parameters({"SSN",  "serverName", "databaseName"})
    public void addStudy(String SSN, String serverName, String databaseName) 
    		throws InterruptedException {
    	 	
    	serverConnection= new ServerConnection();
    	serverConnection.addNewStudy(SSN, serverName, databaseName);
        Thread.sleep(1000);
    }
    
    
}
