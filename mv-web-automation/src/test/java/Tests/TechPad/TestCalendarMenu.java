package Tests.TechPad;


import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patients Portal, Techpad")
public class TestCalendarMenu extends TestBase{

    
    //##############  TEST 	CALENDAR MENU  ######################
    @Description("Techpad: Log in to Techpad > Open Patient \r\n "
    		+ "> Open Calendar > Test days, months, years selection")
    @Test 
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", 
    							  "TechpadPwd", "SSN", "LastName", "FirstName", "birthday"})
    public void testCalendar(String TechpadURL, String HospitalCode, String TechpadUser, 
    		String TechpadPwd, String SSN, String LastName, String FirstName, String birthday) 
    		throws Exception {
    	

    	pages.techPadLoginPage().loginToTechPad(TechpadURL, HospitalCode, TechpadUser, TechpadPwd);
    	Assert.assertTrue(pages.techPadLoginPage().isLoginSuccessful(), "Login to Techpad was unsuccessful");
  	
    	pages.techPadHomePage().clickHamburgerMenuButton();
    	pages.techPadHomePage().clickPatientsLeftButton();
    	pages.techPadHomePage().clickAdvancedSearchPlusButton();    	
    	pages.techPadHomePage().enterTextIntoMRNTextbox(SSN);    	
    	pages.techPadHomePage().clickSearchButton();
    	pages.techPadHomePage().waitUntilLookingForPatientsLabelIsVisible();
    	pages.techPadHomePage().selectPatientBySSN(SSN);
    	
    	pages.patientTabPage().clickCalendarButton();
    	pages.calendarPage().clickCloseButton();
    	pages.patientTabPage().clickCalendarButton();
    	pages.calendarPage().selectOptionFromMonthDropdownByText("Sept");
    	pages.calendarPage().selectOptionFromYearDropdownByText("1949");    	
    	pages.calendarPage().selectDayButtonFromCalendarByText("11");
    	pages.calendarPage().selectDayButtonFromCalendarByText("11");
    	pages.calendarPage().selectDayButtonFromCalendarByText("8");
    	pages.calendarPage().clickSetButton();
    	   	
        Thread.sleep(1000);
    }
    
    
}

