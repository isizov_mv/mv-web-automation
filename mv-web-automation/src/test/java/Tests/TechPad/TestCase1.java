package Tests.TechPad;


import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import Utilities.ServerConnection;

@Feature("Patients Portal, Techpad")
public class TestCase1 extends TestBase{
	
    //##############  TEST CASE 1 ######################
	
    @Description("\"Portal: ADMIN > Techpad > General > Enable “Show Patients Menu Item” > \r\n"
    		+ "          	Save Techpad: Log into techpad > Click on Menu > open Patients Menu > \r\n"
    		+ "          	Search for Patients(test with advance search as well) > \r\n"
    		+ "          	open a patient > Modify patient data > Save > Click Studies tab > \r\n"
    		+ "          	Open Study > Test buttons Add to Worklist, Skip Survey, and Start Exam.\"")
    @Test    
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", 
    							  "TechpadPwd", "PortalURL", "PortalUser", "PortalPwd",
    							  "SSN", "LastName", "FirstName", "birthday"})
    public void testCase1(String TechpadURL, String HospitalCode, String TechpadUser, 
    		String TechpadPwd, String PortalURL, String PortalUser, String PortalPwd,
    		String SSN, String LastName, String FirstName, String birthday) 
    		throws InterruptedException {
    	 	    	
    	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
        Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
       
        pages.patientsPortalHomePage63().clickAdminButton();
        pages.patientsPortalAdminPage63().clickTechpadButton();
        pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
        pages.adminTechPadGeneralSubMenuPage63().check_showPatients_MenuItem();
        pages.adminTechPadGeneralSubMenuPage63().clickSaveButton();
        pages.patientsPortalHomePage63().clickLogoutButton();
    	    	
    	pages.techPadLoginPage().loginToTechPad(TechpadURL, HospitalCode, TechpadUser, TechpadPwd);
    	Assert.assertTrue(pages.techPadLoginPage().isLoginSuccessful(), "Login to Techpad was unsuccessful");
    	
    	pages.techPadHomePage().clickHamburgerMenuButton();
    	pages.techPadHomePage().clickPatientsLeftButton();
    	pages.techPadHomePage().enterTextIntoSimpleSearchTextbox(" ");

    	pages.techPadHomePage().clickSearchButton();
    	pages.techPadHomePage().waitUntilLookingForPatientsLabelIsVisible();
    		
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientSSNIsShown(SSN), "Patient SSN " + "'" + SSN + "'" + "  is not shown");       	  	
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientFirstNameIsShown(LastName), "Patient First Name " + "'" + LastName + "'" + "  is not shown");   
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientLastNameIsShown(FirstName), "Patient Last Name " + "'" + FirstName + "'" + "  is not shown");   
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientBirthdayIsShown(birthday), "Patient birthday " + "'" + birthday + "'" + "  is not shown");      	
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientSSNFnameLnameBirthdayAreShown(SSN, LastName, FirstName, birthday),
    			"Information for the patient " + SSN + " - " + LastName + ", " +FirstName + " " + "(" + birthday + ")" + " is not shown");
	 	
    	pages.techPadHomePage().clickAdvancedSearchPlusButton();
    	
    	pages.techPadHomePage().enterTextIntoMRNTextbox(SSN);
    	pages.techPadHomePage().enterTextIntoFirstNameTextbox(FirstName);
    	pages.techPadHomePage().enterTextIntoLastNameTextbox(LastName);
    	
    	pages.techPadHomePage().clickSearchButton();
    	pages.techPadHomePage().waitUntilLookingForPatientsLabelIsVisible();
    	
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientSSNFnameLnameBirthdayAreShown(SSN, LastName, FirstName, birthday),
    			"Information for the patient " + SSN + " - " + LastName + ", " + FirstName + " " + "(" + birthday + ")" + " is not shown"); 

    	pages.techPadHomePage().selectPatient(SSN, LastName, FirstName, birthday);

    	pages.patientTabPage().enterPatientLastNameInPatientTab("LName_was_edited");
    	pages.patientTabPage().enterPatientFirstNameInPatientTab("FName_was_edited");
    	pages.patientTabPage().clickDOBField();
    	pages.patientTabPage().clickCloseDOBCalendarButton();
    	 	
    	pages.patientTabPage().enterTextIntoEmailTextbox("testCase1@ng.com");
    	
    	pages.patientTabPage().selectOptionFromSelectRaceDropdownByIndex(10);
    	pages.patientTabPage().selectOptionFromSelectRaceDropdownByText("Other Race");
    	pages.patientTabPage().clickSaveButton();

    	//restore names back for future tests
    	pages.patientTabPage().enterPatientLastNameInPatientTab(LastName);
    	pages.patientTabPage().enterPatientFirstNameInPatientTab(FirstName);
    	pages.patientTabPage().clickSaveButton();
    	
    	pages.patientTabPage().clickStudiesTabButton();
    	pages.studiesTabPage().selectStudyFromStudiesListByIDNumber("AccNum" + SSN);
    	
    	pages.studyDetailsPage().clickAddToWorklistButton();
    	pages.studyDetailsPage().clickOKButton();
    	
    	pages.studyDetailsPage().clickSkipSurveyButton();
    	pages.studyDetailsPage().clickOKButton();

    	pages.studyDetailsPage().clickStartSurveyButton();
    	pages.studyDetailsPage().clickOKButton();   	
    	
    	pages.techPadHomePage().clickBackFromPatientsDetailsButton();
	 	
    	pages.techPadHomePage().clickLogoutButton();
    	pages.techPadHomePage().clickYesToConfirmLogoutButton();
    	softAssert.assertAll();

    }
}








