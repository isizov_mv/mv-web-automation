package Tests.TechPad;

import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patients Portal, Techpad")
public class TestCase4 extends TestBase{
   
    //##############  TEST CASE 4 ######################
    @Description("Portal: ADMIN > Techpad > General > Enable ability to create PDF documents. \r\n"
    		+ "Go to admin > techpad pdf forms configuration > select and upload a pdf document. \r\n"
    		+ "Techpad: Log into techpad > Perform Exam on Patient in Worklist > \r\n"
    		+ "Go to documents tab > Click Add New Document> Select doc and Save > \r\n"
    		+ "Confirm document is displayed and open the document. ")
    @Test
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", 
    							  "TechpadPwd", "PortalURL", "PortalUser", "PortalPwd",
    							  "SSN", "LastName", "FirstName", "birthday"})
    public void testCase4(String TechpadURL, String HospitalCode, String TechpadUser, 
    		String TechpadPwd, String PortalURL, String PortalUser, String PortalPwd,
    		String SSN, String LastName, String FirstName, String birthday) 
    		throws Exception {
    	
    	

    	softAssert.assertAll();
    }

}