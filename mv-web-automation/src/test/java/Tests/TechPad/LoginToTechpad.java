package Tests.TechPad;


import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



@Feature("Patient Portal")
public class LoginToTechpad extends TestBase{

    @Description("Login to TechPad")
    @Test
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", "TechpadPwd"})
    public void testLoginToTechPad(String TechpadURL, String HospitalCode, String TechpadUser, String TechpadPwd) 
    		throws InterruptedException {
    	
    	
    	pages.techPadLoginPage().loginToTechPad(TechpadURL, HospitalCode, TechpadUser, TechpadPwd);
    	Assert.assertTrue(pages.techPadLoginPage().isLoginSuccessful(), "Login to Techpad was unsuccessful");
    	
        Thread.sleep(1000);

    }
}
