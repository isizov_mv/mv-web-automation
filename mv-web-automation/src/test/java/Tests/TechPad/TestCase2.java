package Tests.TechPad;

import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Feature("Patients Portal, Techpad")
public class TestCase2 extends TestBase{
   
    //##############  TEST CASE 2 ######################
    @Description("Portal: ADMIN > Techpad > General > Enable Survey history mode > Save\r\n"
    		+ "Techpad: Log into techpad > Perform Exam on Patient in Worklist > Click Survey History tab > "
    		+ "Test Start Survey button and Skip Survey button")
    @Test
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", 
    							  "TechpadPwd", "PortalURL", "PortalUser", "PortalPwd",
    							  "SSN", "LastName", "FirstName", "birthday"})
    public void testCase2(String TechpadURL, String HospitalCode, String TechpadUser, 
    		String TechpadPwd, String PortalURL, String PortalUser, String PortalPwd,
    		String SSN, String LastName, String FirstName, String birthday) 
    		throws Exception {
    	
    	//test Portal
    	String portalVersion = "7.1.1";
    	if (portalVersion == "5.20"){
    			
	    	pages.patientsPortalLoginPage52().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
	        Assert.assertTrue(pages.patientsPortalHomePage52().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
	       
	        pages.patientsPortalHomePage52().clickAdminButton();
	        pages.patientsPortalAdminPage52().clickPortalConfigurationButton();	        
	        pages.patientsPortalAdminPage52().clickTechpadSettingsButton();
	        pages.adminPortalConfigTechpadSettingsPage52().check_EnableSurveyHistoryMode();
	        pages.adminPortalConfigTechpadSettingsPage52().clickSaveButton();
	        pages.patientsPortalHomePage52().clickLogoutButton();
    	}
    	else {
        	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
            Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
                      
            pages.patientsPortalAdminPage63().clickAdminTab();
            pages.patientsPortalAdminPage63().clickTechpadButton();
            pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
            pages.adminTechPadGeneralSubMenuPage63().check_enableSurveyHistoryMode();
            pages.adminTechPadGeneralSubMenuPage63().clickSaveButton();
            pages.patientsPortalHomePage63().clickLogoutButton();
    		
    	}
		//test Techpad
    	//###########################################

    	pages.techPadLoginPage().loginToTechPad(TechpadURL, HospitalCode, TechpadUser, TechpadPwd);
    	Assert.assertTrue(pages.techPadLoginPage().isLoginSuccessful(), "Login to Techpad was unsuccessful");
	
    	pages.techPadHomePage().clickHamburgerMenuButton();
    	pages.techPadHomePage().clickPatientsLeftButton();
    		
    	pages.techPadHomePage().clickAdvancedSearchPlusButton();    	
    	pages.techPadHomePage().enterTextIntoMRNTextbox(SSN);    	
    	pages.techPadHomePage().clickSearchButton();
    	pages.techPadHomePage().waitUntilLookingForPatientsLabelIsVisible();
    	
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientSSNIsShown(SSN));
    	
    	pages.techPadHomePage().selectPatientBySSN(SSN);
    	pages.patientTabPage().clickStudiesTabButton();
    	
    	Assert.assertTrue(pages.studiesTabPage().isStudyShown("AccNum" + SSN), "Study with accession #: '" + "AccNum" + SSN + "' is not shown");
    	
    	pages.studiesTabPage().selectStudyFromStudiesListByIDNumber("AccNum" + SSN);
    	pages.studiesTabPage().clickYesButton();
    	
    	pages.studyDetailsPage().clickAddToWorklistButton();
    	pages.studyDetailsPage().clickGoBackButton();
    	pages.patientTabPage().clickSaveButton();
    	pages.studyDetailsPage().clickGoBackButton();
    	
    	pages.techPadHomePage().clickHamburgerMenuButton(); 	
    	pages.techPadHomePage().clickWorklistLeftButton();
    	
    	pages.techPadHomePage().clickWorklistFilterSettingsButton();
    	pages.techPadHomePage().uncheck_ShowOnlyMyExams();
    	pages.techPadHomePage().uncheck_ShowOnlyTodaysExams();
    	pages.techPadHomePage().clickSaveWorklistFilterSettingsButton();    	

    	pages.techPadHomePage().clickHamburgerMenuButton(); 	
    	pages.techPadHomePage().clickWorklistLeftButton();
  	
    	pages.techPadHomePage().searchExamWorklistByPatientSSNLnameFirstName(SSN, LastName, FirstName);
    	pages.techPadHomePage().selectExamFromSearchResultsByPatientSSNLnameFname(SSN, LastName, FirstName); 	
    	pages.techPadHomePage().clickPerformExamButton();    	
    	pages.techPadHomePage().clickYesToConfirmOvertakeExamButton();
    	
    	
    	//test Exam tab's elements
    	pages.examTabPage().selectOptionFromReasonForVisitDropdownByIndex(5);
    	pages.examTabPage().selectOptionFromReasonForVisitDropdownByText("Diagnostic (Specify Problem)");
    	
    	pages.examTabPage().selectOptionFromInfectionControlDropdownByIndex(0);
    	pages.examTabPage().selectOptionFromInfectionControlDropdownByText("Yes");
    	
    	//delete all views first
    	pages.examTabPage().deleteAllViewsObtained();   	
    	//add two new views and add side and type    	
    	pages.examTabPage().addViewsObtainedByOptionText_AddSide_AddType("Craniocaudal", "Left", "Spot");
    	pages.examTabPage().addViewsObtainedByOptionText_AddSide_AddType("Mediolateral", "Both", "ID");

    	//delete all procedures first
    	pages.examTabPage().deleteAllProcedures();
    	//add two new procedures and add side
    	pages.examTabPage().addProcedureByOptionText_AddSide("Diagnostic Mammogram", "Both");
    	
    	pages.examTabPage().clickProblemsCircleButton();
    	//select Problem, side/s, location/s    	
    	pages.examTabPage().selectProblem_UnspecifiedLeftRightSide_checkLocation(
    			"test", "uncheck", "uncheck", "check", "Lateral");
    	pages.examTabPage().selectProblem_UnspecifiedLeftRightSide_checkLocation(
    			"Very strong family history of breast cancer", "uncheck", "check", "uncheck", "Lateral");
		pages.examTabPage().selectProblem_UnspecifiedLeftRightSide_checkLocation(
    			"Skin thickening or retraction", "uncheck", "uncheck", "check", "Periareolar"); 	
		pages.examTabPage().selectProblem_UnspecifiedLeftRightSide_checkLocation(
    			"Skin thickening or retraction", "uncheck", "uncheck", "check", "1 o'clock");   
		pages.examTabPage().selectProblem_UnspecifiedLeftRightSide_uncheckLocation(
    			"Skin thickening or retraction", "uncheck", "uncheck", "check", "Periareolar"); 	
		
		pages.examTabPage().clickOKButton();
		
    	//add ultrasound procedure and test Scan Technique menu
    	pages.examTabPage().addProcedureByOptionText_AddSide("Breast Ultrasound", "Both"); 
    	
    	pages.examTabPage().clickScanTechniqueButton();
    	
    	pages.examTabPage().selectOptionFromScanTechniqueDropdownByText("radial/anti-radial");
    	pages.examTabPage().selectOptionFromSideDropdownByText("Right");
    	pages.examTabPage().selectOptionFrom_FromLocationDropdown_ByText("Deep Sub-Areolar");
    	pages.examTabPage().selectOptionFrom_FromDistanceDropdown_ByText("28");
    	pages.examTabPage().selectOptionFromToLocationDropdownByText("Medial");
    	pages.examTabPage().selectOptionFromToDistanceDropdownByText("23");
    	pages.examTabPage().selectOptionFromTypeDropdownByText("Palpable abnormality");
    	
    	pages.examTabPage().check_axillaYes_buttonClick();
    	pages.examTabPage().uncheck_axillaYes_buttonClick();
    	pages.examTabPage().check_axillaNo_buttonClick();
    	pages.examTabPage().uncheck_axillaNo_buttonClick();
        	
    	pages.examTabPage().clickCancelScanTechniqueButton();
    	pages.examTabPage().clickNoButton();

    	pages.examTabPage().clickDeleteScanTechniqueButton();
    	pages.examTabPage().clickYesButton();
    	pages.examTabPage().clickCreateNewTechniqueButton();
    	pages.examTabPage().selectOptionFromScanTechniqueDropdownByText("transverse and longitudinal");
    	pages.examTabPage().check_axillaYes_buttonClick(); 
    	pages.examTabPage().enterTextIntoNoteTextbox("Testing Note textbox");
    	pages.examTabPage().clickSaveScanTechniqueButton();
    	pages.examTabPage().clickCloseUltrasoundScanTechniqueButton();
    	pages.examTabPage().clickYesButton();
 	
    	pages.performExamPage().clickSurveyHistoryTab();
    	pages.surveyHistoryTabPage().clickSkipSurveyButton();
    	pages.surveyHistoryTabPage().clickCancelButton();
    	pages.surveyHistoryTabPage().clickStartSurveyButton();
    	pages.surveyHistoryTabPage().clickSaveAndContinueButton();

    	softAssert.assertAll();
    }

}