package Tests.TechPad;


import Tests.TestBase.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


@Feature("Patients Portal, Techpad")
public class TestCase3 extends TestBase{

    //##############  TEST CASE 3 ######################
	
    @Description("Portal: ADMIN > Techpad > General > Enable Included Personal BRCA History > \r\n"
    		+ "Disable Survey History mode > Save\r\n"
    		+ "Techpad: Log into techpad > Perform Exam on Patient in Worklist > Add personal BRCA")
    @Test    
    @Parameters({"TechpadURL", "HospitalCode", "TechpadUser", 
    							  "TechpadPwd", "PortalURL", "PortalUser", "PortalPwd",
    							  "SSN", "LastName", "FirstName", "birthday"})
    public void testCase3(String TechpadURL, String HospitalCode, String TechpadUser, 
    		String TechpadPwd, String PortalURL, String PortalUser, String PortalPwd,
    		String SSN, String LastName, String FirstName, String birthday) 
    		throws InterruptedException {
    	 	    	
/** 	
    	//test Portal
    	String portalVersion = "7.1.1";
    	if (portalVersion == "5.20"){
    			
	    	pages.patientsPortalLoginPage52().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
	        Assert.assertTrue(pages.patientsPortalHomePage52().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
	       
	        pages.patientsPortalHomePage52().clickAdminButton();
	        pages.patientsPortalAdminPage52().clickPortalConfigurationButton();	        
	        pages.patientsPortalAdminPage52().clickTechpadSettingsButton();
	        pages.adminTechPadGeneralSubMenuPage63().check_includedPersonalBRCAHistory();
	        pages.adminPortalConfigTechpadSettingsPage52().clickSaveButton();
	        pages.patientsPortalHomePage52().clickLogoutButton();
    	}
    	else {
        	pages.patientsPortalLoginPage63().loginToPatientPortal(PortalURL, PortalUser, PortalPwd);
            Assert.assertTrue(pages.patientsPortalHomePage63().isLoginSuccessful(), "Login to Patients Portal was unsuccessful");
                      
            pages.patientsPortalAdminPage63().clickAdminTab();
            pages.patientsPortalAdminPage63().clickTechpadButton();
            pages.patientsPortalAdminPage63().clickGeneralTechpadSubMenu();
            pages.adminTechPadGeneralSubMenuPage63().check_includedPersonalBRCAHistory();
            pages.adminTechPadGeneralSubMenuPage63().uncheck_enableSurveyHistoryMode();
            pages.adminTechPadGeneralSubMenuPage63().clickSaveButton();
            pages.patientsPortalHomePage63().clickLogoutButton();
    		
    	}
/**/  	
    	pages.techPadLoginPage().loginToTechPad(TechpadURL, HospitalCode, TechpadUser, TechpadPwd);
    	Assert.assertTrue(pages.techPadLoginPage().isLoginSuccessful(), "Login to Techpad was unsuccessful");
	
    	pages.techPadHomePage().clickHamburgerMenuButton();
    	pages.techPadHomePage().clickPatientsLeftButton();
    		
    	pages.techPadHomePage().clickAdvancedSearchPlusButton();    	
    	pages.techPadHomePage().enterTextIntoMRNTextbox(SSN);    	
    	pages.techPadHomePage().clickSearchButton();
    	pages.techPadHomePage().waitUntilLookingForPatientsLabelIsVisible();
    	
    	softAssert.assertTrue(pages.techPadHomePage().
    			verifyPatientSSNIsShown(SSN));
    	
    	pages.techPadHomePage().selectPatientBySSN(SSN);
    	pages.patientTabPage().clickStudiesTabButton();
    	
    	Assert.assertTrue(pages.studiesTabPage().isStudyShown("AccNum" + SSN), "Study with accession #: '" + "AccNum" + SSN + "' is not shown");
    	
    	pages.studiesTabPage().selectStudyFromStudiesListByIDNumber("AccNum" + SSN);
    	pages.studiesTabPage().clickYesButton();
    	
    	pages.studyDetailsPage().clickAddToWorklistButton();
    	pages.studyDetailsPage().clickGoBackButton();
    	pages.patientTabPage().clickSaveButton();
    	pages.studyDetailsPage().clickGoBackButton();
  	
    	pages.techPadHomePage().clickHamburgerMenuButton(); 	
    	pages.techPadHomePage().clickWorklistLeftButton();
  	    	
    	pages.techPadHomePage().clickWorklistFilterSettingsButton();
    	pages.techPadHomePage().uncheck_ShowOnlyMyExams();
    	pages.techPadHomePage().uncheck_ShowOnlyTodaysExams();
    	pages.techPadHomePage().clickSaveWorklistFilterSettingsButton();    	

    	pages.techPadHomePage().clickHamburgerMenuButton(); 	
    	pages.techPadHomePage().clickWorklistLeftButton();
   	
    	pages.techPadHomePage().searchExamWorklistByPatientSSNLnameFirstName(SSN, LastName, FirstName);

    	pages.techPadHomePage().selectExamFromSearchResultsByPatientSSNLnameFname(SSN, LastName, FirstName); 	
    	pages.techPadHomePage().clickPerformExamButton();    	
    	pages.techPadHomePage().clickYesToConfirmOvertakeExamButton();
    	
    	
    	   	
        Thread.sleep(1000);
    }
    
    
}






